module Main where

import           MaterialistDesign
import           Data.List
import           Data.Foldable
import           Data.Simulation.Population
import           Data.Simulation.GeneticAlgorithms.GeneticAlgorithmPopulation
                                               as Pop
import           Data.Simulation.Types
import           Numeric.Natural
import           Control.Monad                 as CM
                                                ( replicateM )
import           Control.Concurrent.Async        as CCA ( mapConcurrently )
import           Data.Simulation.Solutions.Individual
import qualified Data.Vector                   as V
                                                ( Vector )
import           Data.Vector.Generic           as V
import qualified Data.Vector.Unboxed           as VU
import           Data.Simulation.Internal.Config
import           Data.Tuple                     ( swap )
import           Control.Lens
import           Data.Word                      ( Word32 )

import           Polysemy                hiding ( run )
import           Polysemy.AtomicState
import           Polysemy.RandomFu
import           Control.Effects.Cache
import qualified Data.Random                   as R
import qualified Data.Random.Source.PureMT     as R
import qualified System.Random.PCG.Fast.Pure   as RPCG
import           Data.Simulation.Internal.Utils ( )
import qualified Data.HashMap.Strict           as HM
import           Data.Bifunctor
import Control.Applicative
import Data.Traversable as DT
import qualified Data.List as DL
import Polysemy.Async
import           Debug.Trace
import           GHC.Conc
import Data.Simulation.Analysis.ParetoFrontier
import Control.Lens.Plated
import Algebra.PartialOrd

testfunc :: Double -> Double
testfunc !x = -8 * (cos x) + x

curve :: VU.Vector Double
curve = V.map testfunc fitnessxs
sampleCurve = V.map testfunc samplexs
main :: IO ()
main = do
    config <- getConfiguration
    let pops = fromIntegral $ config ^. run . populations . number
    gen   <- RPCG.createSystemRandom
    seeds <- CM.replicateM pops (RPCG.uniform gen >>= RPCG.initialize)
    let
        k =
            fromIntegral
                $  config
                ^. run
                .  populations
                .  maximumIterationsPerPopulation
    var      <- newTVarIO HM.empty
    critters <- runFinal $ embedToFinal
        $ asyncToIOFinal
        $  runAtomicStateTVar var
        $  runCacheWithHashMapAsAtomicState
        $ sequenceConcurrently
        $ fmap (\seed -> runRandomSource seed $ doRepros var k config) seeds --  CCA.mapConcurrently (doRepros var k config) seeds

    map <- readTVarIO var
    let msg = "Cache size: " <> (show $ HM.size map)
    print msg
    let sorted = fmap swap  $ sortBy (\(_, a) (_, b) -> compare b a) (HM.toList map) -- we want biggest first
    print (DL.take pops sorted)
    print (frontierFromSorted sorted)
sequenceConcurrently :: forall t r a. (Traversable t, Member Async r) =>
    t (Sem r a) -> Sem r (t (Maybe a))
sequenceConcurrently t = traverse async t >>= traverse await
instance PartialOrd Double where
    leq = (<=)
instance PartialOrd ExpressionTree where
    leq a b = (DL.length $ universe a) >= (DL.length $ universe b)
doRepros
    :: Members [RandomFu, Cache ExpressionTree Double] r => TVar (HM.HashMap ExpressionTree Double)
    -> Int
    -> Configuration
    -> Sem r (GeneticAlgorithmPopulation CompiledCGE)
doRepros var k config =
         do
              pop <-
                  spawnNewPopulation
                      config
                      curve
                      sampleCurve
              reproduceFor k config pop

{-module Main where

import           MaterialistDesign
import           Data.List
import           Data.Foldable
import           Data.Simulation.Population
import           Data.Simulation.GeneticAlgorithms.GeneticAlgorithmPopulation
                                               as Pop
import           Data.Simulation.Types
import           Numeric.Natural
import           Control.Monad                 as CM
                                                ( replicateM )
import           Control.Concurrent.Async        as CCA ( mapConcurrently )
import           Data.Simulation.Solutions.Individual
import qualified Data.Vector                   as V
                                                ( Vector )
import           Data.Vector.Generic           as V
import qualified Data.Vector.Unboxed           as VU
import           Data.Simulation.Internal.Config
import           Data.Tuple                     ( swap )
import           Control.Lens
import           Data.Word                      ( Word32 )

import           Polysemy                hiding ( run )
import           Polysemy.AtomicState
import           Polysemy.RandomFu
import           Control.Effects.Cache
import qualified Data.Random                   as R
import qualified Data.Random.Source.PureMT     as R
import qualified System.Random.PCG.Fast.Pure   as RPCG
import           Data.Simulation.Internal.Utils (parFmap )
import qualified Data.HashMap.Strict           as HM
import           Data.Bifunctor
import Control.Applicative
import qualified Data.List as DL
import Data.Traversable as DT
import Polysemy.Async
import           Debug.Trace
import           GHC.Conc

testfunc :: Double -> Double
testfunc !x = -8 * (cos x) + x

curve :: VU.Vector Double
curve = V.map testfunc fitnessxs
sampleCurve = V.map testfunc samplexs
main :: IO ()
main = do
    config <- getConfiguration
    let pops = fromIntegral $ config ^. run . populations . number
    gen   <- RPCG.createSystemRandom
    seeds <- CM.replicateM pops (RPCG.uniform gen >>= RPCG.initialize)
    let
        k =
            fromIntegral
                $  config
                ^. run
                .  populations
                .  maximumIterationsPerPopulation
    var      <- newTVarIO HM.empty
    (Just critters) <- runFinal $ embedToFinal
        $  runAtomicStateTVar var
        $  runCacheWithHashMapAsAtomicState
        $ asyncToIOFinal $ sequenceConcurrently
        $ fmap (\seed -> runRandomSource seed $ doRepros var k config) seeds


    map <- readTVarIO var
    let msg = "Cache size: " <> (show $ HM.size map)
    print msg
    let sorted = sortBy (\(a, _) (b, _) -> compare b a) (DL.take pops $ HM.toList map) -- we want biggest first
    print (fmap fst sorted, sorted)

sequenceConcurrently :: forall t r a. (Traversable t, Member Async r) => t (Sem r a) -> Sem r (Maybe (t a))
sequenceConcurrently t = do
        let tt = fmap async t
            bar = fmap  tt
        foo <- DT.sequence  $ fmap ((=<<)  await)  $ fmap async bar -- fmap DT.sequence $ DT.sequence $
        return (DT.sequence foo)



doRepros
    :: Members [RandomFu, Cache ExpressionTree Double] r => TVar (HM.HashMap ExpressionTree Double)
    -> Int
    -> Configuration
    -> Sem r (GeneticAlgorithmPopulation CompiledCGE)
doRepros var k config =
        do
              pop <-
                  spawnNewPopulation
                      config
                      curve
                      sampleCurve
              reproduceFor k config pop
{-main = do
    config <- getConfiguration
    let pops = fromIntegral $ config ^. run . populations . number
    gen   <- RPCG.createSystemRandom
    seeds <- CM.replicateM pops (RPCG.uniform gen >>= RPCG.initialize)
    let
        k =
            fromIntegral
                $  config
                ^. run
                .  populations
                .  maximumIterationsPerPopulation
    var      <- newTVarIO HM.empty
    critters <- CCA.mapConcurrently (doRepros var k config) seeds

    map <- readTVarIO var
    let msg = "Cache size: " <> (show $ HM.size map)
    print msg
    let sorted = sortBy (\(a, _) (b, _) -> compare b a) critters -- we want biggest first
    print (fmap fst sorted, sorted)-}
{-doRepros
    :: TVar (HM.HashMap ExpressionTree Double)
    -> Int
    -> Configuration
    -> RPCG.GenIO
    -> IO (Double, ExpressionTree)
doRepros var k config seed =
    runM
        $ runAtomicStateTVar var
        $ runCacheWithHashMapAsAtomicState
        $ runRandomSource seed
        $ do
              pop <-
                  spawnNewPopulation @(GeneticAlgorithmPopulation CompiledCGE)
                      config
                      curve
                      sampleCurve
              _ <- reproduceFor k config pop
              Control.Effects.Cache.best-}-}