\section{Expression trees}
\begin{code}
module ExpressionTrees where
import MaterialistDesign
data Literal a where
    IntValue :: Int -> Literal Int
    FloatValue :: Float -> Literal Float

data NamedConstant a where
    PiFloat :: NamedConstant Float
data Expression a where
    Value :: Literal a -> Expression a
    NamedConstant :: NamedConstant a -> Expression a
    Plus :: Expression a -> Expression a -> Expression a
    Multiply :: Expression a -> Expression a -> Expression a
    Subtract :: Expression a -> Expression a -> Expression a
    Divide :: Expression a -> Expression a -> Expression a
    Power :: Expression a -> Expression a -> Expression a

--instance Gene StdGen (Expression Float) where
--    type Nucleotide (Expression Float) =

\end{code}