\chapter{Twenty nucleotides equals one pair of genes: Self-modifying Cartesian genetic programming}
\comment{
TODO: Talk about why a dependently typed language would have made this MUCH easier.


We describe a self-modifying Cartesian genetic programming framework.
}
\comment{
\begin{code}
{-#LANGUAGE DeriveAnyClass #-}
module MaterialistDesign where
import Prelude hiding (filter, reverse, take,drop,length, unzip, zipWith, zip, (++), head, tail, sum)
import           Data.Simulation.Solutions.Genome
import Debug.Trace
import Data.Data
import GHC.Generics
import Data.Word
import qualified Data.Foldable as DF
import Control.Lens hiding ((:<))
import Control.Parallel.Strategies
import Data.Simulation.Solutions.GeneticMaterial

import Data.Simulation.Internal.Config (Configuration)

import Data.Hashable
import qualified Data.Vector as V (Vector)
import qualified Data.Vector.Unboxed as VU
import Data.Vector.Generic as V hiding (Vector)
import Data.Simulation.Types
import Data.Simulation.Population
import Data.Simulation.GeneticAlgorithms.GeneticAlgorithmPopulation
import Data.Simulation.Solutions.Chromosomes

import           Data.Simulation.Solutions.Individual
import           Data.Simulation.Solutions.Genes
import Data.Simulation.Domains.SymbolicRegression.Operations

import Data.Random
import Data.Random.Distribution.Bernoulli
import Data.Random.Distribution.Poisson
import Data.Random.Distribution.Normal
import Data.Random.Distribution.Exponential (exponential)
import Polysemy
import Polysemy.RandomFu
import Control.Effects.Cache
import Control.Lens.Plated (Plated, transform)
class (IsGeneticMaterial a) => HasDeterministicMutations a where




\end{code}
}

\section{Genetic material subject to evolution}
Once stochastic events can occur, such as during \gls{reproduction}, a \gls{genotype} is subject to \gls{mutation}.

%\subimport{Data/Simulation/GeneticAlgorithms/Mutations}{PointMutations.lhs}
%\subimport{Data/Simulation/GeneticAlgorithms/Mutations}{ScalableConstants.lhs}



\section{Cartesian genetic programming}

We now begin our \gls{gp} implementation:

\begin{code}





-- TODO FIXME convert word8 to word16 or int
data Instruction = Instruction !Operator !Word8 !Word8 !Double !Double deriving (Eq, Ord, Show, Read, Generic, Data, Typeable, NFData)


instance HasGenes Instruction where
    {-#SCC genes #-}
    {-#INLINE genes #-}
    genes (Instruction op a b c d) = AVector $! fromListN 5 [AGene op, AGene a,
        AGene b, AGene c, AGene d]
    {-#SCC constructFromGenes #-}
    {-#INLINABLE constructFromGenes #-}
    constructFromGenes g = let [op,a,b,c,d] = g ^.. taking 5 traverse in
        Instruction <$> op ^? _AGene <*> a ^? _AGene <*> b ^? _AGene <*> c ^? _AGene <*> d ^? _AGene

instance IsGene Instruction where
    {-#INLINE supportsMutation #-}
    supportsMutation (PointErrors _  _) _ = Always
    supportsMutation (ConstantScale _ _) _ = Always
    supportsMutation (NormalDrift _ _ _) _ = Always
    {-#SCC mutate #-}
    {-#INLINABLE mutate #-}
    mutate (SpecialToConstant rate) instr@(Instruction E a b _ d) = do
        doIt <- sampleRVar (bernoulli rate)
        if doIt then return (Instruction Constant a b e d) else return instr
    mutate (SpecialToConstant rate) instr@(Instruction Pi a b _ d) = do
        doIt <- sampleRVar (bernoulli rate)
        if doIt then return (Instruction Constant a b pi d) else return instr
    mutate ty a = defaultMutate ty a
    {-#INLINABLE initialiseWith #-}
    {-#SCC initialiseWith #-}
    initialiseWith p = do
        op <- initialiseWith p
        a <- sampleRVar (poisson (2 :: Float))
        b <- sampleRVar (poisson (2 :: Float))
        c <- sampleRVar (normal 1 10)
        d <- sampleRVar (normal 1 10)
        return $ Instruction op a b c d

\end{code}
\comment{
\section{Phenomes}
A phenome is the \glslink{geneexpression}{expressed} result of a \gls{gene}.
\begin{code}
class IsPhenome g p where
    centralDogma :: Lens' Genome Phenome

data Phenome where
    APhenome :: IsPhenome g p => g p -> Phenome
\end{code}
%\subimport{./Data/Simulation/GeneticAlgorithms}{Chromosomes.lhs}
}
Keeping on with our Cartesian genetic programming system:
\begin{code}

type CartesianGeneExpression = V.Vector Instruction

\end{code}


Finally we get to evaluation of our Cartesian computations.
\begin{code}

type InstrEnv = Int -> Double

data ExpressionTree = Add' ExpressionTree ExpressionTree
                    | Sub' ExpressionTree ExpressionTree
                    | Mult' ExpressionTree ExpressionTree
                    | Div' ExpressionTree ExpressionTree
                    | Pow'  ExpressionTree ExpressionTree
                    | Neg' ExpressionTree
                    | Const' !Double
                    | Var' !Int
                    | Sin' ExpressionTree
                    | Cos' ExpressionTree
                    | Tan' ExpressionTree
                    | Exp' ExpressionTree
                    | Pi'
                    | E'
                    deriving (Eq, Ord, Show, Read, Generic, Data, NFData)
instance Plated ExpressionTree
instance Hashable ExpressionTree

{-#SCC simplifyExprTree #-}
{-#INLINABLE simplifyExprTree #-}
simplifyExprTree :: ExpressionTree -> ExpressionTree
simplifyExprTree (Add' a' (Neg' b')) = simplifyExprTree (Sub' a' b')
simplifyExprTree (Add' (Neg' b') a') = simplifyExprTree (Sub' a' b')
simplifyExprTree (Add' (Add' a b) c) = simplifyExprTree (Add' a (Add' b c))
simplifyExprTree (Add' a' b') =
    let a = (simplifyExprTree a')
        b = (simplifyExprTree b')
    in
        if a > b
        then
            Add' b a
        else
            Add' a b
simplifyExprTree (Cos' x) = Cos' (simplifyExprTree x)
simplifyExprTree (Sin' x) = Sin' (simplifyExprTree x)
simplifyExprTree (Tan' x) = Tan' (simplifyExprTree x)
simplifyExprTree (Sub' a' b') =
    let a = (simplifyExprTree a')
        b = (simplifyExprTree b')
    in
        case b of
            (Neg' x) -> simplifyExprTree (Add' a x)
            _ -> Sub' a b
simplifyExprTree (Pow' a' b') =
    let a = simplifyExprTree a'
        b = simplifyExprTree b'
    in Pow' a b
simplifyExprTree (Neg' (Const' x)) = Const' (-x)
simplifyExprTree (Neg' (Neg' a)) = simplifyExprTree a
simplifyExprTree (Neg' a') = let a = simplifyExprTree a' in
    case a of
        (Neg' b) -> b
        _ -> a
simplifyExprTree (Mult' (Neg' a) b) = simplifyExprTree (Neg' (Mult' a b))
simplifyExprTree (Mult' a (Neg' b)) = simplifyExprTree (Neg' (Mult' a b))
simplifyExprTree (Mult' (Mult' a b) c) = simplifyExprTree (Mult' a (Mult' b c))
simplifyExprTree (Mult' a' b') =
    let a = (simplifyExprTree a')
        b =  (simplifyExprTree b')
    in
        if a > b
        then
            Mult' b a
        else
            Mult' a b
simplifyExprTree (Exp' a) = Exp' (simplifyExprTree a)
simplifyExprTree (Div' a' b') = Div' (simplifyExprTree a') (simplifyExprTree b')
simplifyExprTree z = z

alphaRename numVars = transform $ \case
    Var' i -> Var' (i `mod` numVars)
    x -> x

{-#INLINABLE compile #-}
{-#SCC compile #-}
compile :: Int -> CartesianGeneExpression -> ExpressionTree
compile numVars xs  | codeLength == 0 = error "compile: Empty program"
            | otherwise = simplifyExprTree $ alphaRename numVars $ head cached where -- = compile' xs where --head cached where
    cached = let theCache = fmap compiled (V.fromListN codeLength [0.. fromIntegral codeLength -1]) in theCache :: V.Vector ExpressionTree
    codeLength = V.length xs
    isNOP (Instruction NOP _ _ _ _) = 1 :: Int
    isNOP _ = 0
    nops = fmap isNOP xs
    {-#SCC adjustOffset #-}
    -- adjustOffset 0  _ = error "adjustOffset: 0 offset!"
    adjustOffset initialOffset pos = adjustOffset' initialOffset (nopsBetween initialOffset pos) where
        -- adjustOffset' 0 _ = error "0 offset!"
        adjustOffset' advanced 0 = advanced
        adjustOffset' advanced n | pos + advanced + n >= codeLength = codeLength - pos
        adjustOffset' advanced n = adjustOffset' (advanced + n) (nopsBetween advanced (pos + advanced + n))

 {-    nopsBetween offset pos | offset <= 0 = error ("weird offset in nopsBetween! offset: " <> show offset <> "\npos: " <> show pos <> "\ncodeLength: "
        <> show codeLength)
    nopsBetween offset pos | pos < 0 = error ("weird pos in nopsBetween! offset: " <> show offset <> "\npos: " <> show pos <> "\ncodeLength: "
        <> show codeLength)
    nopsBetween offset pos | pos >= codeLength = error ("weird pos in
    nopsBetween! offset: " <> show offset <> "\npos: " <> show pos <>
    "\ncodeLength: "  <> show codeLength) -}
    {-#SCC nopsBetween #-}
    nopsBetween offset pos = let count =  V.sum $ V.slice pos (min offset (codeLength - pos )) nops in count
    {-#SCC handleInputs' #-}
    handleInputs' _ pos | pos >= codeLength = error ("pos too long in handleInputs'! pos: " <> show pos)
    handleInputs' offset' pos =
        let (offset :: Int) = fromIntegral $ if offset' == maxBound then maxBound else offset' + 1
            res = (if
                (offset + pos ) >= codeLength
            then
                Var'  offset
            else
                let adjusted = adjustOffset offset pos in

                if (adjusted + pos + 1) >= codeLength then
                    Var' adjusted
                else
                    (cached ! (pos + adjusted) ))
        in res
    {-#SCC compiled #-}
    compiled pos | pos >= codeLength = error ("pos overflow in compiled! pos: " <> show pos)
    compiled pos = let (Instruction op a b c d) = xs ! (fromIntegral pos) in
        case op of
            Add -> Add' (handleInputs' a pos) (handleInputs' b pos)
            Subtract -> Sub' (handleInputs' a pos) (handleInputs' b pos)
            Multiply -> Mult' (handleInputs' a pos) (handleInputs' b pos)
            Divide -> Div' (handleInputs' a pos) (handleInputs' b pos)
            Power -> Pow' (handleInputs' a pos) (handleInputs' b pos)
            Negate -> Neg' (handleInputs' a pos)
            Constant -> Const' c
            InputVariable -> Var' (fromIntegral a)
            Sin -> Sin' (handleInputs' a pos)
            Cos -> Cos' (handleInputs' a pos)
            Tan -> Tan' (handleInputs' a pos)
            Exp -> Exp' (handleInputs' a pos)
            Pi -> Pi'
            E -> E'
            ID -> (handleInputs' a pos)
            NOP -> (handleInputs' 1 pos)

    {-compile' v  | V.length v == 0 = error "compile: Empty list"
                | otherwise = let
                    (Instruction op a b c d) = head v
                    instrs = tail v
                in
                    case op of
        Add -> Add' (handleInputs a instrs) (handleInputs b instrs)
        Subtract -> Sub' (handleInputs a instrs) (handleInputs b instrs)
        Multiply -> Mult' (handleInputs a instrs) (handleInputs b instrs)
        Divide -> Div' (handleInputs a instrs) (handleInputs b instrs)
        Power -> Pow' (handleInputs a instrs) (handleInputs b instrs)
        Negate -> Neg' (handleInputs a instrs)
        Constant -> Const' c
        InputVariable -> Var' (fromIntegral a)
        Sin -> Sin' (handleInputs a instrs)
        Cos -> Cos' (handleInputs a instrs)
        Tan -> Tan' (handleInputs a instrs)
        Exp -> Exp' (handleInputs a instrs)
        Pi -> Pi'
        E -> E'
        ID -> (handleInputs a instrs)
        NOP -> (handleInputs 1 instrs)-}
    --numberOfNOPs ptr instrs = V.length $ instrs ^. (sliced 0 (fromIntegral ptr)) & V.filter isNOP

{-    handleInputs !ptr instrs = let offset = ((fromIntegral ptr) + numberOfNOPs ptr instrs) in if fromIntegral ptr >= V.length instrs || offset >= V.length instrs then (Var' (fromIntegral ptr)) else compile' (drop offset instrs)-}

e = exp 1
{-#INLINABLE evalExprTree #-}
{-#SCC evalExprTree #-}
evalExprTree :: InstrEnv -> ExpressionTree -> Double
evalExprTree env (Add' !a !b) = {-#SCC "Addition" #-}
                                let a' = (evalExprTree env a)
                                    b' = (evalExprTree env b)
                                in a' + b'
evalExprTree env (Sub' !a !b) = {-#SCC "Subtraction" #-}
                                let a' = (evalExprTree env a)
                                    b' = (evalExprTree env b)
                                    in a' - b'
evalExprTree env (Mult' !a !b) = {-#SCC "Multiplication" #-}
                                let a' = (evalExprTree env a)
                                    b' = (evalExprTree env b)
                                 in a' * b'
evalExprTree env (Div' !a !b) = {-#SCC "Division" #-}
                                let a' = (evalExprTree env a)
                                    b' = (evalExprTree env b)
                                in a' / b'
evalExprTree env (Pow' !a !b) = {-#SCC "Power" #-}
                                let a' = (evalExprTree env a)
                                    b' = (evalExprTree env b)
                                in a' ** b'
evalExprTree env (Neg' !a) = {-#SCC "Negation" #-}let a' = (evalExprTree env a) in -a'
evalExprTree _ (Const' !a) = {-#SCC "Constant" #-}a
evalExprTree env (Var' !i) = {-#SCC "Variable" #-}env i
evalExprTree env (Sin' !a) = {-#SCC "Sine" #-}let a' = (evalExprTree env a) in sin a'
evalExprTree env (Cos' !a) = {-#SCC "Cosine" #-}let a' = (evalExprTree env a) in cos a'
evalExprTree env (Tan' !a) = {-#SCC "Tangent" #-}let a' = (evalExprTree env a) in tan a'
evalExprTree env (Exp' !a) = {-#SCC "Exponential" #-}let a' =  (evalExprTree env a) in exp a'
evalExprTree _ Pi' = {-#SCC pi #-}pi
evalExprTree _ E' = {-#SCC e #-}e




data CompiledCGE = CompiledCGE {_opcodeForm :: CartesianGeneExpression, _astForm
    :: ExpressionTree, _astHash :: {-# UNPACK #-} !Int } deriving (Ord, Show,
    Generic, NFData)
instance Eq CompiledCGE where
    (CompiledCGE o a h) == (CompiledCGE o' a' h') = h == h' && a == a' && o == o'

opcodeForm :: Lens' CompiledCGE CartesianGeneExpression
opcodeForm f (CompiledCGE o _ _) = fmap (\o' -> let p = compile 1 o' in --FIXME assumes 1 var
    CompiledCGE o' p (hash p) ) (f o)
--astForm :: Getter CompiledCGE ExpressionTree
--astForm f (CompiledCGE _ a _) = fmap (CompiledCGE o) (f a)


instance HasChromosomes CompiledCGE where
    {-#SCC toChromosomes #-}
    {-#INLINE toChromosomes #-}
    toChromosomes a = ASingleton $! AChromosome $! a^.opcodeForm
    {-#INLINE constructFromChromosomes #-}
    {-#SCC constructFromChromosomes  #-}
    constructFromChromosomes chromosomes =
        do
            cge <- chromosomes ^? traversed._AChromosome
            let ast = compile 1 cge
                h = hash ast
            return (CompiledCGE cge ast h)
    {-#INLINE recombineGenome #-}
    {-#SCC recombineGenome #-}
    recombineGenome c = recombinationOverGenomeImpl @CartesianGeneExpression c


{-#SPECIALISE INLINE mateWithDefault :: forall t r. (Traversable t, Members [RandomFu, Cache ExpressionTree Double] r) => Configuration -> Environment CompiledCGE ->
    TraversableContainer (MutationKind, MutationTarget) -> t
    CompiledCGE -> Sem r (TraversableContainer CompiledCGE) #-}
instance IsIndividual CompiledCGE where
    type Score CompiledCGE = Double
    type PhenotypeType CompiledCGE = ExpressionTree
    type Environment CompiledCGE = VU.Vector Double
    mateWith = mateWithDefault
    {-#INLINE getLocation #-}
    getLocation _ = ()

    {-#SPECIALISE INLINE similarTo :: Configuration -> TraversableContainer CompiledCGE -> CompiledCGE -> Sem r Bool #-}
    similarTo _ parents critter = return $ DF.any (\parent -> _astHash critter == _astHash parent) parents

    {-#SCC createIndividual #-}
    {-#INLINE createIndividual #-}
    createIndividual (InitialiseIndividualWith (Just !l)) = do
        instrs <- V.replicateM (fromIntegral l) (initialiseWith @Instruction (Initialise 10))
        let compiled = compile 1 instrs
        return (CompiledCGE instrs compiled (hash compiled) )
    createIndividual (InitialiseIndividualWith Nothing) = do
        len <- sampleRVar (exponential (0.1 :: Float))
        createIndividual (InitialiseIndividualWith (Just (ceiling len)))
    {-#INLINABLE fitness #-}
    fitness _ fitAgainst (CompiledCGE _ expr _) = if isNaN score then neginf else -score where
        vals = {-#SCC "evaluation" #-}V.map (\(!x) -> evalExprTree (const x) expr ) fitnessxs
        score= (lp2 fitAgainst vals)
    -- TODO HACK: This is an ugly hack. The fitness function should be given a landscape, in addition to a reference
    {-#INLINABLE approximatedFitness #-}
    approximatedFitness _ fitAgainst (CompiledCGE _ expr _) = if isNaN score then neginf else -score where
        vals = {-#SCC "approximated_evaluation" #-}V.map (\(!x) -> evalExprTree (const x) expr ) samplexs
        score= (lp2 fitAgainst vals)

    {-#INLINABLE getPhenotype #-}
    getPhenotype (CompiledCGE _ a _) = a

neginf = -(read "Infinity")::Double

{-#INLINABLE lp2 #-}
lp2 :: VU.Vector Double -> VU.Vector Double -> Double
lp2 !a !b = sum $ zipWith (\(!x) (!y) -> (x-y)*(x-y)) a b



{-# SCC fitnessxs #-}
fitnessxs :: VU.Vector Double
fitnessxs = fromList [0, 0.5 .. 250]
{-# SCC samplexs #-}
samplexs :: VU.Vector Double
samplexs = fromList [1, 7, 9, 55.3, 87.654, 100, 420.69]
{-#SPECIALISE INLINE mateWith :: Members [RandomFu, Cache ExpressionTree Double] r => Configuration -> Environment CompiledCGE -> TraversableContainer (MutationKind, MutationTarget) -> TraversableContainer CompiledCGE -> Sem r (TraversableContainer CompiledCGE) #-}
{-SPECIALISE INLINE reproduction :: Configuration ->
    -> GeneticAlgorithmPopulation IO CompiledCGE -> IO (GeneticAlgorithmPopulation IO CompiledCGE) -}
\end{code}

\comment{

-- TODO: Also: an excision operator to yeet an operon into a new chromosome, and a translocation operator to move/copy to an extant chromosome
-- TODO: Also, plasmids
-- TODO: Phenotype, population, fitness, "can mate with" (e.g. sexual reproduction), gene expression mechanisms

Note to self: plasmid acceptance itself should be an evolvable parameter
17:53
Actually, so should most evolution parameters (recombination frequency, mutation rate, etc)
So the gene expression machinery evolves itself. That way, multiple lineages could evolve symbiotically, forming a true ecosystem
Although how the fitness function would work with that, I have no idea. Perhaps don't cull based solely on fitness, or give "fitness points" to donor organisms when a fit individual is found and its plasmids remember the original donors
18:03
Or have "food". This way long genomes could be crowned upon if simpler genomes can do the same thing
Ooh or just have multiple populations, with a "primary" one with the real fitness function

18:07
Individual populations having building block generation/simple subproblems to do
}