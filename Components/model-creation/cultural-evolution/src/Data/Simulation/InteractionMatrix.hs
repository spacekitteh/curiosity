module Data.Simulation.InteractionMatrix where

import           Data.Functor.Const
import           Numeric.Natural
import           Data.Kind

class IsInteractionMatrix i where
    type ProbType i :: *
    type ProbType i = Double
    -- | Give the probability of two populations interacting at the given time.
    probabilityOfInteraction :: i -> Natural -> Int -> Int -> ProbType i

instance forall a (b :: Type) . IsInteractionMatrix (Const a b) where
    type ProbType (Const a b) = a

    probabilityOfInteraction (Const a) _ _ _ = a

data NonInteractingPopulations = NonInteractingPopulations deriving (Eq, Ord, Show)

instance IsInteractionMatrix NonInteractingPopulations where
    type ProbType NonInteractingPopulations = Double

    probabilityOfInteraction _ _ i j = if i == j then 1 else 0

data AdjacentPopulationsModel = AdjacentPopulationsModel !Int !Double deriving (Eq, Ord, Show)

instance IsInteractionMatrix AdjacentPopulationsModel where
    type ProbType AdjacentPopulationsModel = Double

    probabilityOfInteraction (AdjacentPopulationsModel range prob) _ i j =
        if abs (i - j) <= range then prob else 0

class IsInteractionMatrix i => CombinableInteractionMatrix i where
    convolve :: i -> i -> i
    mask :: i -> i -> i
