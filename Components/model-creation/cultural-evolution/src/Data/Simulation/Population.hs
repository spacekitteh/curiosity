{-#LANGUAGE Haskell2010, DefaultSignatures, TypeApplications, MultiParamTypeClasses, DuplicateRecordFields, DeriveAnyClass, BangPatterns, DerivingStrategies, GADTs, GeneralisedNewtypeDeriving, DeriveLift, DeriveFoldable,DeriveTraversable, RankNTypes #-}
module Data.Simulation.Population where
import           Data.Simulation.Solutions.Individual
import           Data.Simulation.Types
import           Prelude                 hiding ( zip
                                                , take
                                                , unzip
                                                , filter
                                                , (++)
                                                , head
                                                )
import qualified Data.Foldable                 as DF
import           Data.Word
import           GHC.Exts                       ( oneShot )
import qualified Data.Vector.Unboxed           as VU
                                                ( Vector )
import           Control.Monad.Primitive
import           GHC.Types                      ( SPEC(..) )
import           Control.Lens
import           Numeric.IEEE                   ( epsilon
                                                , maxNum
                                                , IEEE
                                                )
import           Data.Vector                   as V
                                                ( Vector )
import           Data.Vector.Generic           as V
                                         hiding ( mapM
                                                , snoc
                                                , Vector
                                                , fromList
                                                )
import           Numeric.Log                   as NL
import           Data.Simulation.Internal.Config
                                               as Config
                                         hiding ( reproduction
                                                , fitness
                                                )
import           Numeric.Natural
import           Debug.Trace
import           Polysemy
import           Data.Random
import           Polysemy.RandomFu
import           Data.Random.Distribution.Categorical
import           Control.Effects.Cache

type PopulationEffects a r
    = Members
          '[RandomFu, Cache
              (PhenotypeType (IndividualType a))
              (Score (IndividualType a))]
          r

type ReproductiveProbabilityTable a
    = ( Categorical (Score (IndividualType a)) (IndividualType a)
      , TraversableContainer (Score (IndividualType a), IndividualType a)
      )
class IsPopulation a where
    type IndividualType a
    individuals :: Traversal' a (IndividualType a)
    populationSize :: a -> PopulationSize
    --cull :: a -> IndividualType a -> Maybe a
    evaluate :: (res ~ (TraversableContainer (Score (IndividualType a), IndividualType a)), PopulationEffects a r) => Configuration -> a -> Sem r res
    -- | Standardised scores are when lower is better.
    --   The default implementation just negates the scores.
    standardisedScores :: TraversableContainer (Score (IndividualType a), IndividualType a) -> TraversableContainer (Score (IndividualType a), IndividualType a)
    {-#INLINABLE standardisedScores #-}
    {-#SCC standardisedScores #-}
    default standardisedScores :: Num (Score (IndividualType a)) => TraversableContainer (Score (IndividualType a), IndividualType a) -> TraversableContainer (Score (IndividualType a), IndividualType a)
    standardisedScores = fmap (\(!a,!b) -> (-a, b))
    -- | Adjusted fitness lies between 0 and 1. Bigger is better.
    --  Input is standardised score.
    adjustedScores :: TraversableContainer (Score (IndividualType a), IndividualType a) -> TraversableContainer (Score (IndividualType a), IndividualType a)
    {-#SCC adjustedScores #-}
    {-#INLINABLE adjustedScores #-}
    default adjustedScores :: IEEE (Score (IndividualType a)) => TraversableContainer (Score (IndividualType a), IndividualType a) -> TraversableContainer (Score (IndividualType a), IndividualType a)
    adjustedScores = fmap (\(!a,!b) -> (maxNum epsilon (1/(1+a)), b))

    -- | Adjusted scores normalised compared to the whole population.
    -- Input is adjusted scores.
    normalisedScores :: TraversableContainer (Score (IndividualType a), IndividualType a) -> TraversableContainer (Score (IndividualType a), IndividualType a)
    {-#SCC normalisedScores #-}
    {-#INLINABLE normalisedScores #-}
    default normalisedScores :: Fractional (Score (IndividualType a)) => TraversableContainer (Score (IndividualType a), IndividualType a) -> TraversableContainer (Score (IndividualType a), IndividualType a)
    normalisedScores adj = let total = DF.sum (fmap fst adj) in fmap (\(!a,!b) -> (a/total, b)) adj where
    {-#SCC assignProbabilitiesToIndividuals #-}
    {-#INLINABLE assignProbabilitiesToIndividuals #-}
    assignProbabilitiesToIndividuals :: TraversableContainer (Score (IndividualType a), IndividualType a) -> TraversableContainer (Probability, IndividualType a)
    default assignProbabilitiesToIndividuals :: Score (IndividualType a) ~ Double => TraversableContainer (Score (IndividualType a), IndividualType a) -> TraversableContainer (Probability, IndividualType a)
    assignProbabilitiesToIndividuals t =
        let
            adj = (fmap (\(!a,!b) -> (weightToProb a, b)))
                    . adjustedScores @a
                    . standardisedScores @a $ t
            total = NL.sum (fmap fst adj)
        in
            fmap (\(!a,!b) -> (a/total, b)) adj

    {-#SCC reproductionProbabilityTable #-}
    {-#INLINE reproductionProbabilityTable #-}
    reproductionProbabilityTable :: (res ~ ReproductiveProbabilityTable a, PopulationEffects a r) => Configuration ->  a -> Sem r res
    default reproductionProbabilityTable :: (res ~ ReproductiveProbabilityTable a, PopulationEffects a r, Fractional (Score (IndividualType a)), Eq (Score (IndividualType a)))  => Configuration ->  a -> Sem r res
    reproductionProbabilityTable config a = do
        raw <- evaluate config a
        let std = standardisedScores @a raw
            evaluated = V.toList . toVec . normalisedScores @a . adjustedScores @a $ std
        return (fromWeightedList evaluated, std)

    spawnNewPopulation :: PopulationEffects a r => Config.Configuration -> Environment (IndividualType a) -> Environment (IndividualType a) -> Sem r a

    selectIndividualsForReproduction :: PopulationEffects a r => Configuration -> Natural -> a -> Sem r (TraversableContainer (IndividualType a))
    default selectIndividualsForReproduction :: (Distribution
                          Uniform (Score (IndividualType a)),Ord (Score (IndividualType a)),Fractional (Score (IndividualType a)), PopulationEffects a r) => Configuration -> Natural -> a -> Sem r (TraversableContainer (IndividualType a))
    selectIndividualsForReproduction config num a
        = do
            (table', _) <-reproductionProbabilityTable config a

            let table = rvar table'

            parents  <- {-#SCC "selecting_parents" #-}sampleRVar $ V.replicateM (fromIntegral num) table
            return (AVector parents)

    addIndividualsToPopulation :: (PopulationEffects a r, Traversable t) => Configuration -> a -> t (IndividualType a) -> Sem r a

    -- | Create a new generation of critters.
    reproduction :: PopulationEffects a r => Configuration ->  a -> Sem r a



{-#SPECIALISE INLINE adjustedScores :: TraversableContainer (Double, a) ->
    TraversableContainer (Double, a) #-}
{-#SPECIALISE INLINE normalisedScores :: TraversableContainer (Double, a) ->
    TraversableContainer (Double, a) #-}
{-#SPECIALISE INLINE standardisedScores :: TraversableContainer (Double, a) ->
    TraversableContainer (Double, a) #-}


data Population where
    APopulation ::IsPopulation a => a -> Population



{-# INLINABLE iterateGenerations #-}
iterateGenerations :: Monad m => Configuration -> Int -> (a -> m a) -> a -> m a
iterateGenerations config i f a' = iterateGenerations' SPEC i a'  where
    iterateGenerations' _ 0 !a = return a
    iterateGenerations' !sPEC !n !a =
        let !v = (f a)
        in  (logData config
                     (population . generationNumber)
                     ("Generation: " <> (show (i - n)))
                     v
            )
                >>= (GHC.Exts.oneShot (iterateGenerations' sPEC (n - 1)))

{-# INLINE reproduceFor #-}
reproduceFor
    :: (IsPopulation a, PopulationEffects a r)
    => Int
    -> Configuration
    -> a
    -> Sem r a
reproduceFor i config = iterateGenerations config i (reproduction config)
