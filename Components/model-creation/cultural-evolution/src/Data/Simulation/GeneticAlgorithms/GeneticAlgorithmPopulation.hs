{-#LANGUAGE Haskell2010, DefaultSignatures, TypeApplications, MultiParamTypeClasses, DuplicateRecordFields, DeriveAnyClass, BangPatterns, DerivingStrategies, GADTs, GeneralisedNewtypeDeriving, DeriveLift, DeriveFoldable,DeriveTraversable, RankNTypes #-}
module Data.Simulation.GeneticAlgorithms.GeneticAlgorithmPopulation where
import           Data.Simulation.Population
import           Data.Simulation.Solutions.Individual
import           Data.Simulation.Types
import           Data.Simulation.Internal.Utils
import           Prelude                 hiding ( zip
                                                , take
                                                , unzip
                                                , filter
                                                , (++)
                                                , head
                                                )
import qualified Data.List                     as DL
import           Data.Word
import qualified Data.Vector.Unboxed           as VU
                                                ( Vector )
import           Control.Monad.Primitive
import Control.Monad (when, liftM, guard)
import           Data.Hashable
import qualified Data.HashMap.Lazy             as HM
import qualified Data.HashMap.Strict           as HMS
import           Control.Lens
import           Control.Parallel.Strategies
import Data.Maybe (fromJust, isNothing)
import           Data.Vector                   as V
                                                ( Vector )
import           Data.Vector.Generic           as V
                                         hiding ( mapM
                                                , snoc
                                                , Vector
                                                , fromList
                                                )
import           Debug.Trace
import Data.Tuple (swap)
import           Data.Simulation.Internal.Config
                                               as Config
                                         hiding ( reproduction
                                                , fitness
                                                )
import Control.Effects.Cache
import Polysemy hiding (run)
import Data.Witherable
data GeneticAlgorithmPopulation a where
    -- TODO: Use a finger trie as a cache for DAG-style phenotypes
    AGeneticAlgorithmPopulation ::(IsIndividual a, Hashable (PhenotypeType a)) => {_ourMutations :: TraversableContainer (MutationKind, MutationTarget), _config :: Config.Configuration, _individuals :: (Vector a), _environment :: Environment a, _sampleEnvironment :: Environment a} -> GeneticAlgorithmPopulation a

instance (Show a,  Show (Score a)) => Show (GeneticAlgorithmPopulation a) where
    show pop = show (_individuals pop)
instance (NFData a, Score a ~ Double, IsIndividual a, Hashable (PhenotypeType a), Eq (PhenotypeType a), Double ~ (Score (IndividualType (GeneticAlgorithmPopulation a)))) => IsPopulation (GeneticAlgorithmPopulation a) where
    type IndividualType (GeneticAlgorithmPopulation a) = a
    {-# INLINABLE populationSize #-}
    populationSize pop = fromIntegral $ V.length (_individuals pop)
    {-#SCC evaluate #-}
    {-# INLINE evaluate #-}
    evaluate config pop = case config ^. run . cache . phenotypes of
        True ->
            do
                let critters = _individuals pop
                lookups <- forM critters (\i -> do
                    lookup <- isCached (getPhenotype i)
                    return (fromJust lookup, i))
                return (AVector lookups)

        _ ->
            let env = _environment pop
            in return $  AVector $ parFmapVec
                    (\(!i) -> (fitness (_config pop) env i, i))
                    (_individuals pop)
    {-# INLINABLE spawnNewPopulation #-}
    spawnNewPopulation config env sampleEnv = do
        let size          = config ^. population . startingPopulation
            cacheResults  = config ^. run . cache . phenotypes
            critterParams = makeCritterParams config

        individuals <- replicateM (fromIntegral size)
                                  (createIndividual critterParams)
        let evald = withStrategy (parVector rdeepseq) $ fmap (\i -> (fitness config env i, i)) individuals
            initialCritters = fmap (\(a, b) -> (getPhenotype b, a))
                              (evald ^.. folded)
            muts = initialiseMutationSettings config
        storeListInCache initialCritters
        return  $ AGeneticAlgorithmPopulation muts
                                                config
                                                individuals
                                                env
                                                sampleEnv


--TODO: Implement cache sharing between independent runs
    {-# INLINABLE reproduction #-}
    reproduction config a@(AGeneticAlgorithmPopulation ourMutations _ critters  env sampleEnv)
        = do
            parents <- selectIndividualsForReproduction config (fromIntegral num) a
            doProgeny (toVec parents)
      where
        num  = V.length critters

        doProgeny parents = do
            newPop <- fmap toVec $ mateWith config sampleEnv ourMutations (AVector parents) -- FIXME TODO this is destroying genetic diversity. Broods should not be mixed before culling.
            when (config ^. run . cache . phenotypes)
                do
                    toCache <- (flip witherM) newPop $ (\i -> do
                        let p = getPhenotype i
                        m <- isCached p
                        if isNothing m
                            then return $ Just (p, fitness config env i)
                            else return Nothing)
                    storeListInCache (toList $ logData config (population . newPhenotypes)
                                (  "New phenotypes this generation: "
                                <> show (V.length toCache) -- TODO optimise this
                                ) toCache)

            return (AGeneticAlgorithmPopulation
                ourMutations
                config
                newPop
                env
                sampleEnv)






initialiseMutationSettings
    :: Config.Configuration
    -> TraversableContainer (MutationKind, MutationTarget)
initialiseMutationSettings config =
    let mutSettings = config ^. environment . mutations
    in
        AVector $ fromListN
            5
            [ ( PointErrors (mutSettings ^. imperfectCopying . rate)
                            (Run 0 10000)
              , MutateGenes
              )
            , ( NormalDrift (mutSettings ^. normalDrift . rate)
                            (mutSettings ^. normalDrift . mean)
                            (mutSettings ^. normalDrift . standardDeviation)
              , MutateGenes
              )
            , ( Reversal (mutSettings ^. reversal . rate) (Run 0 0)
              , MutateChromosomes
              )
            , ( NewRandomStretch
                  (mutSettings ^. newRandomStretch . rate)
                  (mutSettings ^. newRandomStretch . preservationRate)
                  Nothing
                  (Initialise 1)
              , MutateChromosomes
              )
            , ( SpecialToConstant (mutSettings ^. specialToConstant . rate)
              , MutateGenes
              )
            ]
