\comment{
\begin{code}
module Data.Simulation.GeneticAlgorithms.Mutations.ScalableConstants where
\end{code}

}
\chapter{Constant scaling}
Another form of mutation is constant scaling; this helps produce gradual changes to numerical constants.

\begin{code}

class HasConstantsToScale a where
    constantScale :: a -> Double -> a
    default constantScale :: Fractional a => a -> Double -> a
    constantScale a b = a * (fromRational $ toRational b)

\end{code}
