\comment{
\begin{code}
module Data.Simulation.GeneticAlgorithms.Mutations.PointMutations where
import Data.Simulation.Types
import Data.Simulation.Internal.Utils
import           Data.Simulation.Solutions.GeneticMaterial
import Data.Word
import qualified Data.Binary as DB
import qualified Data.Vector as V
import Data.Bits
import Data.Simulation.Domains.SymbolicRegression.Operations

import Polysemy
import Polysemy.RandomFu
import Data.Random
import Data.Random.Distribution.Bernoulli
\end{code}
}

\chapter{Point mutations}
\Glspl{pointmutation} are the classic \gls{mutation} operator in \glspl{ga}. These occur over a stretch of \glspl{nucleotide}, enabling fine-grained control of variability throughout the \gls{genome}.

\begin{code}
class IsGeneticMaterial a => HasPointMutations a where
    {-# SCC generatePointMutationLocations #-}
    {-#INLINE generatePointMutationLocations #-}
    -- |Generate locations to perform point mutations. The default
    -- |implementation generates them via Bernoulli trials.
    generatePointMutationLocations :: Member RandomFu r => ErrorRate -> Run -> a
        -> Sem r NucleotidePositions
    generatePointMutationLocations rate (Run s l) a =
        generateLocationsByBernoulliTrials rate l s (geneticMaterialSize a)

    {-#SCC pointMutations #-}
    {-#INLINE pointMutations #-}
    -- |Perform point mutations on a gene.
    pointMutations :: Member RandomFu r => ErrorRate -> Run -> a -> Sem r a
    pointMutations rate loc a = do
        positions <- generatePointMutationLocations rate loc a
        nucleotides <- generateNucleotides rate positions a
        return $ (substituteNucleotides nucleotides positions) a

    {-#SCC generateNucleotides #-}
    {-#INLINE generateNucleotides #-}
    -- |Generates multiple nucleotides.
    generateNucleotides :: (IsGeneticMaterial a, Member RandomFu r) =>
        ErrorRate -> NucleotidePositions -> a -> Sem r (Nucleotides a)
    generateNucleotides rate positions a = mapM (\pos -> generateNucleotide rate (Just pos) (Just a)) positions

    {-#SCC generateNucleotide #-}
    {-#INLINE generateNucleotide #-}
    -- |Come up with a plausible new nucleotide. The location
    -- |and/or an extant gene may be provided for reference.
    -- |The default implementation uses a uniform distribution,
    -- |but it does not have to be.
    generateNucleotide :: (Member RandomFu r, IsGeneticMaterial a) => ErrorRate
        -> Maybe NucleotidePosition -> Maybe a -> Sem r (Nucleotide a)
    default generateNucleotide :: (Member RandomFu r, Distribution StdUniform (Nucleotide a))
        =>  ErrorRate -> Maybe NucleotidePosition -> Maybe a -> Sem r (Nucleotide a)
    generateNucleotide _ _ _ = sampleRVar stdUniform

\end{code}

\begin{code}
instance (FiniteBits a,Num a, DB.Binary a, Distribution StdUniform a) => HasPointMutations (GrayCoded a)
instance HasPointMutations Word8
instance HasPointMutations Word16
instance HasPointMutations Word32
instance HasPointMutations Word64
instance (Enum a, Bounded a, Distribution StdUniform a) => HasPointMutations (EnumGene a) where
    {-#SCC generatePointMutationLocations #-}
    {-#INLINE generatePointMutationLocations #-}
    generatePointMutationLocations rate _ _ = do
        doIt <- sampleRVar (bernoulli rate)
        if doIt then return (ASingleton 0) else return (AVector V.empty)

--deriving via EnumGene Operator instance HasPointMutations Operator
instance HasPointMutations Operator
\end{code}