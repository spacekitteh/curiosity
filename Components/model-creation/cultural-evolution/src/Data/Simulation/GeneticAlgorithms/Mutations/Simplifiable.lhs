\comment{
\begin{code}
module Data.Simulation.GeneticAlgorithms.Mutations.Simplifiable where
\end{code}

}
\chapter{Simplification}
An occasionally helpful mutation is simplification; this helps to reduce the size of the gene. However, this can easily destroy non-coding regions. Thus, we generalise to arbitrary --- semantics preserving --- rewrites.

\begin{code}

class Rewritable a where
    -- |The result should be identical in terms of denotational semantics.
    rewrite :: a -> a
\end{code}
