module Data.Simulation.Specification.Problem where
import           Control.Lens
import qualified Data.Vector.Generic           as V

import           Polysemy

class Problem a where
    type TestCaseType a :: *
    type ResultType a :: *
    type ScoreType a :: *
    testCases :: Traversal' a (TestCaseType a)
    sampleTestCases :: Traversal' a (TestCaseType a)

    proposeTestCases :: Traversable t => a -> t (TestCaseType a) -> Maybe a
    proposeSampleTestCases :: Traversable t => a -> t (TestCaseType a) -> Maybe a

    presentSolutions :: Traversable t => a -> t (TestCaseType a, ResultType a) -> ScoreType a

{-data ProblemSolver problem solution score m a where
    EstimateScore ::problem -> solution -> ProblemSolver problem solution score m score
    Score ::problem -> solution -> ProblemSolver problem solution score m score
    Propose
-}

data SymbolicRegressionProblem score x y where
    ASymbolicRegressionProblem ::(V.Vector v x, V.Vector v y) => v x -> v y -> (v (x,y) -> score) -> SymbolicRegressionProblem score x y

instance Problem (SymbolicRegressionProblem score x y) where
    type TestCaseType (SymbolicRegressionProblem score x y) = x
    type ResultType (SymbolicRegressionProblem score x y) = y
    type ScoreType (SymbolicRegressionProblem score x y) = score


data FunctionApproximationProblem score x y where
    AFunctionApproximationProblem ::(V.Vector v x, V.Vector v y) => v x -> v y -> (x -> y) -> (v (x,y) -> score) -> FunctionApproximationProblem score x y

instance Problem (FunctionApproximationProblem score x y) where
    type TestCaseType (FunctionApproximationProblem score x y) = x
    type ResultType (FunctionApproximationProblem score x y) = y
    type ScoreType (FunctionApproximationProblem score x y) = score
