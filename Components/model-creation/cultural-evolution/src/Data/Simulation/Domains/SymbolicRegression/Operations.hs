module Data.Simulation.Domains.SymbolicRegression.Operations where
import qualified Data.HashMap.Strict           as HM
import           Language.Haskell.TH.Syntax     ( Lift )
import           Control.Lens.TH
import qualified Data.Aeson                    as JSON
import qualified Data.Aeson.Types              as JSON
                                                ( typeMismatch )
import           GHC.Generics                   ( Generic )
import           Control.DeepSeq                ( NFData )
import           Data.Aeson.TH                 as JSON.TH
import qualified Data.Text                     as Text
import           Data.Data
import           Data.Hashable
import Data.Random
import Data.Random.Distribution.Uniform

data Operator =   Add
                | Subtract
                | Multiply
                | Divide
                | Power
                | Negate
                | Constant
                | InputVariable
                | Sin
                | Cos
                | Tan
                | Exp
                | Pi
                | E
                | ID
                | NOP
    -- Call | MeijerGFunction
    deriving  (Eq, Ord, Show, Read, Enum, Bounded, Generic, Data, Typeable, Lift)
    deriving anyclass (NFData, Hashable)

instance Distribution StdUniform Operator where
    {-#INLINE rvarT #-}
    {-#SCC rvarT "Operator_rvarT" #-}
    rvarT _ = boundedEnumStdUniform



makeClassyPrisms ''Operator

aliases :: HM.HashMap Text.Text Operator
aliases = HM.fromList aliasList  where
    aliases' Add           = ["+", "plus", "add", "addition", "Add"]
    aliases' Subtract = ["-", "minus", "subtract", "subtraction", "Subtract"]
    aliases' Multiply = ["*", "times", "multiply", "multipliation", "Multiply"]
    aliases' Divide        = ["/", "divide", "division", "Divide"]
    aliases' Power         = ["^", "**", "pow", "powers", "Power"]
    aliases' Negate        = ["(-)", "neg", "negate", "negation", "Negate"]
    aliases' Constant      = ["const", "constant", "constants", "Constant"]
    aliases' InputVariable = ["var", "x", "InputVariable"]
    aliases' Sin           = ["sin", "sine", "Sin"]
    aliases' Cos           = ["cos", "cosine", "Cos"]
    aliases' Tan           = ["tan", "tangent", "Tan"]
    aliases' Exp           = ["exp", "exponential", "Exp"]
    aliases' Pi            = ["pi", "Pi"]
    aliases' E             = ["e", "E"]
    aliases' ID            = ["id", "ID"]
    aliases' NOP           = ["nop", "no-op", "NOP"]

    aliasList = concatMap (\op -> [ (alias, op) | alias <- aliases' op ])
                          [minBound .. maxBound]


instance JSON.FromJSON Operator where
    parseJSON val = JSON.withText
        "Data.Simulation.GeneticAlgorithms.Domains.SymbolicRegression.Operations.Operator"
        (\t -> do
            case HM.lookup t aliases of
                Just op -> return op
                Nothing -> JSON.typeMismatch "Operator" val
        )
        val

$(JSON.TH.deriveToJSON defaultOptions ''Operator)
