\chapter{Common types}
\comment{
\begin{code}
{- OPTIONS_GHC -Wno-inline-rule-shadowing -} -- TODO FIXME
{-#LANGUAGE DeriveAnyClass, BangPatterns, DerivingStrategies, GADTs,
GeneralisedNewtypeDeriving, DeriveLift, NoImplicitPrelude#-}
module Data.Simulation.Types (Rate, ErrorRate, URI, CacheResults,
PopulationSize, PreservationRate, MutationKind(..), Run(..),ChromosomeType(..),
MutationTarget(..), GeneInitialisationParameters(..), GeneKind(..),
CrossoverParameters(..), TraversableContainer(..),aSingleton, aVector,
aTraversable, tcAny, tcOf, toVec, toVecTC,vecOf, GrayCoded(..), EnumGene(..),
integralToGray, grayToIntegral, BroodSize, GrayCodedEnumGene(..),
NucleotidePosition, GenePosition, NucleotidePositions,weightToProb,probToWeight,
Probability) where

import Prelude(Show(show), Double, Floating, Read, Enum, Bounded, Eq,(==),(/=), Num,
    Integer, Integral, String, Real, (.), ($!), log, exp, ($), id, )
import Data.Ord (Ord)
import Data.Bool (Bool (False), otherwise)
import Data.Functor (Functor(..), (<$>))
import Data.Foldable (Foldable(..))
import Data.Traversable(Traversable(..))
import Numeric.Natural (Natural)
import qualified Data.Foldable as DF
import GHC.Generics (Generic)
import qualified Data.ByteString as BS
import Data.Bits (Bits, FiniteBits, shiftR, xor)
import Control.Monad (return)
import qualified Data.Binary as DB
import Data.Maybe (Maybe)
--import Data.Sequence
--import qualified Data.Sequence.Lens as DSL
import qualified Data.Vector.Generic.Lens as VGL
import qualified Data.Vector.Lens as VL
import Control.Lens (Getting, traversed, folded, views)
--import Data.Data
import qualified Data.Vector as V (Vector)
import qualified Data.Vector.Generic as V hiding (Vector)
--import Data.Typeable
import Data.Monoid (Endo)
import Control.DeepSeq (NFData)
import Numeric.Log (Log(Exp))

import Language.Haskell.TH.Syntax (Lift)
import Data.Random (Distribution, StdUniform, Uniform)
--import Control.Lens.TH

\end{code}
}
\begin{code}
type Rate = Double
type ErrorRate = Rate
type URI = String -- TODO
type CacheResults = Bool

type PopulationSize = Natural
{- data PopulationParameters = PopulationParameters PopulationSize !CacheResults deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData
data EvaluationParameters = EvaluationParameters deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData
data ReproductionParameters = ReproductionParameters deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData

data RecombinationParameters = RecombinationParameters !Rate deriving (Eq, Ord, Show, Generic, NFData, Lift)-}

type PreservationRate = Double
data MutationKind = PointErrors !ErrorRate !Run
    | Insertion NucleotidePosition !Run BS.ByteString
    | NewRandomStretch !ErrorRate !PreservationRate (Maybe Run) GeneInitialisationParameters
    | Deletion Run
    | Transposition Run NucleotidePosition
    | Rotation (Maybe Run) Integer  -- ^Positive is rotate right
    | Permutation --TODO
    | Complement Run
    | ConstantScale !ErrorRate !Double
    | NormalDrift !ErrorRate !Double !Double
    | Split GenePosition
    | Duplicate GenePosition
    | Reversal !ErrorRate Run
    | Simplify
    | SpecialToConstant !ErrorRate
    deriving stock (Eq, Ord, Show, Read, Generic)

deriving anyclass instance NFData(MutationKind)

--data MutationParameters = ImperfectCopying !ErrorRate deriving (Eq, Ord, Show, Read, Generic, Lift)
--    deriving anyclass NFData
data ChromosomeType = Linear | Circular | Branching deriving (Eq, Ord, Show, Read, Enum, Generic)
data MutationTarget = MutateGenes | MutateChromosomes deriving (Eq, Ord, Enum, Generic)
    deriving anyclass NFData
newtype GeneInitialisationParameters = Initialise Natural deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData

data GeneKind = Atomic | NuclearString | Tree | Graph | ADT | RecursiveADT

newtype CrossoverParameters = OnePoint NucleotidePosition deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData



data TraversableContainer a where
    ATraversable :: Traversable t => (t a) -> TraversableContainer a
--    ASequence :: (Seq a) -> TraversableContainer a
    AVector :: !(V.Vector a) -> TraversableContainer a
    ASingleton :: !a -> TraversableContainer a

{-#SCC aSingleton #-}
{-#INLINE CONLIKE [1] aSingleton #-}
aSingleton :: a -> TraversableContainer a
aSingleton = ASingleton
{-#SCC aVector #-}
{-# INLINE CONLIKE [1] aVector #-}
aVector :: V.Vector a -> TraversableContainer a
aVector = AVector
{-#SCC aTraversable #-}
{-#INLINE CONLIKE [1] aTraversable #-}
aTraversable :: Traversable t => t a -> TraversableContainer a
aTraversable = ATraversable

{-# RULES "aTraversableTwice" forall x. aTraversable x = x #-}
{-# RULES "aTrav/aVec" aTraversable = AVector #-}
-- -# RULES "TC fmap id"  fmap @(TraversableContainer ) id  = id #-}
-- # RULES "TC fmap fuse" forall f g . fmap f . fmap @(TraversableContainer) g = fmap (f . g)  #-}
instance Functor TraversableContainer where
    {-#INLINE fmap #-}
    fmap f (ASingleton a) = {-#SCC "ASingleton" #-}ASingleton (f a)
    fmap f (AVector a) = {-#SCC "AVector" #-}AVector (fmap f a)
    fmap f (ATraversable a) = {-#SCC "ATraversable" #-}ATraversable (fmap f a)

instance Foldable TraversableContainer where
    {-#INLINE fold #-}
    fold (ASingleton a) = {-#SCC "ASingleton" #-}a
    fold (AVector a) = {-#SCC "AVector" #-}DF.fold a
    fold (ATraversable a) ={-#SCC "ATraversable" #-} DF.fold a
    {-#INLINE sum #-}
    sum (ASingleton a) = {-#SCC "ASingleton" #-}a
    sum (AVector a) = {-#SCC "AVector" #-}V.sum a
    sum (ATraversable a) = {-#SCC "ATraversable" #-}DF.sum a
    {-# INLINE product #-}
    product (ASingleton a) ={-#SCC "ASingleton" #-} a
    product (AVector a) = {-#SCC "AVector" #-}V.product a
    product (ATraversable a) = {-#SCC "ATraversable" #-}DF.product a
    {-#INLINE foldr #-}
    foldr f z (ASingleton a) = {-#SCC "ASingleton" #-}f a z
    foldr f z (AVector a) = {-#SCC "AVector" #-}V.foldr f z a
    foldr f z (ATraversable a) = {-#SCC "ATraversable" #-}foldr f z a
    {-#INLINE foldr' #-}
    foldr' f z (ASingleton a) ={-#SCC "ASingleton" #-} id $! f a z
    foldr' f z (AVector a) = {-#SCC "AVector" #-}V.foldr' f z a
    foldr' f z (ATraversable a) = {-#SCC "ATraversable" #-}DF.foldr' f z a
    {-#INLINE foldl #-}
    foldl f z (ASingleton a) ={-#SCC "ASingleton" #-} f z a
    foldl f z (AVector a) = {-#SCC "AVector" #-}V.foldl f z a
    foldl f z (ATraversable a) = {-#SCC "ATraversable" #-}foldl f z a
    {-#INLINE foldl' #-}
    foldl' f z (ASingleton a) ={-#SCC "ASingleton" #-}{-#SCC "ASingleton" #-} id $! f z a
    foldl' f z (AVector a) = {-#SCC "AVector" #-}V.foldl' f z a
    foldl' f z (ATraversable a) ={-#SCC "ATraversable" #-} DF.foldl' f z a
    {-#INLINE foldl1 #-}
    foldl1 _ (ASingleton a) ={-#SCC "ASingleton" #-} a
    foldl1 f (AVector a) = {-#SCC "AVector" #-}V.foldl1 f a
    foldl1 f (ATraversable a) = {-#SCC "ATraversable" #-}DF.foldl1 f a
    {-# INLINE foldr1 #-}
    foldr1 _ (ASingleton   a) ={-#SCC "ASingleton" #-} a
    foldr1 f (AVector      a) = {-#SCC "AVector" #-}V.foldr1 f a
    foldr1 f (ATraversable a) = {-#SCC "ATraversable" #-}DF.foldr1 f a
    {-#INLINE foldMap #-}
    foldMap f (ASingleton a) ={-#SCC "ASingleton" #-} f a
    foldMap f (AVector a) = {-#SCC "AVector" #-}foldMap f a
    foldMap f (ATraversable a) = {-#SCC "ATraversable" #-}foldMap f a
    {-#INLINE null #-}
    null (ASingleton _) = {-#SCC "ASingleton" #-}False
    null (AVector a) = {-#SCC "AVector" #-}V.null a
    null (ATraversable a) = {-#SCC "ATraversable" #-}null a
    {-#INLINE toList #-}
    toList (ASingleton a) ={-#SCC "ASingleton" #-} [a]
    toList (AVector a) = {-#SCC "AVector" #-}V.toList a
    toList (ATraversable a) = {-#SCC "ATraversable" #-}DF.toList a
    {-#INLINE length #-}
    length (ASingleton _) = {-#SCC "ASingleton" #-}1
    length (AVector a) = {-#SCC "AVector" #-}V.length a
    length (ATraversable a) = {-#SCC "ATraversable" #-}DF.length a

    {-#INLINE elem #-}
    elem x (ASingleton y) = {-#SCC "ASingleton" #-}x == y
    elem x (AVector y) = {-#SCC "AVector" #-}V.elem x y
    elem x (ATraversable y) = {-#SCC "ATraversable" #-}elem x y

{-# RULES "tcAny"  forall f tc. DF.any f tc = tcAny f tc #-}
{-#INLINE tcAny #-}
tcAny :: (a -> Bool) -> TraversableContainer a -> Bool
tcAny f (ASingleton a) = {-#SCC "ASingleton" #-}f a
tcAny f (AVector a) = {-#SCC "AVector" #-}V.any f a
tcAny f (ATraversable a) ={-#SCC "ATraversable" #-} DF.any f a
instance Traversable TraversableContainer where
    {-#INLINE traverse #-}
    traverse f (ASingleton a) = {-#SCC "ASingleton" #-}ASingleton <$> f a
    traverse f (AVector a) = {-#SCC "AVector" #-}AVector <$> (traverse f a)
    traverse f (ATraversable a) ={-#SCC "ATraversable" #-} ATraversable <$> (traverse f a)
    {-# INLINE mapM #-}
    mapM f (ASingleton   a) = {-#SCC "ASingleton" #-}{-#SCC "ASingleton" #-}ASingleton <$> f a
    mapM f (AVector a) = {-#SCC "AVector" #-}AVector <$> (V.mapM f a)
    mapM f (ATraversable a) = {-#SCC "ATraversable" #-}ATraversable <$> (mapM f a)
    {-#INLINE sequenceA #-}
    sequenceA (ASingleton a) = {-#SCC "ASingleton" #-}ASingleton <$> a
    sequenceA (AVector a) = {-#SCC "AVector" #-}AVector <$> (sequenceA a)
    sequenceA (ATraversable a) = {-#SCC "ATraversable" #-}ATraversable <$> (sequenceA a)
{-#INLINE tcOf #-}
{-#SPECIALISE INLINE tcOf :: Getting (Endo [a]) (V.Vector a) a ->
    V.Vector a -> TraversableContainer a #-}
{-#SPECIALISE INLINE tcOf :: Getting (Endo [a]) (TraversableContainer a) a ->
    TraversableContainer a -> TraversableContainer a #-}
tcOf :: Getting (Endo [a]) (t a) a -> t a -> TraversableContainer a
tcOf l k = {-#SCC tcOf #-} AVector $ VGL.toVectorOf l k

{-#RULES  "foldedTraversableContainerVec" [0] VGL.toVectorOf folded =
    toVecTC #-}
{-#RULES  "traversedTraversableContainerVec" [0] VGL.toVectorOf traversed =
    toVecTC #-}
{-#RULES  "foldedTraversableContainerVec" [0] VL.toVectorOf folded =
    toVecTC #-}
{-#RULES  "traversedTraversableContainerVec" [0] VL.toVectorOf traversed =
    toVecTC #-}

{-#RULES  "toVec/toVecVC" toVec = toVecTC #-}
{-#RULES  "toVec/vec" toVec = id #-}

{-#INLINABLE [1] toVec #-}
{-#SCC toVec #-}
toVec :: Traversable t => t a -> V.Vector a
toVec = {-# SCC toVectorOf #-} VGL.toVectorOf ({-# SCC traversed #-} traversed)

{-#INLINE CONLIKE toVecTC #-}
{-#SCC toVecTC #-}
toVecTC :: TraversableContainer a -> V.Vector a
toVecTC (AVector a) = a
toVecTC (ASingleton a) = V.singleton a
toVecTC (ATraversable a) = toVec a



{-# RULES "vecTC/deforestation1"  forall a. toVec (AVector a) = a #-}
{-# RULES "vecTC/deforestation2"  forall a. toVec (ATraversable a) = a #-}


{-# INLINE vecOf #-}
vecOf :: Getting (V.Vector a) s a -> s -> V.Vector a
vecOf f !a = views f V.singleton $ a
--vecOf f (ATraversable a) = views f V.singleton $ a


newtype GrayCoded a = GrayCode {unGrayCoded :: a}
    deriving newtype (Generic, Eq, Ord, Bits,Enum,  Num, FiniteBits, Integral, Real, Show, Read)

{-# INLINE integralToGray #-}
-- | @'integralToGray' n@ encodes @n@ using a BRGC, and returns the
--   resulting bits as an integer. For example, encoding @17@ in BRGC
--   results in @11001@, or 25. So @integralToGray 17@ returns @25@.
integralToGray :: Bits a => a -> a
integralToGray n = (n `shiftR` 1) `xor` n

-- | @'grayToIntegral' n@ decodes @n@ using a BRGC, and returns the
--   resulting integer. For example, 25 is @11001@, which is the code
--   for 17. So @grayToIntegral 25@ returns @17@.
{-# INLINE grayToIntegral #-}
grayToIntegral :: (Num a, Bits a) => a -> a
grayToIntegral n = f n (n `shiftR` 1)
  where
    f k m | m /= 0    = f (k `xor` m) (m `shiftR` 1)
          | otherwise = k

instance (FiniteBits a, Num a, DB.Binary a) => DB.Binary (GrayCoded a) where
    {-# INLINABLE put #-}
    put (GrayCode a) = DB.put (integralToGray a)
    {-# INLINABLE get #-}
    get = do
        g <- DB.get
        return $! GrayCode (grayToIntegral g)

deriving newtype instance Distribution StdUniform a => Distribution StdUniform
    (GrayCoded a)
deriving newtype instance Distribution Uniform a => Distribution Uniform (GrayCoded a)
newtype EnumGene a = AsEnum {unAsEnum :: a}
    deriving newtype (Generic, Eq, Ord, Enum, Bounded, Bits, FiniteBits, DB.Binary, Show, Read)

{-instance (Enum a, Bounded a) => Variate (EnumGene a) where
    {-# INLINABLE uniform #-}
    uniform gen = fmap
        (AsEnum . toEnum)
        (uniformR (fromEnum $ (minBound :: a), fromEnum $ (maxBound :: a)) gen)
    {-# INLINABLE uniformR #-}
    uniformR (!a, !b) gen =
        fmap (AsEnum . toEnum) (uniformR (fromEnum a, fromEnum b) gen)-}

deriving newtype instance Distribution StdUniform a => Distribution StdUniform
    (EnumGene a)
deriving newtype instance Distribution Uniform a => Distribution Uniform (EnumGene a)
type BroodSize = Natural
newtype GrayCodedEnumGene a = AsGrayCodedEnum {unAsGrayCodedEnum :: a}
    deriving newtype (Generic, Eq, Ord, Enum, Bounded, Bits, FiniteBits)


instance (Show a) => Show (TraversableContainer a) where
    show = show . DF.toList


type NucleotidePosition = Natural -- Rename to Locus
type GenePosition = Natural -- Likewise

type NucleotidePositions = TraversableContainer NucleotidePosition

data Run = Run {start :: !NucleotidePosition, length :: !Natural} deriving stock (Eq, Ord, Show, Read, Generic)
    deriving anyclass NFData

type Probability = Log Double
{-#INLINABLE weightToProb #-}
weightToProb :: Floating a => a -> Log a
weightToProb !a = Exp (log a)
{-#INLINABLE probToWeight #-}
probToWeight :: Floating a => Log a -> a
probToWeight (Exp !a) = exp a



\end{code}