{-#LANGUAGE ScopedTypeVariables #-}
module Data.Simulation.Analysis.ParetoFrontier
    ( Pareto(..)
    , SortedPareto(..)
    , sortedFrontierDefault
    )
where
import           Prelude                        ( not
                                                --, Maybe(Just)
                                                , ($)
                                                , Ord
                                                )
import           Control.Category               ( (.) )
import           Algebra.PartialOrd             ( PartialOrd
                                                , leq
                                                )
import           Data.Foldable                  ( Foldable
                                                , foldr
                                                , any
                                                )
--import           Data.Functor
import           Control.Applicative            ( Alternative
                                                , (<|>)
                                                , empty
                                                , pure
                                                )
import           Data.Witherable                ( Filterable
                                                , filter
                                                )
import           Data.List                      ( sort )
import           Control.Lens                   ( Each
                                                , each
                                                , partsOf'
                                                , (%~)
                                                , (&)
                                                )

{-instance {-# OVERLAPPABLE #-} (Functor f, Foldable f, Alternative f) => Filterable f where

    {-# INLINABLE mapMaybe #-}
    mapMaybe p = foldr
        (\x xs -> case p x of
            Just a -> pure a <|> xs
            _      -> xs
        )
        empty-}
class Pareto p where
    frontier :: PartialOrd a => p a -> p a
    {-#INLINABLE frontier #-}
    default frontier :: (Foldable p, Alternative p,PartialOrd a) => p a -> p a
    frontier = foldr (\x acc->
        if
            any (leq x) acc
        then
            acc
        else
            (pure x) <|> (notDominating x acc)
        ) empty
    dominating :: PartialOrd a => a -> p a -> p a
    {-# INLINABLE dominating #-}
    default dominating :: (Filterable p, PartialOrd a) => a -> p a -> p a
    dominating x ps = filter (`leq` x) ps
    notDominating :: PartialOrd a => a -> p a -> p a
    {-# INLINABLE notDominating #-}
    default notDominating :: (Filterable p, PartialOrd a) => a -> p a -> p a
    notDominating x ps = filter (not . (`leq` x)) ps

    frontierFromSorted :: PartialOrd a => p a -> p a
    {-#INLINE frontierFromSorted #-}
    frontierFromSorted = frontier
{-# SPECIALISE frontier :: PartialOrd a => [a] -> [a] #-}
class Pareto p => SortedPareto p where
    sortedFrontier :: PartialOrd a => p a -> p a


instance {-#OVERLAPPABLE #-} (Foldable f, Alternative f, Filterable f) => Pareto f where

{-# INLINABLE sortedFrontierDefault #-}
sortedFrontierDefault
    :: forall f a
     . (Each (f a) (f a) a a, PartialOrd a, Ord a, Pareto f)
    => f a
    -> f a
sortedFrontierDefault f = frontier @f @a $ (f & partsOf' each %~ sort)
