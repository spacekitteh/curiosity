module Data.Simulation.Solver where

import           Data.Simulation.Population
import           Data.Simulation.Internal.Config

data RunManager where
    RunManager ::{_config :: Configuration} -> RunManager
