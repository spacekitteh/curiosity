\begin{code}
{-#LANGUAGE LambdaCase #-}
module Data.Simulation.Solutions.Genes where
import           Data.Simulation.Solutions.GeneticMaterial
import Data.Simulation.Types
import Data.Simulation.GeneticAlgorithms.Mutations.PointMutations
import Data.Simulation.Internal.Utils
import Data.Simulation.Domains.SymbolicRegression.Operations
import Data.Typeable
import Numeric.Natural
import GHC.Generics (Generic)
import Data.Word
import GHC.Exts
import Data.Bits
import qualified Data.Binary as DB
import qualified Data.Vector as V (Vector)
import qualified Data.Vector.Generic as V hiding (Vector)
import Data.Data
import Control.Lens
import Control.Monad (replicateM)
import Polysemy
import Polysemy.RandomFu

import Data.Random
import Data.Random.Distribution.Uniform
import Data.Random.Distribution.Bernoulli

\end{code}

\chapter{Genes}
Let us now specify what counts as a gene for us. We shall assume \gls{diploid} individuals. A \gls{gene} is a unit of inheritence -- essentially, the smallest unit which supports \gls{mutation} and \gls{crossover}, and other basic genetic operators.

We make this a \gls{typeclass} in order to allow domain-specific representations. A standard case is a simple string. Importantly, a \gls{gene}'s expression is dependent on the \gls{phenotype}: As in reality, \glspl{gene} don't cause \gls{phenotype}. Instead, a \gls{phenotype} \emph{expresses} its \gls{genotype} using the mechanisms of its current \gls{phenotype}. Eventually, we should like to include things such as \glspl{promoter}, inhibitors, \gls{posttranscriptionmodification}, and other more complex, biologically-inspired mechanisms.

\begin{code}

data MutationSupportParameterInfo = OnlyOnWholeGene deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic)

data MutationSupportType = Always | CurrentlySupports (Maybe MutationSupportParameterInfo) | DoesntCurrentlySupport (Maybe MutationSupportParameterInfo) | Never (Maybe MutationSupportParameterInfo) | NotYetImplemented deriving (Eq, Ord, Read, Show, Generic)



class HasGenes a where
    {-#INLINE genes #-}
    genes ::  a -> TraversableContainer Gene
    genes a = AVector V.empty
    {-#INLINABLE _ComposingGenes #-}
    _ComposingGenes :: Prism' (TraversableContainer Gene) a
    _ComposingGenes = prism' genes constructFromGenes
    {-#INLINE constructFromGenes #-}
    constructFromGenes :: Traversable t => t Gene -> Maybe a
    constructFromGenes _ = Nothing
    {-#INLINABLE zoomMutations #-}
    zoomMutations :: a -> MutationKind -> TraversableContainer (Gene, Maybe MutationKind)
    zoomMutations k ty ={-# SCC defaultZoomMutations #-} AVector . snd . mapAccumLOf traverse (\(!t) g ->
        let
            (a,b) = maybe (Nothing,Nothing) (\(!t) -> zoomMutation t (geneSize' g)) t
        in
            (b, (g,a)))
        (Just ty) $ toVec (genes k)
    {-#INLINE mutateGenes #-}
    mutateGenes :: Member RandomFu r =>  MutationKind -> a -> Sem r (TraversableContainer Gene)
    mutateGenes ty a = {-#SCC defaultMutateGenes#-}applyMutations (zoomMutations a ty)

{-#SCC applyMutations #-}
{-#INLINE applyMutations #-}
{-#SPECIALISE INLINE applyMutations :: Member RandomFu r => V.Vector (Gene,
    Maybe MutationKind) -> Sem r (TraversableContainer Gene) #-}
applyMutations :: (Traversable t, Member RandomFu r) => t (Gene, Maybe
    MutationKind) ->  Sem r (TraversableContainer Gene)
applyMutations !tys = AVector <$> mapM (
        \(!g, ty) -> maybe (pure g) (\(!t) -> mutateGene t g) ty
    ) (toVec tys)

instance HasGenes (TraversableContainer Gene) where
    {-# INLINE genes #-}
    genes = id
    {-# INLINE CONLIKE constructFromGenes #-}
    {-# SPECIALISE INLINE constructFromGenes :: TraversableContainer Gene -> Maybe (TraversableContainer Gene) #-}
    constructFromGenes = Just . aTraversable


instance {-# OVERLAPPABLE #-} (Typeable a, IsGene a) => HasGenes (V.Vector a) where
    {-#SCC genes "vector_genes" #-}
    {-#INLINE genes #-}
    genes = AVector . fmap AGene
    {-#SCC constructFromGenes "constructFromGenes_vector" #-}
    {-#INLINE constructFromGenes #-}
    constructFromGenes !a = traverse (\(!g) -> g ^? _AGene) (toVec a)

    {-# INLINABLE zoomMutations #-}
    zoomMutations !k !ty =
        {-#SCC vectorZoomMutations #-}AVector $! snd $! mapAccumLOf traverse (\(!t) g ->
        let
            (a,b) = maybe (Nothing,Nothing) (\(!t) -> zoomMutation t (geneSize g)) t
        in
            (b, (AGene g,a)))
        (Just ty) k

    {-#INLINABLE mutateGenes #-}
    mutateGenes !ty !a = {-#SCC vectorMutateGenes #-}do
        let muts = toVec $ zoomMutations a ty
        mutated <- mapM (\(!g, !ty) -> maybe (pure g) (\t -> mutateGene t g) ty) muts
        return $! AVector mutated

instance HasGenes (V.Vector Gene) where
    {-# INLINE CONLIKE genes #-}
    genes = AVector
    {-# INLINE constructFromGenes #-}
    constructFromGenes = Just . toVec
    {-# INLINABLE zoomMutations #-}
    zoomMutations k ty =
        AVector $ snd $ mapAccumLOf traverse (\(!t) g ->
        let
            (a,b) = maybe (Nothing,Nothing) (\(!t) -> zoomMutation t (geneSize' g)) t
        in
            (b, (g,a)))
        (Just ty) k

class HasGenes a => IsGene a where
    type GeneType a :: GeneKind
    type GeneType a = 'ADT
    type VariableLengthGene a :: Bool
    type VariableLengthGene a = 'False

    -- |Generate a new random gene.
    initialiseWith :: Member RandomFu r => GeneInitialisationParameters
        -> Sem r a
    {-#INLINABLE initialiseWith #-}
    default initialiseWith :: (IsGeneticMaterial a, Member RandomFu r,
        Distribution StdUniform (Nucleotide a)) => GeneInitialisationParameters ->  Sem r a
    initialiseWith (Initialise len) = do
        nucls <- sampleRVar $ replicateM (fromIntegral len) stdUniform
        return $! (construct . AVector . fromListN (fromIntegral len)) nucls
    -- TODO: Use QuickCheck

    {-#INLINE validate #-}
    -- |Sometimes a gene might have some constraints on valid values.
    validate :: a -> Bool
    validate _ = True

    {-#INLINE geneSize #-}
    -- |The size of the gene in nucleotides.
    geneSize :: a -> Natural
    geneSize !a = max (sum ( fmap geneSize' (genes a))) 1

    -- |Whether this gene supports the given mutation.
    supportsMutation :: MutationKind -> a -> MutationSupportType

    {-#INLINE geneID #-}
    -- |A unique identifier for the specific gene.
    geneID :: a -> URI
    geneID _ = "Unnamed"

    {-#INLINE alleleID #-}
    -- |A unique identifier for the specific allele.
    alleleID :: a -> URI
    default alleleID :: Show a => a -> URI
    alleleID = show

    {-#INLINE geneOrigin #-}
    -- |Where the gene arose.
    geneOrigin :: a -> Maybe URI
    geneOrigin _ = Nothing

    {-#INLINE alleleOrigin #-}
    -- |Where the allele arose.
    alleleOrigin :: a -> Maybe URI
    alleleOrigin _ = Nothing

    {-#INLINABLE mutate #-}
    -- |Mutate the gene.
    mutate :: Member RandomFu r => MutationKind -> a -> Sem r a
    mutate = defaultMutate

{-#INLINABLE defaultMutate #-}
defaultMutate :: (IsGene a, Member RandomFu r) => MutationKind -> a -> Sem r a
defaultMutate !ty !a  = do
    let g = genes a
    g' <- mutateGenes ty g
    maybe (return a) (return) (constructFromGenes g')

\end{code}
We implement some mutation operators for some basic \glspl{type}:
\begin{code}




instance HasGenes Operator
instance IsGene Operator where
    type GeneType (Operator) = 'Atomic
    {-#INLINE supportsMutation #-}
    supportsMutation (PointErrors _ _) _ = Always
    supportsMutation _ _ = NotYetImplemented
    {-#INLINE mutate #-}
    mutate (PointErrors rate !r) a = pointMutations rate r a
    mutate  _ a = return a
    {-#INLINE geneSize #-}
    geneSize _ = 1

instance HasGenes (EnumGene a)
instance (Enum a, Bounded a, Show a, Distribution StdUniform a) => IsGene (EnumGene a) where
    type GeneType (EnumGene a) = 'Atomic
    {-#INLINABLE initialiseWith #-}
    initialiseWith _ = fmap (coerce @a) $ sampleRVar stdUniform
    {-#INLINE supportsMutation #-}
    supportsMutation (PointErrors _ _) _ = Always
    supportsMutation  _ _ = NotYetImplemented
    {-#INLINE mutate #-}
    mutate (PointErrors rate r) a = pointMutations rate r a
    mutate _ a = return a
    {-#INLINE geneSize #-}
    geneSize _ = 1

instance HasGenes (GrayCoded a)
instance (FiniteBits a, Integral a, DB.Binary a, Distribution Uniform a,Distribution StdUniform a, Show a) => IsGene (GrayCoded a) where
    type GeneType (GrayCoded a) = 'NuclearString
    -- |TODO: Change this to use minBound/maxBound
    {-#INLINABLE initialiseWith #-}
    initialiseWith (Initialise !maxSize) = sampleRVar $ uniform 0 (fromIntegral
        maxSize)
    {-#SCC geneSize "grayCodedGeneSize" #-}
    {-#INLINABLE geneSize #-}
    geneSize = finiteBitsGeneticMaterialSize
    {-#INLINABLE supportsMutation #-}
    supportsMutation (PointErrors _ _) _ = Always
    supportsMutation (NormalDrift _ _ _) _ = Always
    supportsMutation _ _ = NotYetImplemented
    {-#INLINABLE mutate #-}
    mutate (PointErrors rate r) a = pointMutations  rate r a
    mutate (NormalDrift r bias dev) a = do
        doit <- sampleRVar (bernoulli r)
        if doit then do
            diff <- sampleRVar (normal bias dev)
            return $ a + (round diff)
        else return a
    mutate _ a = return a

instance HasGenes (Word8)
instance IsGene Word8 where
    type GeneType Word8 = 'NuclearString
    {-#INLINABLE initialiseWith #-}
    initialiseWith (Initialise maxSize) = sampleRVar $ uniform 0 (fromIntegral maxSize)
    {-#INLINE geneSize #-}
    geneSize _ = 8
    {-#INLINABLE supportsMutation #-}
    supportsMutation (PointErrors _ _) _ = Always
    supportsMutation (NormalDrift _ _ _) _ = Always
    supportsMutation _ _ = NotYetImplemented
    {-#INLINABLE mutate #-}
    {-#SCC mutate "Word8mutate" #-}
    mutate (PointErrors rate r) a = pointMutations rate r a
    mutate (NormalDrift rate bias dev) a = do
        doit <- sampleRVar (bernoulli rate)
        if doit then do
            diff <- sampleRVar (normal bias dev)
            return $ a + (round diff)
        else return a
    mutate _ a = return a

instance HasGenes (Word16)
instance IsGene Word16 where
    type GeneType Word16 = 'NuclearString
    {-#INLINABLE initialiseWith #-}
    initialiseWith (Initialise maxSize) = sampleRVar $ uniform 0 (fromIntegral maxSize)
    {-#INLINE geneSize #-}
    geneSize _ = 16
    {-#INLINABLE supportsMutation #-}
    supportsMutation (PointErrors _ _) _ = Always
    supportsMutation (NormalDrift _ _ _) _ = Always
    supportsMutation _ _ = NotYetImplemented
    {-#INLINABLE mutate #-}
    mutate (PointErrors rate r) a = pointMutations rate r a
    mutate (NormalDrift rate bias dev) a = do
        doit <- sampleRVar (bernoulli rate)
        if doit then do
            diff <- sampleRVar (normal bias dev)
            return $ a + (round diff)
        else return a
    mutate _ a = return a
instance HasGenes (Word32)
instance IsGene Word32 where
    type GeneType Word32 = 'NuclearString
    {-# INLINABLE initialiseWith #-}
    initialiseWith (Initialise maxSize) =
        sampleRVar $ uniform 0 (fromIntegral maxSize)
    {-# INLINE geneSize #-}
    geneSize _ = 32
    {-# INLINABLE supportsMutation #-}
    supportsMutation (PointErrors _ _) _ = Always
    supportsMutation (NormalDrift _ _ _) _ = Always
    supportsMutation _               _ = NotYetImplemented
    {-# INLINABLE mutate #-}
    mutate (PointErrors rate r) a = pointMutations rate r a
    mutate (NormalDrift rate bias dev) a = do
        doit <- sampleRVar (bernoulli rate)
        if doit then do
            diff <- sampleRVar (normal bias dev)
            return $ a + (round diff)
        else return a
    mutate _ a  = return a
instance HasGenes(Word64)
instance IsGene Word64 where
    type GeneType Word64 = 'NuclearString
    {-# INLINABLE initialiseWith #-}
    initialiseWith (Initialise maxSize) =
        sampleRVar $ uniform 0 (fromIntegral maxSize)
    {-# INLINE geneSize #-}
    geneSize _ = 64
    {-# INLINABLE supportsMutation #-}
    supportsMutation (PointErrors _ _) _ = Always
    supportsMutation (NormalDrift _ _ _) _ = Always
    supportsMutation _               _ = NotYetImplemented
    {-# INLINABLE mutate #-}
    mutate (PointErrors rate r) a = pointMutations rate r a
    mutate (NormalDrift r bias dev) a = do
        doit <- sampleRVar (bernoulli r)
        if doit then do
            diff <- sampleRVar (normal bias dev)
            return $ a + (round diff)
        else return a
    mutate _ a = return a

instance HasGenes (Double)
instance IsGene Double where
    type GeneType Double = 'Atomic
    {-#INLINE initialiseWith #-}
    initialiseWith (Initialise maxSize) = sampleRVar (uniform (-(fromIntegral maxSize)) (fromIntegral maxSize))
    {-#INLINE geneSize #-}
    geneSize _ = 1
    {-#INLINE supportsMutation #-}
    supportsMutation (ConstantScale _ _) _ = Always
    supportsMutation (NormalDrift _ _ _) _ = Always
    supportsMutation _ _ = NotYetImplemented
    {-#INLINABLE mutate #-}
    {-#SCC mutate "DoubleMutate" #-}
    mutate (ConstantScale r s) !a = do
        doit <- sampleRVar (bernoulli r)
        if doit then return (a * s) else return a
    mutate (NormalDrift r bias dev) !a = do
        doit <- sampleRVar (bernoulli r)
        if doit then
            sampleRVar (normal (a + bias) dev)
        else return a
    mutate _ a = return a
\end{code}

\section{Special genes}
While the \glspl{gene} of interest in actually using \glspl{ga} for practical problem solving are typically domain-specific, there are some parameters of the algorithm which are universal. Typically, these are inspired by biological \glspl{gene} controlling the fundamental processes of evolution; for example, the \gls{mutationrate} is a direct simplification of \glspl{gene} involved in \gls{dna} replication.

These \glspl{gene} usually differ between species. We thus enable special support for these algorithmic parameters to be encoded in \glspl{gene}, which are universally applicable: Why not let evolution find the optimal values for the problem?
\begin{code}

-- |A gene suggesting how the critter should produce offspring.
data BreedingStrategy =
    -- |Two parents produce two offspring,
    -- |with complimentary recombination
    EqualCrossover
    -- |Brood selection, with a given brood size.
    | Brood BroodSize
    deriving (Eq, Show, Read, Generic, Data, Typeable)

-- |A gene suggesting fundamental evolutionary mechanics
data ReplicationFactor where
    AMutationRate :: ErrorRate -> ReplicationFactor
    -- ^Controls the mutation rate for the critter
    ABreedingStrategy :: BreedingStrategy -> ReplicationFactor
    -- ^How the critter reproduces
    ARecombinationFrequency :: Rate -> ReplicationFactor
    -- ^How often the parental genes are mixed in the progeny
    deriving (Eq, Show, Read, Generic, Data, Typeable)

type TranscriptionBuff = Double

-- |Genes controlling gene transcription processes
data TranscriptionFactor where
    Buff :: TranscriptionBuff -> TranscriptionFactor
    Regulator :: TranscriptionFactor -- TODO
    deriving (Eq, Ord, Show, Read, Generic, Data, Typeable)

\end{code}
\section{Gene container}
In order to be as generic as possible when handling \glspl{gene}, we introduce a container which can hold either a special gene, or a domain-specific gene.
\begin{code}
-- |A generic gene, with some universally known special genes.
data Gene where
    AGene :: (Typeable a, IsGene a) => {_theGene :: !a} -> Gene
    ATranscriptionFactor :: TranscriptionFactor -> Gene
    ATranslationFactor :: Gene -- TODO
    PlasmidDonorFactor :: Rate -> Gene
    PlasmidAcceptaneFactor :: Rate -> Gene
    AReplicationFactor :: ReplicationFactor -> Gene
    AName :: String -> Gene

deriving instance Typeable Gene

{-#INLINABLE _AGene #-}
{-#SPECIALISE _AGene :: Prism' Gene Double #-}
{-#SPECIALISE _AGene :: Prism' Gene Word8 #-}
{-#SPECIALISE _AGene :: (Typeable a, FiniteBits a, Integral a, DB.Binary a,
    Distribution StdUniform a, Distribution Uniform a, Show a) => Prism' Gene (GrayCoded a) #-}
{-#SPECIALISE _AGene :: (Typeable a, Enum a, Bounded a, Show a, Distribution StdUniform a) =>
    Prism' Gene (EnumGene a) #-}
{-#SPECIALISE _AGene :: Prism' Gene (GrayCoded Word8) #-}
_AGene :: (Typeable a, IsGene a) => Prism' Gene a
_AGene = prism' AGene (\case
        AGene c -> cast c
        _ -> Nothing)

{-#INLINE geneSize' #-}
-- TODO: Implement IsGene for Gene
geneSize' :: Gene ->  Natural
geneSize' (AGene g) = geneSize g
geneSize' _ = 1 -- TODO

{-#INLINABLE mutateGene #-}
mutateGene :: Member RandomFu r =>  MutationKind -> Gene -> Sem r Gene
mutateGene ty (AGene g) = fmap AGene $ mutate ty g
mutateGene _ g = return g -- TODO


\end{code}