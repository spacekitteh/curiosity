\comment{
\begin{code}
module Data.Simulation.Solutions.Genome where
import Prelude hiding (drop, zipWith, length, (++))
import           Data.Simulation.Solutions.Chromosomes
import Data.Simulation.Types
import Data.Simulation.Internal.Config
import Data.Typeable
import Control.Lens
import Data.Vector.Generic as V
import qualified Data.Vector as VN
import Data.Foldable as DF
import Data.Traversable as DT
import Data.Maybe
import           GHC.Types                      ( SPEC(..) )
import Polysemy
import Polysemy.RandomFu
import Data.Random.Distribution.Uniform
\end{code}
}
\chapter{Genomes}
Now, what about a \gls{genome}?

\begin{code}

class HasChromosomes a where
    toChromosomes :: a -> Chromosomes
    constructFromChromosomes :: Traversable t => t Chromosome -> Maybe a
    similiarity :: a -> a -> Double
    {-#INLINABLE chromosomes #-}
    chromosomes :: Prism' (TraversableContainer Chromosome) a
    chromosomes = prism' toChromosomes constructFromChromosomes
    {-#INLINABLE mutateGenome #-}
    -- | Perform the given mutations on a critter's genome.
    mutateGenome :: forall t r.  (Traversable t, Member RandomFu r) => t
        (MutationKind, MutationTarget) -> a -> Sem r a
    default mutateGenome :: forall t r. (Eq a, Traversable t, Member RandomFu r)
        => t (MutationKind, MutationTarget) -> a -> Sem r a
    mutateGenome muts g = do
        let chroms = toChromosomes g
        newChroms <- DT.forM chroms $ \a ->
            foldrM (\( k, t) pieces -> fmap (V.foldr' (++) empty . fmap toVec) (traverse (mutateChromosome' @r t k) pieces))(singleton a) muts

        let concatted = V.foldr' (++) empty (toVec newChroms)
        case constructFromChromosomes concatted of
            Just result | result /= g -> return result
            _ -> {-#SCC "mutateGenome_recursive_case" #-} mutateGenome muts g
    -- | Given some parents, produce new offspring. You can assume perfect copying, as there is a later mutation pass, but it can be more efficient to mutate during recombination.
    recombineGenome :: (Traversable t, Member RandomFu r) =>
        Configuration -- ^ The config
        -> t a -- ^ Parents
        -> Sem r (TraversableContainer a) -- ^ New critters

{-#SCC recombinationOverGenomeImplVec #-}
{-#INLINE recombinationOverGenomeImplVec #-}
recombinationOverGenomeImplVec :: forall (c :: *) r a. (IsChromosome c, Typeable
    c, Member RandomFu r, HasChromosomes a) =>  Configuration -> VN.Vector a ->
    Sem r (TraversableContainer a)
recombinationOverGenomeImplVec = recombinationOverGenomeImpl @c


-- |Performs recombination across the whole genome.
-- By default, it only recombines matching chromosomes.
recombinationOverGenomeImpl :: forall (c :: *) r a t. (IsChromosome c, Typeable
    c, Member RandomFu r, HasChromosomes a, Traversable t) =>  Configuration ->
    t a -> Sem r (TraversableContainer a)
{-#SCC recombinationOverGenomeImpl #-}
{-#INLINABLE recombinationOverGenomeImpl #-}
recombinationOverGenomeImpl config parents | DF.length parents >= 2 =  recomb SPEC where
    {-#INLINABLE recomb #-}
    recomb :: SPEC -> Sem r (TraversableContainer a)
    recomb !sPEC =
        let broodSize =
                if config^.reproduction.strategy.brood.enabled
                then fromIntegral $ config^.reproduction.strategy.brood.size
                else DF.length parents

            count = (broodSize + 1) `div` 2
            parents' = {-# SCC toVecParents #-} toVec parents
            l = DF.length parents' - 1
        in do

            progeny <- {-# SCC unfolding #-} V.unfoldrNM  count (\(!i) ->
                if i == 0
                then
                    return Nothing
                else do
                    x <- sampleRVar (uniform 0 l)
                    y <- sampleRVar (uniform 0 l)

                    let newI = (max i 2) - 2
                    offspring <- recombinationOverGenomeImpl' sPEC config (parents' V.! x) (parents' V.! y)
                    return $ Just (offspring, newI)
                ) broodSize
                --return $! AVector $ \(a,b) -> a ++ b $ V.unzip progeny
            return $ {-# SCC concatMap #-} AVector  (V.concatMap (\(a,b) -> V.fromListN 2 [a,b])
                progeny)
    {-# INLINABLE recombinationOverGenomeImpl' #-}
    {-# SCC recombinationOverGenomeImpl' #-}
    recombinationOverGenomeImpl' !sPEC config  a b =
        let a' = {-# SCC toVecToChromsA #-}toVec (toChromosomes a)
            b' = {-# SCC toVecToChromsB #-}toVec (toChromosomes b)
            la = V.length a'
            lb = V.length b'
            (x,y) = (case compare la lb of
                        EQ -> (a',b')
                        LT -> (a' ++ ((drop la) b'), b')
                        GT -> (a', b' ++ ((drop lb) a')))
        in do
            newChroms <- {-# SCC newChroms #-} V.sequence(zipWith (\x' y' -> do
                (c1,c2) <- recombination @c config (x'^?! _AChromosome) (y'^?! _AChromosome)
                return (AChromosome c1, AChromosome c2)
                ) x y) -- FIXME TODO
            let (newX, newY) = V.unzip newChroms
                newA = fromMaybe a ({-# SCC constructFromChromosomesA #-}constructFromChromosomes newX)
                newB = fromMaybe b ({-# SCC constructFromChromosomesB #-}constructFromChromosomes newY)
            return (newA, newB)
recombinationOverGenomeImpl _ a = return (aTraversable a)

data Genome where
    AGenome :: (Typeable a, HasChromosomes a) => a -> Genome
    deriving Typeable

{-#INLINABLE _AGenome #-}
_AGenome :: (Typeable a, HasChromosomes a) => Prism' Genome a
_AGenome = prism' AGenome (\(AGenome g) -> cast g)

\end{code}