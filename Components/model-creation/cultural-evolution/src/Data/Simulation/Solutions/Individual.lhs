\comment{
\begin{code}
module Data.Simulation.Solutions.Individual where
import Data.Simulation.Solutions.Genome
import           Data.Simulation.Types
import Control.Lens
import qualified Data.Vector as DV (Vector)
import qualified Data.Vector.Generic as V
import qualified Data.Vector.Generic.Mutable as VM
import qualified Data.Foldable as DF
import Data.Simulation.Internal.Config
import Data.Vector.Algorithms.Tim
import Data.Proxy
import GHC.Types (SPEC(..))

import Polysemy
import Polysemy.RandomFu
import Control.Effects.Cache
import Data.Random.Distribution.Bernoulli
import Data.Maybe (isJust)
\end{code}
}

\chapter{Individuals}
A \gls{typeclass} for \glspl{individual}.
\begin{code}

data IndividualInitialisationParameters a where
    InitialiseIndividualWith :: {_genomeLength :: (Maybe GenePosition)} -> IndividualInitialisationParameters a


--defaultCritterParams = InitialiseIndividualWith (Nothing)
makeCritterParams :: Configuration -> IndividualInitialisationParameters a
makeCritterParams config = InitialiseIndividualWith (Just $ config ^. individuals . initialisation . minimumInitialGenomeSize)
class HasChromosomes a => IsIndividual a where
    type Environment a
    type Environment a = ()
    type Score a
    type Score a = TraversableContainer Double
    type Location a
    type Location a = ()
    type PhenotypeType a
    type PhenotypeType a = a
    {-#INLINE getPhenotype #-}
    getPhenotype :: a -> PhenotypeType a
    default getPhenotype :: PhenotypeType a ~ a => a -> PhenotypeType a
    getPhenotype = id

    -- | Bigger is better.
    fitness :: Configuration -> Environment a -> a -> Score a
    {-#INLINE approximatedFitness #-}
    -- | Bigger is better
    approximatedFitness :: Configuration -> Environment a -> a -> Score a
    approximatedFitness = Data.Simulation.Solutions.Individual.fitness
    {-#INLINABLE orderApproximatedFitness #-}
    -- | Bigger is better; that is, if a > b, then a is better.
    orderApproximatedFitness :: Proxy a -> (Score a) -> (Score a) -> Ordering
    default orderApproximatedFitness :: Proxy a -> Ord (Score a) => (Score a) -> (Score a) -> Ordering
    orderApproximatedFitness _ =  compare
    location :: Lens' a (Location a)
    location = lens getLocation setLocation
    getLocation :: a -> Location a
    {-#INLINE similarTo #-}
    similarTo ::  (Traversable t) => Configuration -> t a -> a -> Sem r Bool
    default similarTo ::  (Traversable t, Eq (PhenotypeType
        a)) =>  Configuration -> t a -> a -> Sem r Bool
    --similarTo _ _ (AVector parents) critter = return $ V.any (\parent -> getPhenotype critter == getPhenotype parent) parents
    similarTo _ parents critter = return $ DF.any (\parent -> getPhenotype critter == getPhenotype parent) parents
    {-#INLINE setLocation #-}
    setLocation :: a -> Location a -> a
    setLocation a _ = a
    {-#SCC survivesBrood #-}
    {-#INLINABLE survivesBrood #-}
    survivesBrood :: (Traversable t, Members [RandomFu, Cache (PhenotypeType a) (Score a)] r) =>  Configuration -> t a -> t a -> a -> Sem r Bool
    survivesBrood config _ _ _ | config ^. reproduction . strategy . brood . enabled == False = return True
    survivesBrood config parents brood critter = do
        similar <- similarTo  config parents critter
        --seen <- isCached (getPhenotype critter)
        if similar -- || isJust seen
        then
            not <$> sampleRVar (bernoulli (config ^. reproduction . strategy . identicalPhenotypeRejectionRate))
        else
            return True

    -- TODO: mutatePhenotype?
    {-#INLINE canMateWith #-}
    canMateWith :: a -> a -> Bool
    canMateWith _ _ = True
    createIndividual :: Member RandomFu r => IndividualInitialisationParameters a
        ->  Sem r a
    {-#INLINE mateWith #-}
    mateWith :: (Traversable t, Members [RandomFu, Cache (PhenotypeType a) (Score a)] r) => Configuration -> Environment a ->
        TraversableContainer (MutationKind, MutationTarget) -> t
        a -> Sem r (TraversableContainer a)
    mateWith = mateWithDefault


    {-#INLINE parents#-}
    parents :: a -> Maybe (TraversableContainer a)
    parents _ = Nothing
    individualID :: a -> URI


{-#INLINABLE mateWithDefault #-}
mateWithDefault :: forall a t r. (IsIndividual a, Traversable t, Members [RandomFu, Cache (PhenotypeType a) (Score a)] r) => Configuration -> Environment a ->
    TraversableContainer (MutationKind, MutationTarget) -> t
    a -> Sem r (TraversableContainer a)
mateWithDefault config matingEnvironment reproductiveMutations theParents = mateWith' SPEC (V.empty) where
    parentLength = DF.length theParents
    broodEnabled = config^.reproduction.strategy.brood.enabled
    {-#INLINABLE mateWith' #-}
    mateWith' !sPEC existing = do
        offspring <- {-#SCC "spawning_offspring" #-}recombineGenome config theParents
        mutants <- {-#SCC "mutating_offspring" #-}traverse (\critter -> mutateGenome reproductiveMutations critter) (toVec offspring)
        survivors <- {-#SCC "culling_runts" #-}if broodEnabled
            then
                V.filterM (survivesBrood config (aTraversable theParents) (AVector mutants)) mutants
            else pure mutants
        let testedSurvivors = fmap (\a -> (approximatedFitness config matingEnvironment a, a)) survivors
            allSurvivors = {-# SCC concattingSurvivors #-} testedSurvivors V.++ existing
        if V.length allSurvivors < parentLength
        then
            {-#SCC "recursive_mateWith'" #-}mateWith' sPEC allSurvivors
        else
            let sorted = {-#SCC "sorting_brood" #-} V.create (do
                    thawed <- V.thaw allSurvivors

                    --Since bigger is better, we want to sort so that the biggest elements appear first.
                    sortBy (\(a,_) (b,_) -> flip (orderApproximatedFitness (Proxy :: Proxy a)) a b) thawed
                    let taken = VM.take parentLength thawed
                    return taken )
            in {-#SCC "forcing_brood" #-}return . AVector . V.force . fmap snd $ sorted


\end{code}