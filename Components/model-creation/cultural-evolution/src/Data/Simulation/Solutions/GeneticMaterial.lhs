\chapter{Genetic material}
The full source code can be found at \cite{sophie_taylor_artificial_2019}.
For now, we look at plain genetic material, without any stochastic behaviour.
\comment{
\begin{code}
{-# OPTIONS_GHC -Wno-redundant-constraints #-}
module Data.Simulation.Solutions.GeneticMaterial where
import Prelude hiding (zip, zipWith, (++), foldr)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Control.Lens.Iso
import Numeric.Natural
import qualified Data.Foldable as DF
import Data.Bits
import Data.Bits.Lens
--import Codec.Gray
import GHC.Generics
import qualified Data.Binary as DB
import Data.Word
import Control.Lens hiding (indices)
import Data.Either
import Data.Maybe
import Data.Functor.Reverse
import qualified Data.Vector as V (Vector)
import Data.Vector.Generic as V hiding (Vector)
import Data.Simulation.Types
import Data.Simulation.Domains.SymbolicRegression.Operations


type family IsAtomic' (k :: GeneKind) :: Bool where
    IsAtomic' 'Atomic = 'True
    IsAtomic' 'NuclearString = 'False
    IsAtomic' 'Tree = 'False
    IsAtomic' 'Graph = 'False
    IsAtomic' 'ADT = 'False
    IsAtomic' 'RecursiveADT = 'False

type family Not (a :: Bool) :: Bool where
    Not 'True = 'False
    Not 'False = 'True

type SupportsCrossovers a = 'True ~ Not (IsAtomic' (GeneticMaterialKind a))





\end{code}
}
Genetic material is a structured collection of \glspl{nucleotide}. Since each problem domain has unique \glspl{nucleotide}, and therefore likely has specialised containers of them, we make this a \gls{typeclass} in order to increase genericity.
\begin{code}


type role IsGeneticMaterial nominal
type Nucleotides a = TraversableContainer (Nucleotide a)
class IsGeneticMaterial a where
    type GeneticMaterialKind a :: GeneKind
    type GeneticMaterialKind a = 'NuclearString
    type Nucleotide a
    type Nucleotide a = Bool
    type GeneticHelperStructure a
    type GeneticHelperStructure a = a

    toHelper :: a -> GeneticHelperStructure a
    default toHelper :: a ~ GeneticHelperStructure a => a -> GeneticHelperStructure a
    toHelper = id
    fromHelper :: GeneticHelperStructure a -> a
    default fromHelper :: a ~ GeneticHelperStructure a => GeneticHelperStructure a -> a
    fromHelper = id
    {-# MINIMAL construct, nucleotides#-}

    construct :: Nucleotides a -> a
    --default construct ::
    --  (Generic a, GenericGeneticMaterial (Rep a)) =>
    -- Nucleotides a -> a
    --  construct = GHC.Generics.to . gconstruct . toList
    nucleotides :: a -> Nucleotides a
    --default nucleotides ::
    --  (Generic a, Generic (Nucleotide a),
    --  Rep (Nucleotide a) ~ GNucleotide (Rep a),
    --  GenericGeneticMaterial (Rep a)) => a -> Nucleotides a
    --nucleotides =
    --  ATraversable . (fmap GHC.Generics.to) .
    --  gnucleotides . GHC.Generics.from
    {-#INLINE fromByteString #-}
    fromByteString :: BS.ByteString -> a
    default fromByteString :: DB.Binary (V.Vector (Nucleotide a)) => BS.ByteString -> a
    fromByteString = construct . AVector . DB.decode @(V.Vector (Nucleotide a)) . BSL.fromStrict
    {-#INLINE toByteString #-}
    toByteString :: a -> BS.ByteString
    default toByteString :: DB.Binary (V.Vector (Nucleotide a)) => a -> BS.ByteString
    toByteString = BSL.toStrict . DB.encode . toVec . nucleotides
    {-#INLINABLE viewNucleotide #-}
    viewNucleotide :: a -> NucleotidePosition -> Nucleotide a
    viewNucleotide a pos = fromJust $ preview (confusing (traversed . (Control.Lens.index (fromIntegral pos)))) (a ^. asNucleotides)

    {-#INLINABLE viewNucleotides #-}
    -- TODO: replace current nucleotide system with lenses/prisms?
    viewNucleotides :: a -> Run -> Nucleotides a
    viewNucleotides a (Run !s !l) = AVector $! slice (fromIntegral s) (fromIntegral l) (toVec . nucleotides $ a)

    {-#INLINE canSubstituteNucleotide #-}
    canSubstituteNucleotide :: Nucleotide a -> NucleotidePosition -> a -> Bool
    canSubstituteNucleotide _ _ _= True

    {-#INLINABLE substituteNucleotides #-}
    -- |Substitute multiple nucleotides into a genotype.
    substituteNucleotides :: Traversable t => t (Nucleotide a) -> t NucleotidePosition ->  a -> a
    -- TODO: Use traversed.indices... or withIndex?
    substituteNucleotides newNucleotides positions a = {-#SCC "default" #-}result where
        changes = zip (toVec positions) (toVec newNucleotides)
        result = foldr (\((pos), (nucleotide)) (newGenome :: a) -> (substituteNucleotide) nucleotide (pos) newGenome) a changes
    {-#INLINABLE replaceNucleotidesAlong #-}
    replaceNucleotidesAlong :: Traversable t => t (Nucleotide a) -> Run -> a -> a
    replaceNucleotidesAlong nucls (Run s' len) a =let s = fromIntegral s' in substituteNucleotides (toVec nucls) (V.enumFromN s (fromIntegral len)) a

    --TODO Add a function to grab a collection of Iso's to equivalent representations in order to diversify mutations

    -- |Sometimes you just want to set a specific locus to a specific
    -- |nucleotide. Can be used to implement `mutate`.
    -- |If the substitution can't be made for representational reasons,
    -- |it returns the unaltered material.
    {-#INLINABLE substituteNucleotide #-}
    substituteNucleotide :: Nucleotide a -> NucleotidePosition -> a -> a
    substituteNucleotide n pos a = if canSubstituteNucleotide n pos a then a & (confusing(asNucleotides.traversed.(Control.Lens.index (fromIntegral pos)))).~ n else a


    {-#INLINABLE reverseNucleotides #-}
    -- |Reverse a stretch of nucleotides.
    reverseNucleotides :: Run -> a -> a
    reverseNucleotides run a = replaceNucleotidesAlong (ATraversable . Reverse $ (viewNucleotides a run)) run a


    {-#INLINABLE crossover #-}
    -- |Cross two genes.
    crossover :: SupportsCrossovers a => CrossoverParameters -> a -> a -> a
    crossover (OnePoint p) x y = progeny where
        ylength = Run p (geneticMaterialSize y - p)
        ys = viewNucleotides y ylength
        progeny = replaceNucleotidesAlong ys ylength x

    {-#SCC geneticMaterialSize #-}
    {-#INLINE geneticMaterialSize #-}
    -- |The raw size of the material in nucleotides.
    geneticMaterialSize :: a -> Natural
    geneticMaterialSize = fromIntegral . DF.length . nucleotides

    -- Do an "Isomorphism" operation, e.g. optimisation/simplification

\end{code}
We now introduce some helper definitions.
\begin{code}
-- TODO: Use Prisms
{-#INLINABLE asNucleotides #-}
asNucleotides :: IsGeneticMaterial a =>  Iso' a (Nucleotides a)
asNucleotides = iso nucleotides construct

{-#INLINABLE asByteString #-}
asByteString :: IsGeneticMaterial a => Iso' a BS.ByteString
asByteString = iso toByteString fromByteString

\end{code}
A common class of \glspl{nucleotide} are \glspl{word}. We choose to denote these as strings of bits, and transform them via \gls{graycode} to decrease abrupt changes as a result of \glspl{seu}.
\begin{code}
{-#INLINABLE grayCodeCrossover #-}
{-#SCC grayCodeCrossover #-}
grayCodeCrossover :: forall a. (FiniteBits a, Num a) =>  CrossoverParameters -> a -> a -> a
grayCodeCrossover (OnePoint !p) !x' !y' = grayToIntegral ((leftMask .&. x) .|. (rightMask .&. y))  where
        x = integralToGray  x'
        y = integralToGray y'
        ones = complement zeroBits
        i = (finiteBitSize x) - (fromIntegral p)
        leftMask = shiftL ones i
        rightMask = complement leftMask
{-#INLINABLE finiteBitsGeneticMaterialSize #-}
{-#SCC finiteBitsGeneticMaterialSize #-}
finiteBitsGeneticMaterialSize :: FiniteBits a => a -> Natural
finiteBitsGeneticMaterialSize = fromIntegral . finiteBitSize
{-#INLINABLE graySubstituteNucleotide #-}
{-#SCC graySubstituteNucleotide #-}
graySubstituteNucleotide :: forall a. (Nucleotide a ~ Bool, Bits a, Num a) =>  Nucleotide a -> NucleotidePosition -> a -> a
graySubstituteNucleotide True !n !a = grayToIntegral (setBit (integralToGray a) i)  where
    i = fromIntegral n
graySubstituteNucleotide False !n !a = clearBit a i where
    i = fromIntegral n

{-#SCC graySubstituteNucleotides #-}
{-#INLINABLE graySubstituteNucleotides #-}
graySubstituteNucleotides :: forall a t. (Bits a, Num a, Traversable t) =>  t (Bool, NucleotidePosition) ->  a -> a
graySubstituteNucleotides combd a = grayToIntegral newVal where
    grayd = integralToGray a
    {-pos' = toVec positions
    newvals' = toVec newvals
    combd = zipWith (\(!b) (!p) -> (b, fromIntegral p)) newvals' pos'-}
    newVal = DF.foldr (\(!b,!p) !num -> num & bitAt (fromIntegral p) .~ b) grayd combd



listVec :: Int -> V.Vector Int
listVec n = let res = fromListN n [1 .. n] in res
{-#SCC listVecs #-}
listVecs :: V.Vector (V.Vector Int)
listVecs = fromListN 128 [listVec n | n <- ([1..128] :: [Int])]
{-#INLINABLE bitsNucleotides #-}
{-#SCC bitsNucleotides #-}
bitsNucleotides :: (IsGeneticMaterial a, Bool ~ Nucleotide a, Bits a) => a -> Nucleotides a
bitsNucleotides !a =
    let
        (sz:: Int) = fromIntegral (geneticMaterialSize a)
        indices = if sz <= 128 then listVecs ! (sz - 1) else fromListN sz [1 .. sz]
    in AVector $! fmap (testBit a) indices

{-#INLINABLE bitsViewNucleotide #-}
{-#SCC bitsViewNucleotide #-}
bitsViewNucleotide :: (Bool ~ Nucleotide a, Bits a) => a -> NucleotidePosition -> Nucleotide a
bitsViewNucleotide !a !pos = testBit a (fromIntegral pos)

{-#SCC bitsConstruct #-}
{-#INLINABLE bitsConstruct #-}
bitsConstruct :: (Bits a, Bool ~ Nucleotide a) => Nucleotides a -> a
bitsConstruct nucls = V.ifoldr (\i (!n) (!val) -> if n then setBit val i else val) zeroBits (toVec nucls)


{-instance Distribution d a => Distribution d (GrayCoded a) where
    rvarT params = do
        val <- rvarT (coerce @(d (GrayCoded a)) params)
        return (GrayCode val)-}

{-instance Distribution StdUniform a => Distribution StdUniform (GrayCoded a) where
    rvarT _ = do
        val <- rvarT StdUniform
        return (GrayCode val)
-}



instance forall a. (FiniteBits a, Num a, DB.Binary a) => IsGeneticMaterial (GrayCoded a) where
    type Nucleotide (GrayCoded a) = Bool
    {-# INLINABLE toByteString #-}
    {-# INLINABLE fromByteString #-}
    toByteString = BSL.toStrict . DB.encode
    fromByteString = DB.decode . BSL.fromStrict
    {-#INLINABLE crossover #-}
    crossover = grayCodeCrossover
    {-#INLINABLE geneticMaterialSize #-}
    geneticMaterialSize = finiteBitsGeneticMaterialSize
    {-#INLINABLE substituteNucleotide #-}
    substituteNucleotide = graySubstituteNucleotide
    {-#INLINABLE substituteNucleotides #-}
    {-#SCC substituteNucleotides "grayCoded_substituteNucleotides" #-}
    substituteNucleotides newvals positions = graySubstituteNucleotides (zipWith (\(!b) (!p) -> (b, fromIntegral p)) (toVec newvals) (toVec positions))
    {-#INLINABLE nucleotides #-}
    nucleotides = bitsNucleotides
    {-#INLINABLE viewNucleotide #-}
    viewNucleotide = bitsViewNucleotide
    {-#INLINABLE construct #-}
    construct = bitsConstruct

deriving via GrayCoded Word8 instance IsGeneticMaterial Word8
deriving via GrayCoded Int instance IsGeneticMaterial Int
deriving via GrayCoded Word16 instance IsGeneticMaterial Word16
deriving via GrayCoded Word32 instance IsGeneticMaterial Word32
deriving via GrayCoded Word64 instance IsGeneticMaterial Word64
\end{code}

Another common class of genetic material are \glspl{enum}. We represent these as a single, atomic \gls{nucleotide}, with each \gls{enum} value being represented by a distinct possible \gls{nucleotide}.
\begin{code}


instance (Enum a) => IsGeneticMaterial (EnumGene a) where
    type Nucleotide (EnumGene a) = a
    type GeneticMaterialKind (EnumGene a) = 'Atomic
    {-# INLINABLE construct #-}
    {-# INLINABLE nucleotides #-}
    {-#INLINE geneticMaterialSize #-}
    {-#INLINE substituteNucleotide #-}
    {-#INLINE viewNucleotide #-}
    {-#INLINE toByteString #-}
    {-#INLINE fromByteString #-}
    {-# SCC construct #-}
    {-# SCC nucleotides #-}
    {-#SCC geneticMaterialSize #-}
    {-#SCC substituteNucleotide #-}
    {-#SCC viewNucleotide #-}
    {-#SCC toByteString #-}
    {-#SCC fromByteString #-}
    geneticMaterialSize _ = 1
    substituteNucleotide a _ _ = AsEnum a -- TODO: Something better
    nucleotides (AsEnum a) = ASingleton a
    viewNucleotide (AsEnum a) _ = a
    construct a = AsEnum $! a ^. (singular traversed)
    toByteString = BSL.toStrict . DB.encode . fromEnum
    fromByteString = toEnum . DB.decode . BSL.fromStrict


deriving via EnumGene Bool instance IsGeneticMaterial Bool
deriving via EnumGene Ordering instance IsGeneticMaterial Ordering


instance forall a. (Enum a, Bounded a) => IsGeneticMaterial (GrayCodedEnumGene a) where
    type Nucleotide (GrayCodedEnumGene a) = Bool
    {-# INLINABLE construct #-}
    {-# INLINABLE nucleotides #-}
    crossover params a b = toEnum $ grayCodeCrossover  params (fromEnum a) (fromEnum b)
    geneticMaterialSize _ = let range = (fromEnum (maxBound :: a) ) - (fromEnum (minBound :: a)) in fromIntegral $ finiteBitSize range - 1 - countLeadingZeros range
    toByteString =  toByteString . fromEnum
    -- |TODO: Fix to take into account valid toEnum
    fromByteString = toEnum . fromByteString
    construct = toEnum . bitsConstruct
    -- |TODO: Remove the empty nucleotides? It looks like
    -- |bitsNucleotides should do it already, though.
    nucleotides = bitsNucleotides . fromEnum
    viewNucleotide = bitsViewNucleotide . fromEnum
    -- |TODO: Factor this out into a func to share with
    -- |substitute and fromBS
    canSubstituteNucleotide n pos a =
        let max' = (fromEnum (maxBound :: a))
            min' = (fromEnum (minBound:: a))
            a' = graySubstituteNucleotide n pos (fromEnum a)
        in if a' > max' || a' < min' then False else True
    -- |TODO: For the mutate func, perhaps do it cyclically.
    -- |e.g. don't do point errors, but translocation errors?
    substituteNucleotide n pos a =
        let max' = (fromEnum (maxBound :: a))
            min' = (fromEnum (minBound :: a))
            a' = graySubstituteNucleotide n pos (fromEnum a)
        in if a' > max' || a' < min' then a else toEnum a'

deriving via EnumGene Operator instance IsGeneticMaterial Operator
deriving via GrayCodedEnumGene Char instance IsGeneticMaterial Char
\end{code}

We now introduce some default implementations for basic structures.
\begin{code}
instance (IsGeneticMaterial a, IsGeneticMaterial b) => IsGeneticMaterial (a,b) where
    type Nucleotide (a,b) = Either (Nucleotide a) (Nucleotide b)
    type GeneticMaterialKind (a,b) = 'ADT
    toByteString (a,b) = BSL.toStrict $ DB.encode (toByteString a, toByteString b)
    fromByteString bs = let (aBS, bBS) = DB.decode . BSL.fromStrict $ bs in (fromByteString aBS, fromByteString bBS)

    nucleotides (a, b) = AVector $ ( toVec $ fmap Left $ nucleotides a) ++  (toVec $ fmap Right $ nucleotides b)
    construct l = (construct (ATraversable as), construct (ATraversable bs)) where
        (as,bs) = partitionEithers . DF.toList $ l
    canSubstituteNucleotide (Left an) pos (a, _) =
        let sza = geneticMaterialSize a in
            if pos < sza
                then canSubstituteNucleotide an pos a
                else False
    canSubstituteNucleotide (Right bn) pos (a , b) =
        let sza = geneticMaterialSize a in
            if pos >= sza
                then canSubstituteNucleotide bn (pos - sza) b
                else False

class GenericGeneticMaterial f where
    type (GNucleotide f) :: k -> *
    gtoByteString :: f p -> BS.ByteString
    gfromByteString :: BS.ByteString -> f p
    gconstruct :: [GNucleotide f p] -> f p
    gnucleotides :: f p -> [GNucleotide f p]

instance GenericGeneticMaterial U1 where
    type GNucleotide U1 = U1
    gtoByteString _ = BS.empty
    gfromByteString _ = U1
    gconstruct _ = U1
    gnucleotides _ = []



instance (IsGeneticMaterial a) => GenericGeneticMaterial (K1 i a) where
    type GNucleotide (K1 i a) = K1 i (Nucleotide a)
    gtoByteString (K1 a) = toByteString a
    gfromByteString = K1 . fromByteString
    gconstruct = K1 . construct . ATraversable . (fmap unK1)
    gnucleotides = DF.toList . (fmap K1) . nucleotides . unK1

instance (GenericGeneticMaterial f) => GenericGeneticMaterial (M1 i t f) where
    type GNucleotide (M1 i t f) = M1 i t (GNucleotide f)
    gtoByteString (M1 a) = gtoByteString a
    gfromByteString = M1 . gfromByteString
    gconstruct = M1 . gconstruct . (fmap unM1)
    gnucleotides = (fmap M1) . gnucleotides . unM1
instance (GenericGeneticMaterial a, GenericGeneticMaterial b) => GenericGeneticMaterial (a :*: b) where
    type GNucleotide (a :*: b) = (GNucleotide a) :+: (GNucleotide b)
    gtoByteString (a :*: b) = BSL.toStrict $ DB.encode ((gtoByteString a), (gtoByteString b))
    gfromByteString bs = let (aBS , bBS) = DB.decode . BSL.fromStrict $ bs in (gfromByteString aBS :*: gfromByteString bBS)
    gconstruct l = (gconstruct as) :*: (gconstruct bs) where
        (as,bs) = partitionEithers . (fmap toEithers) . DF.toList $ l
        toEithers (L1 x) = Left x
        toEithers (R1 y) = Right y
    gnucleotides (a :*: b) = DF.concat [(fmap L1 (gnucleotides a)), (fmap R1 (gnucleotides b))]



-- TODO: Figure out how to get non-coding DNA working for generic sums
{-instance (GenericGeneticMaterial a, GenericGeneticMaterial b) =>
    GenericGeneticMaterial (a :+: b) where
    type GNucleotide (a :+: b) = (GNucleotide a) :+: (GNucleotide b)
    gtoByteString (L1 a) = BSL.toStrict . DB.encode .
    (Left :: BS.ByteString ->
        Either BS.ByteString BS.ByteString) `flip &` gtoByteString  a
    gtoByteString (R1 b) = BSL.toStrict . DB.encode .
    (Right :: BS.ByteString
    -> Either BS.ByteString BS.ByteString) . gtoByteString `flip &` b
    gfromByteString z = case DB.decode (BSL.fromStrict z) of
                            Left a -> L1 (gfromByteString a)
                            Right b -> R1 (gfromByteString b)
    gconstruct -}


instance IsGeneticMaterial (U1 p) where
    type GeneticMaterialKind (U1 p) = 'Atomic
    type Nucleotide (U1 p) = U1 p
    toByteString U1 = BS.empty
    fromByteString _ = U1
    construct _ = U1
    nucleotides _ = ATraversable [U1]
    geneticMaterialSize _ = 1
    viewNucleotide _ _ = U1
    substituteNucleotide _ _ _ = U1


instance (IsGeneticMaterial (a p), IsGeneticMaterial (b p)) => IsGeneticMaterial ((a :*: b) p) where
    type Nucleotide ((a :*: b) p) = Either (Nucleotide (a p)) (Nucleotide (b p))
    type GeneticMaterialKind ((a :*: b) p) = 'ADT
    toByteString (a :*: b) = BSL.toStrict . DB.encode
        $ ((toByteString a), (toByteString b))
    fromByteString bs = let (aBS , bBS) = DB.decode . BSL.fromStrict $ bs
        in (fromByteString aBS :*: fromByteString bBS)
    nucleotides (a :*: b) = AVector $ ( toVec $ fmap Left $ nucleotides a) ++  (toVec $ fmap Right $ nucleotides b)
    construct l =
        (construct (ATraversable as) :*: construct (ATraversable bs)) where
            (as,bs) = partitionEithers . DF.toList $ l
    canSubstituteNucleotide (Left an) pos (a :*: _) =
        let sza = geneticMaterialSize a in
            if pos < sza
                then canSubstituteNucleotide an pos a
                else False
    canSubstituteNucleotide (Right bn) pos (a :*: b) =
        let sza = geneticMaterialSize a in
            if pos >= sza
                then canSubstituteNucleotide bn (pos - sza) b
                else False

instance (IsGeneticMaterial a) => IsGeneticMaterial (K1 i a p) where
    type Nucleotide (K1 i a p) = Nucleotide a
    type GeneticMaterialKind (K1 i a p) = GeneticMaterialKind a
    toByteString (K1 a) = toByteString a
    fromByteString a = K1 (fromByteString a)
    nucleotides (K1 a) = nucleotides a
    construct l = K1 (construct l)
    canSubstituteNucleotide n pos (K1 a) = canSubstituteNucleotide n pos a
    crossover params (K1 a) (K1 b) = K1 $ crossover params a b

instance (IsGeneticMaterial (f p)) => IsGeneticMaterial (M1 i t f p) where
    type Nucleotide (M1 i t f p) = Nucleotide (f p)
    type GeneticMaterialKind (M1 i t f p) = GeneticMaterialKind (f p)
    toByteString (M1 a) = toByteString a
    fromByteString a = M1 (fromByteString a)
    nucleotides (M1 a) = nucleotides a
    construct l = M1 (construct l)
    canSubstituteNucleotide n pos (M1 a) = canSubstituteNucleotide n pos a
    crossover params (M1 a) (M1 b) = M1 $ crossover params a b

\end{code}

This (currently unused) \gls{typeclass} is to represent variable-length genetic material, such as a list.
\begin{code}
class IsGeneticMaterial a => IsVariableLengthGeneticMaterial a where
    -- |Inject genetic material at a specific location.
    insertNucleotides :: NucleotidePosition ->
        TraversableContainer (Nucleotide a) -> a -> a

    -- |Remove genetic material at a specific location.
    deleteNucleotides :: Run -> a -> a

    geneticSizeRange :: a -> (Natural, Natural)


\end{code}
\comment{
-- TODO: List, ByteString and Text instances
-- TODO: How to do this for ADTs?
Oki I just figured out a shitty way to do genetic sums. Simply turn it into a pair like (ConstructorID, Map ConstructorID Gene)
Products is probably easiest to do as just a traversable container of genes

Then wrap both of those with a Generic instance
19:06
W-types would be weird though :/
Actually, it'd simulate non-coding DNA quite well}
