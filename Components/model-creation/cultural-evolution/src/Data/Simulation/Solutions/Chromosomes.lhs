\comment{

\begin{code}
module Data.Simulation.Solutions.Chromosomes where
import Prelude hiding (splitAt, drop, take, reverse, length, (++))
import Data.Simulation.Types
import Data.Simulation.Internal.Utils
import Data.Simulation.Internal.Config
import           Data.Simulation.Solutions.Genes
import Control.Lens
import Data.Vector (Vector)
import Data.Vector.Generic as V hiding (Vector)
import qualified Control.Monad as CM (replicateM)
import qualified Data.Foldable as DF
import Data.Typeable
import qualified Data.HashMap.Strict as HMS
import Polysemy
import Polysemy.RandomFu
import Data.Random.Distribution.Uniform
import Data.Random.Distribution.Bernoulli
\end{code}
}
\chapter{Chromosomes}
A \gls{chromosome} is simply an ordered collection of genes. Genes may be rearranged during reproduction, or even edited during an individual's lifetime. One particular example would be \glspl{plasmid} and other mechanisms of \gls{hgt}, a case of non-Darwinian evolution.
\begin{code}


type Chromosomes = TraversableContainer Chromosome
class HasGenes a => IsChromosome a where
    fromGenes :: Traversable t => t Gene -> a
    {-#INLINE splitChromosome #-}
    splitChromosome :: GenePosition -> a -> (a,a)
    splitChromosome n a = {-#SCC defaultSplitChromosome#-}let g = toVec (genes a) in  splitAt (fromIntegral n) g & both %~ ( fromGenes . AVector)
    {-#INLINE chromosomeSize #-}
    chromosomeSize :: a -> NucleotidePosition
    chromosomeSize = {-#SCC defaultChromosomzeSize #-} sumOf (confusing (asGenes. traversed).Control.Lens.to geneSize')
    {-#INLINE joinChromosome #-}
    joinChromosome :: a -> a -> a
    joinChromosome a b = fromGenes $ (toVec . genes $ a) ++ (toVec . genes $ b)
    {-#INLINE reverseChromosome #-}
    reverseChromosome :: a -> a
    reverseChromosome = fromGenes . reverse . toVec . genes
    {-#INLINE reverseChromosomeSegment #-}
    reverseChromosomeSegment :: Run -> a -> a
    reverseChromosomeSegment r a = let (x,y,z) = exciseChromosomeSegment r a
        in (joinChromosome x (reverseChromosome y)) `joinChromosome` z
    {-#INLINABLE exciseChromosomeSegment #-}
    exciseChromosomeSegment :: Run -> a -> (a,a,a)
    exciseChromosomeSegment (Run s l) a = let
            (x,w) = splitAt (fromIntegral s) . toVec $ genes a
            (y,z) = splitAt (fromIntegral l) w
        in (fromGenes x, fromGenes y, fromGenes z)
    createNewGene :: Member RandomFu r => GeneInitialisationParameters -> a ->
        Maybe GenePosition -> Sem r Gene
    {-#INLINABLE mutateChromosome #-}
    mutateChromosome :: Member RandomFu r =>  MutationTarget -> MutationKind -> a -> Sem r (TraversableContainer a)
    mutateChromosome MutateGenes k chrom= do
        let g = chrom ^. asGenes
            k' = cropMutation k (chromosomeSize chrom)
        newgenes <- mutateGenes k' g
        let newchrom = fromGenes newgenes
        return (ASingleton newchrom)
    mutateChromosome MutateChromosomes (Reversal r (Run 0 0)) c = do
        let (sz :: Int) = fromIntegral  $ chromosomeSize c
        s <- sampleRVar (uniform 0 sz)
        l <- sampleRVar (uniform 1 (10::Int))
        mutateChromosome MutateChromosomes (Reversal r (Run (fromIntegral s) (fromIntegral l))) c
    mutateChromosome MutateChromosomes k@(Reversal rate _) c = do
        doit <- sampleRVar (bernoulli rate)
        if not doit
        then
            return (ASingleton c)
        else do
            let (Reversal rate r) = cropMutation k (chromosomeSize c)
            return (ASingleton $ reverseChromosomeSegment r c)
    mutateChromosome MutateChromosomes (NewRandomStretch r pres (Just (Run s l)) gparams) c = do
        doIt <- sampleRVar (bernoulli r)
        if not doIt then return (ASingleton c) else do
            (newGenes ::Vector Gene) <- V.replicateM (fromIntegral l) (createNewGene gparams c Nothing) --TODO fix the nothing
            let (a,b) = splitChromosome s c
            let b' = joinChromosome (fromGenes newGenes) b
            return $ ASingleton (a `joinChromosome` b')
    mutateChromosome MutateChromosomes (NewRandomStretch rate pres Nothing gparams) c = do
        pos <- sampleRVar (uniform (0 :: Int) (fromIntegral (chromosomeSize c)))  -- TODO Add config for this
        len <- sampleRVar (uniform 1 (5 :: Int))
        mutateChromosome MutateChromosomes (NewRandomStretch rate pres (Just (Run (fromIntegral pos) (fromIntegral len))) gparams) c

    {-#INLINABLE chromosomalCrossover #-}
    chromosomalCrossover :: NucleotidePosition -> a -> a -> (a,a)
    chromosomalCrossover point a b =
        (joinChromosome x1 y2, joinChromosome x2 y1) where
            (x1,y1) = splitChromosome point a
            (x2,y2) = splitChromosome point b
    {-#INLINABLE recombination #-}
    recombination :: Member RandomFu r => Configuration -> a -> a -> Sem r (a,a)
    recombination config a b = do
        let freq = config ^. reproduction . sexual . crossoverFrequency
            sz = min (chromosomeSize a) (chromosomeSize b)
        trials <- sampleRVar $ CM.replicateM (fromIntegral sz) (bernoulli freq)
        let locations = bernoulliTrials trials 0 sz
        return $ DF.foldr (\(!pos) (!x,!y) -> chromosomalCrossover pos x y) (a,b) locations

    {-#INLINE chromosomeType #-}
    chromosomeType :: a -> ChromosomeType
    chromosomeType _ = Linear
    {-#INLINE chromosomeName #-}
    chromosomeName :: a -> String
    chromosomeName _ = "Unnamed chromosome"

{-#SPECIALISE INLINE chromosomeSize :: (Typeable a, IsGene a) =>
    Vector a -> NucleotidePosition #-}
{-#SPECIALISE mutateChromosome :: Member RandomFu r =>
    MutationTarget -> MutationKind ->
    TraversableContainer Gene ->
    Sem r (TraversableContainer (TraversableContainer Gene)) #-}
{-#SPECIALISE INLINE mutateChromosome :: Member RandomFu r =>
    MutationTarget ->  MutationKind ->
    Vector a -> Sem r (TraversableContainer (Vector a)) #-}

{-#SPECIALISE INLINE asGenes :: (Typeable a, IsGene a) =>
    Iso' (Vector a) (TraversableContainer Gene) #-}
{-#INLINABLE asGenes #-}
asGenes :: IsChromosome a => Iso' a (TraversableContainer Gene)
asGenes = iso genes fromGenes


instance IsChromosome (TraversableContainer Gene) where
    {-#INLINE fromGenes #-}
    fromGenes = aTraversable
    {-#INLINE splitChromosome #-}
    splitChromosome n a = let (x,y) = toVec a & splitAt (fromIntegral n) in (AVector x, AVector y)
    {-#INLINE joinChromosome #-}
    joinChromosome a b = AVector $ (toVec a) ++ (toVec b)
    {-#INLINE reverseChromosome #-}
    reverseChromosome a = AVector $ reverse (toVec a)
    {-#INLINE exciseChromosomeSegment #-}
    exciseChromosomeSegment (Run !s !l) !a = let
            (x,w) = splitAt (fromIntegral s) $ toVec a
            (y,z) = splitAt (fromIntegral l) w
        in (AVector x, AVector y, AVector z)


instance (Typeable a, IsGene a) => IsChromosome (Vector a) where
    {-#INLINE fromGenes #-}
    {-#SPECIALISE INLINE fromGenes :: (Typeable a, IsGene a) => Vector Gene -> Vector a #-}
    fromGenes = vecOf (confusing (traversed._AGene))
    {-#INLINE splitChromosome #-}
    splitChromosome !n !a = splitAt (fromIntegral n) a
    {-#INLINE joinChromosome #-}
    joinChromosome = (++)
    {-#INLINE chromosomeSize #-}
    chromosomeSize !a = V.sum (fmap geneSize a)
    {-#INLINE reverseChromosome #-}
    reverseChromosome = reverse
    {-#INLINE exciseChromosomeSegment #-}
    exciseChromosomeSegment (Run !s !l) !a = let
            (x,w) = splitAt (fromIntegral s) a
            (y,z) = splitAt (fromIntegral l) w
        in (x, y, z)
    {-#INLINE createNewGene #-}
    createNewGene gparams _ _ = AGene <$> initialiseWith @a gparams

    {-#INLINABLE mutateChromosome #-}
    mutateChromosome MutateGenes k g= do
        let k' = cropMutation k (chromosomeSize g)
        newGenes <- mutateGenes k' g
        let newchrom = fromGenes newGenes
        return (ASingleton newchrom)
    mutateChromosome MutateChromosomes (Reversal r (Run 0 0)) c = do
        let (sz :: Int) = fromIntegral  $ chromosomeSize c
        s <- sampleRVar (uniform  0 sz)
        l <- sampleRVar (uniform 1 (10::Int))
        mutateChromosome MutateChromosomes (Reversal r (Run (fromIntegral s) (fromIntegral l))) c
    mutateChromosome MutateChromosomes k@(Reversal rate _) c = do
        doit <- sampleRVar (bernoulli rate)
        if not (doit) then return $ ASingleton c else do
            let (Reversal _ r) = cropMutation k (chromosomeSize c)
            return (ASingleton $ reverseChromosomeSegment r c)

    mutateChromosome MutateChromosomes (NewRandomStretch r pres (Just (Run s l)) gparams) c = do
        doIt <- sampleRVar (bernoulli r)
        if not doIt then return (ASingleton c) else do
            newGenes <- V.replicateM (fromIntegral l) (initialiseWith @a gparams) --TODO fix the nothing
            let (a,b) = splitChromosome s c
            return $ ASingleton (V.concat ([a, newGenes, b]))
    mutateChromosome MutateChromosomes (NewRandomStretch r pres Nothing gparams) c = do
        pos <- sampleRVar (uniform (0 :: Int) (fromIntegral (chromosomeSize c) ))
        len <- sampleRVar (uniform 1 (5 :: Int))
        mutateChromosome MutateChromosomes (NewRandomStretch r pres (Just (Run (fromIntegral pos) (fromIntegral len))) gparams) c

data Chromosome where
    AChromosome :: (Typeable a, IsChromosome a) => !a -> Chromosome
    deriving Typeable

{-#INLINABLE mutateChromosome' #-}
mutateChromosome' :: Member RandomFu r =>  MutationTarget -> MutationKind ->
    Chromosome -> Sem r Chromosomes
mutateChromosome' t k (AChromosome a) = fmap  (fmap (AChromosome )) $ mutateChromosome t k a
{-#INLINABLE _AChromosome #-}
_AChromosome :: (Typeable a, IsChromosome a) => Prism' Chromosome a
_AChromosome = prism' AChromosome (\(AChromosome g) -> cast g)
\end{code}