{-#LANGUAGE DeriveAnyClass, OverloadedLabels, DuplicateRecordFields, FunctionalDependencies, StrictData #-}
{-# OPTIONS -funbox-small-strict-fields #-}
module Data.Simulation.Internal.Config (module Data.Simulation.Internal.Configuration.Environment,
module Data.Simulation.Internal.Configuration.Individuals,
module Data.Simulation.Internal.Configuration.Logging,
module Data.Simulation.Internal.Configuration.Reproduction,
module Data.Simulation.Internal.Configuration.Run,
module Data.Simulation.Internal.Configuration.Population,
Configuration, environment, logging, individuals, reproduction, run,logData, defaultConfig, getConfiguration, strategy, minimumInitialGenomeSize, sexual, asexual) where

import Data.Simulation.Internal.Configuration.Individuals

import           Data.Simulation.Domains.SymbolicRegression.Operations
import Data.Simulation.Internal.Configuration.Logging

import           Data.Simulation.Internal.Configuration.Run
import           Data.Simulation.Internal.Configuration.Environment
import           Data.Simulation.Internal.Configuration.Reproduction
import           Data.Simulation.Internal.Configuration.Population
import qualified System.Etc                    as Etc



import           Control.Lens.TH
import qualified Data.Aeson                    as JSON
import           GHC.Generics
import           Control.DeepSeq (NFData, force)
import           Data.Aeson.TH                 as JSON.TH
import qualified Data.HashSet as HS
import           Data.Proxy
import Control.Lens
import Debug.Trace
import System.Mem
import GHC.Stack
import           System.FilePath
import           Text.Printf
import Language.Haskell.TH.Syntax
import Data.Simulation.Internal.Configuration.TH
makeFieldsNoPrefix ''ReproductionConfiguration

makeFieldsNoPrefix ''IndividualInitialisationConfiguration











data AllowedOperationsConfiguration = AllowedOperationsConfiguration {_everything :: Bool, _ops :: HS.HashSet Operator} -- TODO Turn to a bitvector
    deriving stock (Eq, Ord, Show, Read, Generic)
    deriving anyclass NFData

{-#INLINABLE isOperationAllowed #-}
isOperationAllowed :: AllowedOperationsConfiguration -> Operator -> Bool
isOperationAllowed (AllowedOperationsConfiguration True _) _ = True
isOperationAllowed (AllowedOperationsConfiguration _ ops) operator = HS.member operator ops


makeFieldsNoPrefix  ''AllowedOperationsConfiguration
instance JSON.FromJSON AllowedOperationsConfiguration where
    parseJSON = JSON.withText "AllowedOperationsConfiguration" $
        \v -> do
            if v == "everything" then return (AllowedOperationsConfiguration True HS.empty)
            else error "AllowedOperationsConfiguration: Unimplemented fromJSON"

$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''AllowedOperationsConfiguration)


data SymbolicRegressionConfiguration = SymbolicRegressionConfiguration {_allowedOperations :: AllowedOperationsConfiguration}
    deriving stock (Eq, Ord, Show, Read, Generic)
    deriving anyclass NFData

makeFieldsNoPrefix ''SymbolicRegressionConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''SymbolicRegressionConfiguration)
data DomainsConfiguration = DomainsConfiguration {_symbolicRegression :: SymbolicRegressionConfiguration}
    deriving stock (Eq, Ord, Show, Read, Generic)
    deriving anyclass NFData

makeFieldsNoPrefix ''DomainsConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''DomainsConfiguration)



data Configuration =
    Configuration  {_individuals :: IndividualConfiguration, _reproduction :: ReproductionConfiguration, _population :: PopulationConfiguration, _environment :: EnvironmentConfiguration, _run :: RunConfiguration, _logging :: LoggingConfiguration}
    deriving (Eq, Ord, Show, Read, Generic)
    deriving anyclass NFData

makeFieldsNoPrefix ''Configuration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''Configuration)


{-#INLINABLE logData #-}
{-#SCC logData #-}
logData :: HasCallStack => Configuration -> Getting Bool LoggingConfiguration Bool -> String -> a -> a
logData = withFrozenCallStack (\config path message object  -> let doIt = config ^. logging . path in if doIt then trace (message ++ (let (_,sl)  = head (getCallStack callStack) in printf " at %s:%i"
                                (takeFileName (srcLocFile sl))
                                (srcLocStartLine sl))) object else object)


configSpec :: Etc.ConfigSpec ()
configSpec =
     $(Etc.readConfigSpecTH (Proxy :: Proxy ()) $(configFilePath))

defaultConfig :: Etc.Config
defaultConfig = Etc.resolveDefault configSpec

{-# SCC getConfiguration #-}
getConfiguration :: IO Configuration
getConfiguration = do
    traceMarkerIO "Cultural Evolution - loading config"
    Etc.reportEnvMisspellingWarnings configSpec
    (fileConfig, _fileWarnings) <- Etc.resolveFiles configSpec
    envConfig  <- Etc.resolveEnv configSpec
    cliConfig  <- Etc.resolvePlainCli configSpec
    let !config = cliConfig `mappend` envConfig `mappend` fileConfig `mappend` defaultConfig
    configuration <- force <$> Etc.getConfigValue ["culturalEvolution"] config
    performMajorGC
    traceMarkerIO "Cultural Evolution - config loaded"
    return configuration
