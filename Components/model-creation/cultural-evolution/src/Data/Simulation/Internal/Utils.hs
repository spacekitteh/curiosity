{-#OPTIONS_GHC -Wno-orphans #-}
module Data.Simulation.Internal.Utils where
import           Data.Simulation.Types
import           Numeric.Natural
import           Control.Parallel.Strategies
import           GHC.Types                      ( SPEC(..) )
--import           Data.Sequence as DS
import           Control.Monad
import           Control.Lens
import           GHC.Conc
import qualified Data.Vector                   as V
import qualified Data.Vector.Generic           as VG
import qualified Control.Monad.ST              as STStrict
import qualified GHC.Exts
import           Data.Random.Distribution.Binomial
import           Polysemy
import           Polysemy.RandomFu
import           Data.Random

import           Data.Random.Source
import           System.Random.PCG.Fast.Pure    ( Gen
                                                , uniformW8
                                                , uniformW16
                                                , uniformW32
                                                , uniformW64
                                                , uniformD
                                                )

$(randomSource
    [d| instance RandomSource (STStrict.ST s) (Gen s) where
            getRandomWord8From  = uniformW8
            getRandomWord16From = uniformW16
            getRandomWord32From = uniformW32
            getRandomWord64From = uniformW64
            getRandomDoubleFrom = uniformD
     |])


$(randomSource
    [d| instance RandomSource IO (Gen GHC.Exts.RealWorld) where
            getRandomWord8From  = uniformW8
            getRandomWord16From = uniformW16
            getRandomWord32From = uniformW32
            getRandomWord64From = uniformW64
            getRandomDoubleFrom = uniformD
     |])


--import qualified Data.Vector.Algorithms.Tim    as DVAT
                                                --( sortBy )
{-# INLINABLE [1] parFmap #-}
parFmap :: Traversable t => (a -> b) -> t a -> t b
parFmap f ~a = withStrategy (parTraversable rseq) . (fmap f) $ a
{-# INLINE parFmapIO #-}
parFmapIO :: Traversable t => (a -> b) -> t a -> IO (t b)
parFmapIO f a = withStrategyIO (parTraversable rseq) . (fmap f) $ a
{-#RULES "parFmap fuse" forall f g a . parFmap f (parFmap g a) = parFmap (f . g)  a #-}

{-#RULES "parFmapTC fuse" forall f g a.  parFmapTraversableContainer f  (parFmapTraversableContainer g a) = parFmapTraversableContainer (f . g) a #-}
{-#RULES "parFmapVec fuse" forall f g a. parFmapVec f (parFmapVec g a) = parFmapVec (f . g) a #-}
{-# RULES "parFmap/TC" forall f a. parFmap f  a= parFmapTraversableContainer f a #-}
{-#RULES  "parFmapTC/vec" forall (f :: forall a b. NFData b => (a -> b)) (v :: V.Vector a).  parFmapTraversableContainer f (AVector v) = AVector (parFmapVec f v) #-}
{-#RULES  "parFmap/vec" forall (f :: forall a b. NFData b => (a -> b)) (v :: V.Vector a).  parFmap f (AVector v) = AVector (parFmapVec f v) #-}
{-# INLINE [1] parFmapTraversableContainer #-}
parFmapTraversableContainer
    :: (a -> b) -> TraversableContainer a -> TraversableContainer b
parFmapTraversableContainer f (ASingleton a) =
    ASingleton $! withStrategy rpar (f a)
parFmapTraversableContainer f (AVector      a) = AVector $ parFmap f a
parFmapTraversableContainer f (ATraversable a) = ATraversable $ parFmap f a

{-# INLINE [1] parFmapVec #-}
{-#SPECIALISE INLINE [1] parFmapVec :: (a -> b) -> V.Vector a -> V.Vector b #-}
parFmapVec
    :: (VG.Vector v a, VG.Vector v b){-, NFData b-}
                                      => (a -> b) -> v a -> v b
parFmapVec f ~a =
    ( VG.fromList
        . withStrategy (parListChunk ((VG.length a) `div` 2) rseq)
        . fmap f
        . VG.toList
        )
        a






{-#RULES  "parFmap/vec" forall (f :: forall a b. NFData b => (a -> b)) (v :: V.Vector a).  parFmap f v = parFmapVec f v #-}

{-#SCC generateLocationsByBernoulliTrials #-}
{-# INLINABLE generateLocationsByBernoulliTrials #-}
generateLocationsByBernoulliTrials
    :: Member RandomFu r
    => Double
    -> Natural
    -> NucleotidePosition
    -> Natural
    -> Sem r NucleotidePositions
generateLocationsByBernoulliTrials !r !len !offset !size =
    let
        s      = fromIntegral size
        o      = fromIntegral offset
        l      = fromIntegral len
        capped = min s (o + l)
        count :: Int
        count = capped - o
    in
        do
            -- Instead of allocating a large vector, it's probably more
            -- efficient to allocate a vector where we know the size.
            -- That way, less memory shenanigans occur.
            num <- {-# SCC "binomialSampling" #-} sampleRVar $ binomial count r
            positions <-
                {-#SCC "uniformSampling"#-}sampleRVar $ replicateM num (uniform o capped)
            return $! AVector (V.fromListN num (fmap fromIntegral positions))
            {-v <- uniformVector gen (fromIntegral len)
            let shifted = VU.map (+ o) (VU.findIndices (< r) v)
            return
                $! (AVector . fmap (fromIntegral) . VU.convert . VU.filter (< s)
                   )
                       shifted-}

{-# INLINE bernoulliTrials #-}
-- |A helper function to generate random positions via Bernoulli trials.
-- |It makes sure to add an offset, so it generates absolute loci.
bernoulliTrials
    :: Traversable t
    => t Bool
    -> NucleotidePosition
    -> Natural
    -> NucleotidePositions
bernoulliTrials results !offset !size =
    AVector
        $! ( V.fromList
           . Prelude.filter (< size)
           . fmap ((+ offset) . fromIntegral)
           . elemIndicesOf traversed True
           )
               results


-- TODO FIXME
{-sortBy :: (NFData e) => (e -> e -> Ordering) -> V.Vector e -> V.Vector e
sortBy ord v =
    let assembled = if VG.length v < 2 * numCapabilities
            then v
            else
                let chunks = parMap
                        rdeepseq
                        (\vec -> STStrict.runST $ do
                            thawed <- VG.thaw vec
                           -- DVAT.sortBy ord thawed
                            sorted <- VG.freeze thawed
                            return sorted
                        )
                        (chunkVec numCapabilities v)
                in  chunks `pseq` VG.concat chunks
    in  assembled
            `pseq` (STStrict.runST $ do
                       thawed <- VG.thaw assembled
                       --DVAT.sortBy ord thawed
                       sorted <- VG.freeze thawed
                       return sorted
                   )

-}
chunkVec :: Int -> V.Vector e -> [V.Vector e]
chunkVec n v =
    let len       = VG.length v
        chunkSize = let t = len `div` n in if t * n < len then t + 1 else t
    in  chunkVec' SPEC len chunkSize v  where
    chunkVec' !sPEC !len !chunkSize v'
        | len <= chunkSize = [v']
        | otherwise = let (xs, ys) = VG.splitAt chunkSize v'
                        in  (xs) : (chunkVec' sPEC (len - chunkSize) chunkSize ys)


{-# INLINE parVector #-}
{-#SPECIALISE INLINE parVector :: Strategy a -> Strategy (V.Vector a) #-}
parVector :: VG.Vector v a => Strategy a -> Strategy (v a)
parVector s v =
    liftM VG.fromList
        . parListChunk ((VG.length v) `div` numCapabilities) s
        . VG.toList
        $ v

{- INLINABLE withSeedLazy
withSeedLazy
    :: forall a
     . (forall s . Gen s -> STStrict.ST s a)
    -> Seed
    -> (a, VU.Vector Word32)
withSeedLazy f s = ST.runST $ ST.strictToLazyST $ do
    gen  <- restore s
    a    <- f gen
    seed <- save gen
    return (a, (fromSeed seed))
-}

{-INLINE parChunkedSequence
parChunkedSequence :: NFData a => Seq a -> Seq a
parChunkedSequence a = -- TODO: Figure out why the hell this isn't working in parallel. look at mapM and bang patterns.
    (DF.foldr' (><) empty
            (withStrategy (parTraversable (evalTraversable rdeepseq))  (chunksOf ((1 +) $ (DS.length a) `div` numCapabilities) a)))
-}
{-# INLINABLE iterateM #-}
iterateM :: Monad m => Int -> (a -> m a) -> a -> m a
iterateM 0 _ !a = return a
iterateM !n !f !a =
    let !v = (f a) in v >>= (GHC.Exts.oneShot (iterateM (n - 1) f))

{-# INLINABLE cropMutation #-}
cropMutation :: MutationKind -> NucleotidePosition -> MutationKind
cropMutation (PointErrors rate (Run s !l)) !len =
    PointErrors rate (Run s (min l (len)))
cropMutation nd@(NormalDrift _ _ _) _ = nd
cropMutation (Reversal rate (Run s !l)) !len =
    Reversal rate (Run s (min l (len)))
cropMutation cs@(ConstantScale _ _) _ = cs
cropMutation sp@(SpecialToConstant _) _ = sp
cropMutation a _ = error ("cropMutation unimplemented for " ++ show a)


{-#SCC zoomRun #-}
{-# INLINE zoomRun #-}
zoomRun :: Run -> Natural -> (Maybe Run, Maybe Run)
zoomRun = zoomRun' SPEC

{-#SCC zoomRun' #-}
{-# INLINABLE zoomRun' #-}
zoomRun' :: SPEC -> Run -> Natural -> (Maybe Run, Maybe Run)
zoomRun' !sPEC (Run _ 0) _ = {-#SCC "zero_length_run" #-}(Nothing, Nothing)
zoomRun' !sPEC (Run 0 l) geneLength =
    {-#SCC "zero_start_run" #-}( Just $ Run 0 (min l geneLength)
    , if l <= geneLength then Nothing else Just (Run 0 (l - geneLength))
    )
zoomRun' !sPEC (Run s l) geneLength = {-#SCC "arbitrary_run" #-}(thisRun, thatRun)      where
    oob = s >= geneLength
    thisRun =
        if oob then Nothing else Just $ Run s (min l (geneLength - s))
    thatRun | oob            = (Just $ Run (s - geneLength) l)
            | l > geneLength = Just $ Run 0 (l - (geneLength - s))
            | otherwise      = Nothing


{-# INLINABLE zoomMutation #-}
zoomMutation
    :: MutationKind -> Natural -> (Maybe MutationKind, Maybe MutationKind)
zoomMutation = zoomMutation' SPEC  where
    zoomMutation' !sPEC (PointErrors rate !r) l ={-#SCC "PointErrors" #-}
        let (!a, !b) = zoomRun' sPEC r l
        in  (fmap (PointErrors rate) a, fmap (PointErrors rate) b)
    zoomMutation' _ (Insertion _ _ _) _ =
        error "Unimplemented: zoomMutation Insertion"
    zoomMutation' !sPEC (Deletion r) l =
        let (a, b) = zoomRun' sPEC r l in (fmap Deletion a, fmap Deletion b)
    zoomMutation' _ (Rotation _ _) _ =
        error "Unimplemented: zoomMutation Rotation"
    zoomMutation' !sPEC (Complement r) l =
        {-#SCC "Complement" #-} let (a, b) = zoomRun' sPEC r l in (fmap Complement a, fmap Complement b)
    zoomMutation' !sPEC (Reversal rate r) l =
        {-#SCC "Reversal" #-}let (a, b) = zoomRun' sPEC r l
        in  (fmap (Reversal rate) a, fmap (Reversal rate) b)
   {- zoomMutation' !sPEC (NewRandomStretch (Just r) p) l =
        let (a, b) = zoomRun r l
        in  ( fmap (flip (NewRandomStretch) p) a
            , fmap (flip (NewRandomStretch) p) b
            ) -- This might fuck up with the default behaviour caused by the Nothing-} --TODO FIXME
    zoomMutation' _ m l = {-#SCC "default_case" #-}(Just m, Just m)
