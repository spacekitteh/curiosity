module Data.Simulation.Internal.Configuration.Individuals where
import           Language.Haskell.TH.Syntax     ( Lift )
import           Control.Lens.TH
import           GHC.Generics
import           Control.DeepSeq                ( NFData )
import           Data.Aeson.TH                 as JSON.TH

import           Data.Char                      ( toLower )
import           Numeric.Natural
data IndividualInitialisationStrategy where
    Uniform ::IndividualInitialisationStrategy
    NonUniform ::IndividualInitialisationStrategy
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData

makeClassyPrisms ''IndividualInitialisationStrategy
$(JSON.TH.deriveJSON defaultOptions {constructorTagModifier = fmap toLower, sumEncoding = ObjectWithSingleField } ''IndividualInitialisationStrategy)

data IndividualInitialisationConfiguration =
    IndividualInitialisationConfiguration {_minimumInitialGenomeSize :: Natural, _strategy :: IndividualInitialisationStrategy}
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''IndividualInitialisationConfiguration)


data IndividualConfiguration =
    IndividualConfiguration {_minimumGenomeSize :: Natural, _maximumGenomeSize :: Natural, _initialisation :: IndividualInitialisationConfiguration}
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData

makeFieldsNoPrefix ''IndividualConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''IndividualConfiguration)
