module Data.Simulation.Internal.Configuration.Reproduction where
import           Language.Haskell.TH.Syntax     ( Lift )
import           Control.Lens.TH
import           Data.Aeson                     ( (.:) )
import qualified Data.Aeson                    as JSON
import           GHC.Generics (Generic)
import           Control.DeepSeq                ( NFData )
import           Data.Aeson.TH                 as JSON.TH
import           Numeric.Natural


data AsexualReproductionConfiguration =
    AsexualReproductionConfiguration {_enabled :: Bool}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''AsexualReproductionConfiguration
instance JSON.FromJSON AsexualReproductionConfiguration where
    parseJSON = JSON.withObject "AsexualReproductionConfiguration"
        $ \v -> AsexualReproductionConfiguration <$> v .: "enabled"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''AsexualReproductionConfiguration)
data SexualReproductionConfiguration =
    SexualReproductionConfiguration {_numberOfIndividuals :: Natural, _crossoverFrequency :: Double}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''SexualReproductionConfiguration
instance JSON.FromJSON SexualReproductionConfiguration where
    parseJSON = JSON.withObject "SexualReproductionConfiguration" $ \v ->
        SexualReproductionConfiguration
            <$> v
            .:  "numberOfIndividuals"
            <*> v
            .:  "crossoverFrequency"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''SexualReproductionConfiguration)
data BroodConfiguration =
    BroodConfiguration {_enabled :: Bool, _size :: Natural}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''BroodConfiguration
instance JSON.FromJSON BroodConfiguration where
    parseJSON = JSON.withObject "BroodConfiguration" $ \v ->
        BroodConfiguration <$> (fmap not $ v .: "disabled") <*> v .: "size"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''BroodConfiguration)
data SampledFitnessConfiguration = SampledFitnessConfiguration {_proportion :: Double}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''SampledFitnessConfiguration

$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''SampledFitnessConfiguration)
data ReproductionStrategyConfiguration =
    ReproductionStrategyConfiguration {_brood :: BroodConfiguration, _identicalPhenotypeRejectionRate :: Double, _destructiveReproductionRejectionRate :: Double, _sampledFitness :: SampledFitnessConfiguration}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''ReproductionStrategyConfiguration

$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''ReproductionStrategyConfiguration)
data ReproductionConfiguration =
    ReproductionConfiguration {_asexual :: AsexualReproductionConfiguration, _sexual :: SexualReproductionConfiguration, _strategy :: ReproductionStrategyConfiguration}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData

instance JSON.FromJSON ReproductionConfiguration where
    parseJSON = JSON.withObject "ReproductionConfiguration" $ \v ->
        ReproductionConfiguration
            <$> v
            .:  "asexual"
            <*> v
            .:  "sexual"
            <*> v
            .:  "strategy"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''ReproductionConfiguration)
