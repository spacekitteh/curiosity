module Data.Simulation.Internal.Configuration.Population where
import           Language.Haskell.TH.Syntax     ( Lift )
import           Control.Lens.TH
import           Data.Simulation.Types
import           Data.Aeson                     ( (.:) )
import qualified Data.Aeson                    as JSON
import qualified Data.Aeson.Types              as JSON
                                                ( typeMismatch )
import           GHC.Generics
import           Control.DeepSeq                ( NFData )
import           Data.Aeson.TH                 as JSON.TH



data PopulationConfiguration =
    PopulationConfiguration {_minimumPopulation :: PopulationSize, _maximumPopulation :: PopulationSize, _startingPopulation :: PopulationSize}
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData

makeFieldsNoPrefix ''PopulationConfiguration
instance JSON.FromJSON PopulationConfiguration where
    parseJSON = JSON.withObject "PopulationConfiguration" $ \v ->
        PopulationConfiguration
            <$> v
            .:  "minimumPopulation"
            <*> v
            .:  "maximumPopulation"
            <*> v
            .:  "startingPopulation"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''PopulationConfiguration)
