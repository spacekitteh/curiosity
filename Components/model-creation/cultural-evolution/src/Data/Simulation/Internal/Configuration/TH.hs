module Data.Simulation.Internal.Configuration.TH where
import           Data.Text
import           Data.String
import           Paths_cultural_evolution
import           Language.Haskell.TH.Syntax
import           System.FilePath
import           System.Directory               ( doesFileExist )

configFileName :: IO FilePath
configFileName = do
    asData        <- getDataFileName "config-spec.yaml"
    dataVerExists <- doesFileExist asData
    return $ if dataVerExists then asData else "config-spec.yaml"
configFilePath :: Q Exp
configFilePath = do
    str <- runIO (configFileName)
    addDependentFile str
    [|fromString str|]
