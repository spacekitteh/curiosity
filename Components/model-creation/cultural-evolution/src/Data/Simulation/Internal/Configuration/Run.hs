module Data.Simulation.Internal.Configuration.Run where
import           Language.Haskell.TH.Syntax     ( Lift )
import           Control.Lens.TH
import           Data.Aeson                     ( (.:) )
import qualified Data.Aeson                    as JSON
import           GHC.Generics
import           Control.DeepSeq                ( NFData )
import           Data.Aeson.TH                 as JSON.TH
import           Numeric.Natural

data RunPopulationConfiguration =
    RunPopulationConfiguration {_number :: Natural, _maximumIterationsPerPopulation :: Natural, _maximumTotalIterations :: Natural, _allowInterbreeding :: Bool}
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData

makeFieldsNoPrefix ''RunPopulationConfiguration
instance JSON.FromJSON RunPopulationConfiguration where
    parseJSON = JSON.withObject "RunPopulationConfiguration" $ \v ->
        RunPopulationConfiguration
            <$> v
            .:  "number"
            <*> v
            .:  "maximumIterationsPerPopulation"
            <*> v
            .:  "maximumTotalIterations"
            <*> v
            .:  "allowInterbreeding"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''RunPopulationConfiguration)

data RunCacheConfiguration =
    RunCacheConfiguration {_genotypes :: Bool, _phenotypes :: Bool, _best :: Bool}
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData
makeFieldsNoPrefix ''RunCacheConfiguration
{-instance JSON.FromJSON RunCacheConfiguration where
    parseJSON = JSON.withObject "RunCacheConfiguration" $ \v ->
        RunCacheConfiguration
        <$> v .: "genotypes"
        <*> v .: "phenotypes"
        <*> v .: "best"-}
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''RunCacheConfiguration)


data RunConcurrencyConfiguration =
    RunConcurrencyConfiguration {_chunkSize :: Natural}
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData
makeFieldsNoPrefix ''RunConcurrencyConfiguration
instance JSON.FromJSON RunConcurrencyConfiguration where
    parseJSON = JSON.withObject "RunConcurrencyConfiguration"
        $ \v -> RunConcurrencyConfiguration <$> v .: "chunkSize"

$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''RunConcurrencyConfiguration)
data RunConfiguration =
    RunConfiguration  {_populations :: RunPopulationConfiguration, _cache :: RunCacheConfiguration, _concurrency :: RunConcurrencyConfiguration}
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData
makeFieldsNoPrefix ''RunConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''RunConfiguration)
