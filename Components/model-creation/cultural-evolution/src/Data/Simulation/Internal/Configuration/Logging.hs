module Data.Simulation.Internal.Configuration.Logging where
import           Language.Haskell.TH.Syntax     ( Lift )
import           Control.Lens.TH
import           GHC.Generics
import           Control.DeepSeq                ( NFData )
import           Data.Aeson.TH                 as JSON.TH


data PopulationLoggingConfiguration = PopulationLoggingConfiguration {_generationNumber :: Bool, _newPhenotypes :: Bool, _evaluations :: Bool, _maximumFitness :: Bool, _meanFitness :: Bool, _medianFitness :: Bool}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeClassy ''PopulationLoggingConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''PopulationLoggingConfiguration)

data LoggingConfiguration = LoggingConfiguration {_population :: PopulationLoggingConfiguration}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData


makeFieldsNoPrefix ''LoggingConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''LoggingConfiguration)
