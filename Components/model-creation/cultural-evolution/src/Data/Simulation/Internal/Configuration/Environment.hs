{-# OPTIONS_GHC -Wno-overlapping-patterns #-}
module Data.Simulation.Internal.Configuration.Environment where
import           Language.Haskell.TH.Syntax     ( Lift )
import           Control.Lens.TH
import           Data.Aeson                     ( (.:) )
import qualified Data.Aeson                    as JSON
import qualified Data.Aeson.Types              as JSON
                                                ( typeMismatch )
import           GHC.Generics
import           Control.DeepSeq                ( NFData )
import           Data.Aeson.TH                 as JSON.TH



data DistanceMetric where
    Lp ::{_dimension :: Double} -> DistanceMetric
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData

makeClassyPrisms ''DistanceMetric

instance JSON.FromJSON DistanceMetric where
    parseJSON = JSON.withObject "DistanceMetric" $ \v -> do
        family <- v .: "family"
        case family of
            JSON.String "lp" -> Lp <$> v .: "lpNormDimension"
            _ -> JSON.typeMismatch "Unknown distance metric" family
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''DistanceMetric)
data ImperfectCopyingConfiguration =
    ImperfectCopyingConfiguration {_rate :: Double}
    deriving (Eq, Ord, Show, Read, Generic, Lift)
    deriving anyclass NFData


instance JSON.FromJSON ImperfectCopyingConfiguration where
    parseJSON = JSON.withObject "ImperfectCopyingConfiguration"
        $ \v -> ImperfectCopyingConfiguration <$> v .: "rate"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''ImperfectCopyingConfiguration)

data NormalDriftConfiguration =
    NormalDriftConfiguration {_rate :: Double, _mean :: Double, _standardDeviation :: Double}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''NormalDriftConfiguration
makeFieldsNoPrefix ''ImperfectCopyingConfiguration
instance JSON.FromJSON NormalDriftConfiguration where
    parseJSON = JSON.withObject "NormalDriftConfiguration" $ \v ->
        NormalDriftConfiguration
            <$> v
            .:  "rate"
            <*> v
            .:  "mean"
            <*> v
            .:  "standardDeviation"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''NormalDriftConfiguration)

data ReversalConfiguration =
    ReversalConfiguration {_rate :: Double}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''ReversalConfiguration
instance JSON.FromJSON ReversalConfiguration where
    parseJSON = JSON.withObject "ReversalConfiguration"
        $ \v -> ReversalConfiguration <$> v .: "rate"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''ReversalConfiguration)


data InsertionConfiguration =
    InsertionConfiguration {_rate :: Double, _preservationRate :: Double}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''InsertionConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''InsertionConfiguration)
data NewRandomStretchConfiguration =
    NewRandomStretchConfiguration {_rate :: Double, _preservationRate :: Double}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''NewRandomStretchConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''NewRandomStretchConfiguration)
data OperonExtractionConfiguration =
    OperonExtractionConfiguration {_rate :: Double}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''OperonExtractionConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''OperonExtractionConfiguration)
data SpecialToConstantConfiguration =
    SpecialToConstantConfiguration {_rate :: Double}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''SpecialToConstantConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''SpecialToConstantConfiguration)

data MutationsConfiguration =
    MutationsConfiguration {_imperfectCopying :: ImperfectCopyingConfiguration, _normalDrift :: NormalDriftConfiguration, _insertion :: InsertionConfiguration, _newRandomStretch :: NewRandomStretchConfiguration, _reversal :: ReversalConfiguration, _operonExtraction :: OperonExtractionConfiguration, _specialToConstant :: SpecialToConstantConfiguration}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''MutationsConfiguration
$(JSON.TH.deriveJSON defaultOptions {fieldLabelModifier = drop 1 } ''MutationsConfiguration)
data TimeSeriesConfiguration =
    TimeSeriesConfiguration {_distanceMetric :: DistanceMetric}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''TimeSeriesConfiguration
instance JSON.FromJSON TimeSeriesConfiguration where
    parseJSON = JSON.withObject "TimeSeriesConfiguration"
        $ \v -> TimeSeriesConfiguration <$> v .: "distanceMetric"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''TimeSeriesConfiguration)

data FitnessConfiguration =
    FitnessConfiguration {_timeSeries :: TimeSeriesConfiguration}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''FitnessConfiguration
instance JSON.FromJSON FitnessConfiguration where
    parseJSON = JSON.withObject "FitnessConfiguration"
        $ \v -> FitnessConfiguration <$> v .: "timeSeries"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''FitnessConfiguration)


data EnvironmentConfiguration =    EnvironmentConfiguration {_fitness :: FitnessConfiguration, _mutations :: MutationsConfiguration}
    deriving (Eq, Ord, Show, Read, Lift, Generic)
    deriving anyclass NFData
makeFieldsNoPrefix ''EnvironmentConfiguration
instance JSON.FromJSON EnvironmentConfiguration where
    parseJSON = JSON.withObject "EnvironmentConfiguration" $ \v ->
        EnvironmentConfiguration <$> v .: "fitness" <*> v .: "mutations"
$(JSON.TH.deriveToJSON defaultOptions {fieldLabelModifier = drop 1 } ''EnvironmentConfiguration)
