module Control.Effects.HypothesisTesting where
import Polysemy

import Polysemy.State
import Data.Foldable

data HypothesisProvenance x = Guessed | Deduced x deriving (Eq, Show, Functor)

data HypothesisTesting hypo m a where
    AddHypotheses :: Traversable t => t hypo -> HypothesisTesting hypo m ()

    PopMostRecentHypotheses :: HypothesisTesting hypo m [hypo]
    AllHypotheses :: HypothesisTesting hypo m [[hypo]]


makeSem ''HypothesisTesting

runHypothesisTestingAsList :: Sem (HypothesisTesting hypo : r) a -> Sem (State [[hypo]] : r) a
runHypothesisTestingAsList = reinterpret $ \case
    AddHypotheses h -> modify' ((toList h):)
    AllHypotheses -> get
    PopMostRecentHypotheses -> do
        stack <- get
        case stack of
            [] -> return []
            r:rest -> do
                put rest
                return r