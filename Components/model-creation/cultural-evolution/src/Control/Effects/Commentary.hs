module Control.Effects.Commentary where
import           Polysemy
import           Data.String                    ( IsString )
import GHC.Generics (Generic)
import Data.Data (Data)

type UUID = String
type URI = String
type Synopsis = String
type Reasoning = String
data Requirement where
    RequirementUUID :: UUID -> Requirement
    RequirementURI :: URI -> Requirement
    ARequirement :: Synopsis -> Reasoning -> Requirement
    deriving (Eq, Ord, Show, Generic, Data)
data Commentary m a where
    Comment ::IsString s => s -> Commentary m ()
    Explanation :: IsString s => s -> Commentary m ()
    Assumption ::IsString s => s -> Commentary m ()
    Citation ::IsString s => s -> Commentary m ()
    RequiredBy :: (Traversable t, IsString s) => s -> t Requirement -> Commentary m ()

makeSem ''Commentary

{-# INLINE ignoreCommentary #-}
ignoreCommentary :: InterpreterFor Commentary r
ignoreCommentary = interpret $ \case
    Comment     _ -> pure ()
    Explanation _ -> pure ()
    Assumption  _ -> pure ()
    Citation    _ -> pure ()
    RequiredBy _ _ -> pure ()
