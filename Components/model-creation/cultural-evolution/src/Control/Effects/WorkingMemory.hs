{-#LANGUAGE FunctionalDependencies#-}
module Control.Effects.WorkingMemory where

import           Polysemy
import           GHC.Generics
import           Data.Data
import           Data.Hashable
import qualified Data.HashSet                  as HS
import           Control.Lens

data PortDirection = Undirected | Input | Output deriving (Eq, Ord, Data, Generic, Read, Show)
instance Hashable PortDirection

data Port n l = Port {_portDirection :: PortDirection, _germ :: (Maybe n), _connection :: l} deriving (Eq, Ord, Data, Generic, Read, Show)
instance (Hashable n, Hashable l) => Hashable (Port n l)

class Linkage n l | l -> n where
    ports :: Traversal' l (Port n l)
    nodes :: Traversal' l n

{-class Section g n l | g -> n l, l -> n where
    glue ::
class LinkageStructure g n l  | g -> n l, l -> n where
-}



data GraphConstruction identifier node edge m a where
    AddNode ::node -> GraphConstruction identifier node edge m identifier
    AddEdge ::edge -> GraphConstruction identifier node edge m identifier
    AddNodes ::Traversable t => t node -> GraphConstruction identifier node edge m (t identifier)
    AddEdges ::Traversable t => t edge -> GraphConstruction identifier node edge m (t identifier)
    AddElements ::Traversable t => t node -> t edge -> GraphConstruction identifier node edge m (t identifier,t identifier)
    AddCompleteSubgraph ::Traversable t => t node -> GraphConstruction identifier node edge m (t identifier)

data GraphDeconstruction identifier node edge m a where
    RemoveNode ::identifier -> GraphDeconstruction identifier node edge m ()
    RemoveEdge ::identifier -> GraphDeconstruction identifier node edge m ()
    RemoveNodes ::Traversable t => t identifier -> GraphDeconstruction identifier node edge m ()
    RemoveEdges ::Traversable t => t identifier -> GraphDeconstruction identifier node edge m ()
    RemoveElements ::Traversable t => t identifier -> t identifier -> GraphDeconstruction identifier node edge m ()

data GraphNodeQuery identifier node m a where
    LookupNode ::identifier -> GraphNodeQuery identifier node m node
