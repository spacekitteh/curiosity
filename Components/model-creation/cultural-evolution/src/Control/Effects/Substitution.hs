module Control.Effects.Substitution where
import           Polysemy
import           Polysemy.State
import           Data.HashMap.Strict           as HM
import Data.HashSet as HS
import           Data.Foldable                 as DF
import           Data.Hashable
import Polysemy.Fresh
import Data.Maybe (listToMaybe)

data Substitution var term m a where
    AddSubstitutions ::Traversable t => t (var,term) -> Substitution var term m ()
    LookupVariable ::var -> Substitution var term m (Maybe term)
    Substitutions ::Substitution var term m [(var,term)]
    DefinedSubstitutions :: Substitution var term m (HS.HashSet var)
    MatchesDefinition :: term -> Substitution var term m (Maybe var)
    RemoveMappings :: Traversable t => t var -> Substitution var term m ()
    Define :: term -> Substitution var term m var
makeSem ''Substitution

{-# INLINE runSubstitutionsAsHashMapState #-}
runSubstitutionsAsHashMapState
    :: (Eq term, Eq var, Hashable var, Member (Fresh var) r)
    => Sem (Substitution var term : r) a
    -> Sem (State (HashMap var term) : r) a
runSubstitutionsAsHashMapState = reinterpret $ \case
    AddSubstitutions terms ->
        modify' (HM.union (HM.fromList (DF.toList terms)))
    LookupVariable var -> HM.lookup var <$> get
    Substitutions      -> HM.toList <$> get
    DefinedSubstitutions -> HM.keysSet <$> get
    MatchesDefinition term -> matchesDefinition' term
    RemoveMappings t -> do
        modify' (\mapping -> DF.foldr ( HM.delete) mapping t)
    Define term -> do
        possible <- matchesDefinition' term
        case possible of
            Just v -> return v
            _ -> do
                v <- fresh
                modify' (HM.union (HM.singleton  v term))
                return v
matchesDefinition' :: (Eq term, Member (State (HashMap var term)) r) => term -> Sem r (Maybe var)
matchesDefinition' term = do
        map <- get
        let filtered = HM.filter (== term) map
            theKeys = HM.keys filtered
        return (listToMaybe theKeys)