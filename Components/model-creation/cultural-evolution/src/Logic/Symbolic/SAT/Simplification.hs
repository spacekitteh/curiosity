module Logic.Symbolic.SAT.Simplification where
import           Logic.Symbolic.SAT.Types
import           Logic.Symbolic.SAT.Representations.Types
import           Logic.Symbolic.SAT.Representations.Expression
import           Logic.Symbolic.SAT.Representations.CNF
import           Data.Foldable                 as DF
import           Data.Witherable               as DW
import qualified Data.HashSet                  as HS
import           Data.Hashable
import           Data.Either
import           Control.Effects.Commentary
import           Control.Effects.Substitution
import           Control.Lens
import           Polysemy
import           Control.Monad
import           Control.Applicative
import           Data.List                      ( partition )
import           Data.Data
import           Debug.Trace
import           Logic.Symbolic.SAT.Representations.ConversionRules

{-# INLINABLE oneLiteralRule #-}
oneLiteralRule
    :: (Eq v, Hashable v, StandardEffects r, v ~ Variable)
    => CNF v
    -> Sem r (Maybe (CNF v))
oneLiteralRule (CNF cnf) = if HS.null singles
    then return Nothing
    else do
        traverse addSubstitutions definitions

        let result = (cnf `HS.difference` singles)
        return (Just (CNF result))
  where
    singles     = HS.filter (\c -> 1 == clauseSize c) cnf
    singlesList = HS.toList singles
    definitions = fmap (mapClause literalAtom) singlesList
    literalAtom :: forall m a . Literal m a -> (a, Expr a)
    literalAtom (PositiveLiteral (Variable a)) = (a, Top)
    literalAtom (NegativeLiteral (Variable a)) = (a, Bottom)

{-#INLINABLE positiveNegativeRule #-}
positiveNegativeRule :: (Eq v, Hashable v, StandardEffects r, v ~ Variable)
    => CNF v
    -> Sem r (Maybe (CNF v))
positiveNegativeRule cnf = do
    let posis = positiveLiterals cnf
        negis = negativeLiterals cnf
        onlyPosis = posis `HS.difference` negis
        onlyNegis = negis `HS.difference` posis
    if (HS.null onlyPosis && HS.null onlyNegis) then return Nothing
    else do
        addSubstitutions
            ((fmap (\(Variable v) -> (v, Top)) $ HS.toList onlyPosis)
             ++ (fmap (\(Variable v) -> (v, Bottom)) $ HS.toList onlyNegis))
        return $ Just (removeClausesWith (onlyPosis `HS.union` onlyNegis) cnf)


{-#INLINE trivialCNFSimplifications #-}
trivialCNFSimplifications :: (Eq v, Hashable v, StandardEffects r, v ~ Variable) => CNF v -> Sem r (Maybe (CNF v))
trivialCNFSimplifications = combineRewrites [oneLiteralRule, positiveNegativeRule]


{-# INLINE trivialSimplifications #-}
trivialSimplifications :: StandardEffects r => Prop -> Sem r (Prop)
trivialSimplifications =
    rewriteM (combineRewrites [trivialSimplificationRules, varLookupRule])

{-# INLINE varLookupRule #-}
varLookupRule :: StandardEffects r => Prop -> Sem r (Maybe Prop)
varLookupRule prop = case prop of
    Var x -> lookupVariable x
    _     -> return Nothing
