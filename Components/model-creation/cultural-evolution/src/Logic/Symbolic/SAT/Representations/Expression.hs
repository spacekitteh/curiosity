module Logic.Symbolic.SAT.Representations.Expression where
import Control.Lens
import Data.Hashable
import Data.Data
import GHC.Generics
import qualified Data.HashSet as HS
import Logic.Symbolic.SAT.Representations.Types
import Polysemy
import Control.Effects.Substitution
import Polysemy.Error
import qualified Data.HashMap.Strict as HM
import Polysemy.Fresh
import Data.List
import Numeric.Natural

data Sort = Boolean
data Predicate v = Predicate v [Atom v]


varifyExpr :: Member (Fresh Variable) r => Expr String -> Sem r (Prop)
varifyExpr term = do
    let vars = term ^.. variables
    map <- stringVarsToVariables vars
    return (fmap (map HM.!) term)

{-data Expr [exprSize] v =
      Atomic (Atom v)
    | Top
    | Bottom
    | And [Expr v]
    | Or [Expr v]
    | (:<=>:) (Expr v) (Expr v)
    | (:=>:) (Expr v) (Expr v)
    | Not (Expr v)
    @-}
{-@autosize Expr@-}
data Expr v = Atomic (Atom v)
    | Top
    | Bottom
    | And [Expr v]
    | Or [Expr v]
    | (:=>:) (Expr v) (Expr v)
    | Not (Expr v)
    | (:<=>:) (Expr v)  (Expr v)
    deriving stock (Eq, Show, Read, Data, Typeable, Generic, Functor, Foldable, Traversable)
{-instance Show v => Show (Expr v) where
    show Top = "⊤"
    show Bottom = "⊥"
    show (And as) = "⋀(" ++ (intercalate ", " (fmap show as)) ++ ")"
    show (Or os) = "⋁(" ++ (intercalate ", " (fmap show os)) ++ ")"
    show (a :=>: b) = (show a) ++ "→" ++ (show b)
    show (Not a) = '¬': show a
    show (a :<=>: b) = '(':(show a) ++ "↔" ++ (show b) ++ ")"
    show (Atomic a) = show a-}

{- measure lsum :: [Nat] -> Nat}
lsum :: [Int] -> Int
lsum [] = 0
lsum (a:as) = a + (lsum as)

- measure exprSize :: a:(Expr v) -> Nat  }
exprSize :: Expr v -> Int
exprSize Top = 1
exprSize Bottom = 1
exprSize (And as) = 1 + lsum (fmap exprSize as)
exprSize (Or os) = 1 + lsum (fmap exprSize os)
exprSize (Not v) = 1 + exprSize v
exprSize (a :=>: b) = 1 + exprSize a + exprSize b
exprSize (a :<=>: b) = 1 + exprSize a + exprSize b
exprSize (Atomic _) = 1

-}
instance (Hashable v) => Hashable (Expr v)
instance (Data v) => Plated (Expr v)

pattern Var :: forall v. v -> Expr v
pattern Var v = Atomic (Variable v)

{-@toAtom :: n:Expr v -> {x : Atom v | isAtomic n} @-}
toAtom :: Expr v -> Atom v
toAtom (Atomic atom         ) = atom
toAtom (Not    (Atomic atom)) = atom
toAtom a                      = toAtom a


{-@ autosize [] @-}
{-# INLINE variables #-}
-- FIXME This should be called "atoms" or something
variables :: (Eq a, Hashable a) => Fold (Expr a) a
variables = folding (foldr HS.insert HS.empty)

{-#INLINE negateLiterals #-}
negateLiterals :: Expr v -> Expr v
negateLiterals = mapLiterals Not

{-#INLINABLE mapLiterals #-}
{-@ lazy mapLiterals @-}
{-@ mapLiterals :: f:(Expr v -> Expr v) -> a:(Expr v) -> Expr v / [autolen a] @-}
mapLiterals :: (Expr v -> Expr v) -> Expr v -> Expr v
mapLiterals f a@(Atomic _) = f a
mapLiterals f a@(Not(Atomic _)) = f a
mapLiterals f (And as) = And (fmap (mapLiterals f) as)
mapLiterals f (Or as) = Or (fmap (mapLiterals f) as)
mapLiterals f (Not p) = Not (mapLiterals f p)
mapLiterals f (a :=>: b) = (mapLiterals f a) :=>: (mapLiterals f b)
mapLiterals f (a :<=>: b) = (mapLiterals f a) :<=>: (mapLiterals f b)
mapLiterals _ Top = Top
mapLiterals _ Bottom = Bottom
type Prop = Expr Variable
type Valuation = Substitution Variable Prop

{-@ measure isNegativeLiteral :: Expr v -> Bool @-}

{-# INLINE isNegativeLiteral #-}
isNegativeLiteral :: Expr v -> Bool
isNegativeLiteral (Not (Atomic (Variable _))) = True
isNegativeLiteral _                = False

{-@ measure isAtomic @-}

{-#INLINE isAtomic #-}
isAtomic :: Expr v -> Bool
isAtomic (Atomic _) = True
isAtomic _          = False
{-#INLINE isLiteral #-}
{-@ measure isLiteral :: Expr v -> Bool @-}

isLiteral :: Expr v -> Bool
isLiteral (Atomic _) = True
isLiteral (Not (Atomic _)) = True
isLiteral _ = False
{-@ measure isPositiveLiteral :: Expr v -> Bool @-}
{-# INLINE isPositiveLiteral #-}
isPositiveLiteral :: Expr v -> Bool
isPositiveLiteral (Atomic (Variable _)) = True
isPositiveLiteral _ = False

{-@ measure isOr @-}
{-# INLINE isOr #-}
isOr :: Expr v -> Bool
isOr (Or _) = True
isOr _ = False
{-@ measure isAnd @-}
{-# INLINE isAnd #-}
isAnd :: Expr v -> Bool
isAnd (And _) = True
isAnd _ = False
{-#INLINE (/\) #-}
infixl 5 /\
(/\) :: Expr v -> Expr v -> Expr v
a       /\ (And b) = And (a : b)
(And a) /\ b       = And (b : a)
a       /\ b       = And [a, b]
{-# INLINE (\/) #-}
infixl 4 \/
(\/) :: Expr v -> Expr v -> Expr v
a      \/ (Or b) = Or (a : b)
(Or a) \/ b      = Or (b : a)
a      \/ b      = Or [a, b]

{-# INLINE lookupVar #-}
lookupVar
    :: Members '[Valuation, Error UnboundVariable] r => Variable -> Sem r Prop
lookupVar a = do
    result <- lookupVariable a
    case result of
        Just x  -> return x
        Nothing -> throw (UnboundVariable a)
{-# INLINE applySubstitution #-}
applySubstitution :: Members '[Valuation] r => Prop -> Sem r Prop
applySubstitution t = flip rewriteM t $ \case
    Var x -> lookupVariable x
    _     -> return Nothing





{-# INLINE positiveLiteral #-}
positiveLiteral :: Expr v -> Maybe (Literal Positive v)
positiveLiteral (Atomic v) = Just (PositiveLiteral v)
positiveLiteral _ = Nothing

{-#INLINE negativeLiteral #-}
negativeLiteral :: Expr v -> Maybe (Literal Negative v)
negativeLiteral (Not (Atomic v)) = Just (NegativeLiteral v)
negativeLiteral _ = Nothing


