module Logic.Symbolic.SAT.Representations.CNF where
import           Logic.Symbolic.SAT.Representations.Expression
import           Data.Hashable
import qualified Data.HashSet                  as HS
import           Control.Lens
import           Data.Data
import           Data.Foldable                 as DF
import           GHC.Generics
import           Logic.Symbolic.SAT.Representations.Types
import           Data.List                      ( partition
                                                , intercalate
                                                )
import           Polysemy
import           Control.Effects.Commentary
import           Logic.Symbolic.SAT.Representations.ConversionRules
import           Data.Maybe
import           Debug.Trace
import Data.HashSet.Lens
import GHC.Stack
data Clause v = MkClause (HS.HashSet (Literal Positive v)) (HS.HashSet (Literal Negative v))
    deriving stock (Eq,
         Typeable, Generic)
{-#INLINABLE clauseContains #-}
clauseContains :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> Clause v -> Bool
clauseContains vars (MkClause posis negis) = not $ HS.null $ (HS.union (HS.map rawLiterals posis) (HS.map rawLiterals negis)) `HS.intersection` vars

instance (Show v) => Show (Clause v) where
    show (MkClause posis negis) = intercalate " ∨ " combined
      where
        combined =
            (fmap show $ HS.toList posis) ++ (fmap show $ HS.toList negis)
emptyClause :: Clause v
emptyClause = MkClause HS.empty HS.empty
{-# INLINABLE clauseSize #-}
clauseSize :: Clause v -> Int
clauseSize (MkClause a b) = (HS.size a) + (HS.size b)
{-# INLINABLE clauseAtoms #-}
clauseAtoms :: (Eq v, Hashable v) => Fold (Clause v) v
clauseAtoms = (Control.Lens.to fromClause) . variables
instance (Hashable v) => Hashable (Clause v)

{-# INLINABLE makeClause #-}
makeClause :: (Eq v, Hashable v) => Expr v -> Maybe (Clause v)
makeClause (Or as) | DF.all isLiteral as = Just (MkClause pos neg)  where
    (p, n) = partition isPositiveLiteral as
    pos    = HS.fromList (fmap (PositiveLiteral . toAtom) p)
    neg    = HS.fromList (fmap (NegativeLiteral . toAtom) n)

makeClause a | isPositiveLiteral a =
    Just (MkClause (HS.singleton (fromJust . positiveLiteral $ a)) HS.empty)
makeClause a | isNegativeLiteral a =
    Just (MkClause HS.empty (HS.singleton (fromJust . negativeLiteral $ a)))
makeClause _ = Nothing

{-# INLINABLE fromClause #-}
fromClause :: Clause v -> Expr v
fromClause (MkClause p n) = Or (pos ++ neg)  where
    pos = fmap toAtomic (HS.toList p)
    neg = fmap toAtomic (HS.toList n)

{-# INLINABLE _AClause #-}
_AClause :: (Eq v, Hashable v) => Prism' (Expr v) (Clause v)
_AClause = prism' fromClause makeClause
{-# INLINE toAtomic #-}
toAtomic :: Literal m v -> Expr v
toAtomic (PositiveLiteral a) = Atomic a
toAtomic (NegativeLiteral a) = Not (Atomic a)

{-# INLINABLE mapClause #-}
mapClause :: (forall m . Literal m v -> a) -> Clause v -> [a]
mapClause f (MkClause p n) = (fmap f (HS.toList p)) ++ (fmap f (HS.toList n))
newtype CNF v = CNF (HS.HashSet (Clause v)) deriving (Eq, Show, Typeable, Generic)
instance Data v => Data (CNF v)
instance Data v => Plated (CNF v)
{-# INLINE cnfToExpr #-}
cnfToExpr :: (Eq v, Hashable v, Data v) => CNF v -> Expr v
cnfToExpr (CNF c) | c == HS.empty = Bottom
cnfToExpr (CNF cnf) =
    run $ ignoreCommentary $ rewriteM trivialSimplificationRules $ And $ fmap
        fromClause
        (HS.toList cnf)

{-# INLINE exprToCNF #-}
exprToCNF :: (HasCallStack, Eq v, Hashable v, Data v, Show v) => Expr v -> CNF v
exprToCNF prop =
    run $ ignoreCommentary $ do
    cnfd <- toCNF prop
    case cnfd of
        And as -> return $ CNF $ HS.fromList $ fmap (fromJust . makeClause) as
        Or os | DF.null os -> return (CNF HS.empty)
        Or  os -> return $ CNF $ HS.singleton $ fromJust . makeClause $ cnfd
        a | isLiteral a ->
            return $ CNF $ HS.singleton $ fromJust . makeClause $ Or [cnfd]
        Top    -> return $ CNF HS.empty
        Bottom -> return $ CNF $ HS.singleton emptyClause
        _      -> error (show cnfd)

{-#INLINE cnf #-}
cnf :: (Eq v, Hashable v, Data v, Show v) => Iso' (Expr v) (CNF v)
cnf = iso exprToCNF cnfToExpr

{-#INLINABLE positiveLiterals #-}
positiveLiterals ::(Eq v, Hashable v) => CNF v -> HS.HashSet (Atom v)
positiveLiterals (CNF clauses) = foldr HS.union HS.empty $ fmap posis (clauses ^.. folded) where
    posis (MkClause p _) = HS.map rawLiterals p

{-#INLINABLE negativeLiterals #-}
negativeLiterals ::(Eq v, Hashable v) => CNF v -> HS.HashSet (Atom v)
negativeLiterals (CNF clauses) = foldr HS.union HS.empty $ fmap negis (clauses ^.. folded) where
    negis (MkClause _ n) = HS.map rawLiterals n

{-#INLINABLE removeClausesWith #-}
removeClausesWith :: (Eq v, Hashable v) => HS.HashSet (Atom v) -> CNF v -> CNF v
removeClausesWith h (CNF a) = CNF (HS.filter (not . (clauseContains h)) a)