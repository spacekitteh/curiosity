{-@LIQUID "--no-totality" @-}
module Logic.Symbolic.SAT.Representations.Types where

import           Data.Unique
import           Polysemy
import           Polysemy.Fresh
import qualified Data.HashMap.Strict           as HM
import           Data.Hashable
import           Data.Data
import           GHC.Generics
import           Data.Bits                      ( xor )
import Control.Lens.Plated
data Variable = AVariable (Maybe String) Unique
    deriving (Ord, Data, Typeable, Generic)
instance Eq Variable where
    (AVariable _ a) == (AVariable _ b) = a == b
instance Hashable Variable where
    hash (AVariable _ a) = hashUnique a
    hashWithSalt i (AVariable _ a) = i `xor` (hashUnique a)
{-@autosize Atom@-}
data Atom v = Variable v
            | Function v -- todo sort this out. it will contain multiple v's, but they can't be split up
    deriving stock (Eq, Read, Data, Typeable, Generic, Functor, Foldable, Traversable)
instance Show v => Show (Atom v) where
    show (Variable v) = show v
instance Hashable v => Hashable (Atom v)
instance Data v => Plated (Atom v)


data Sense = Positive | Negative deriving (Eq, Generic, Data, Typeable)

data Literal (m :: Sense) v where
    PositiveLiteral ::Atom v -> Literal Positive v
    NegativeLiteral ::Atom v -> Literal Negative v

{-@
measure isPositiveLiteral' :: Literal Positive v -> Bool
isPositiveLiteral' (PositiveLiteral a) = True
@-}
{-@
measure isNegativeLiteral' :: Literal Negative v -> Bool
isNegativeLiteral' (NegativeLiteral a) = True
@-}
deriving stock instance (Eq (Atom v)) => Eq (Literal m v)
instance Show (Atom v) => Show (Literal m v) where
    show (PositiveLiteral a) = show a
    show (NegativeLiteral a) = '¬': show a
deriving stock instance Functor (Literal m)
deriving stock instance Foldable (Literal m)
deriving stock instance Traversable (Literal m)
deriving stock instance Typeable (Literal m v)
{-#INLINE rawLiterals #-}
rawLiterals :: Literal m v -> Atom v
rawLiterals (PositiveLiteral v) = v
rawLiterals (NegativeLiteral v) = v



instance Hashable (Atom v) => Hashable (Literal Positive v) where
    {- instance Hashable (Atom v) => Hashable (Literal Positive v) where
        hashWithSalt :: Int -> a:{x:(Literal Positive v) | isPositiveLiteral' x}
    -> Int @-}
    {-@ignore hashWithSalt @-}
    hashWithSalt :: Int -> Literal Positive v -> Int
    hashWithSalt i (PositiveLiteral a) = hashWithSalt @(Atom v) i a


instance Hashable v => Hashable (Literal Negative v) where
    {- instance Hashable (Atom v) => Hashable (Literal Negative v) where
            hashWithSalt :: Int ->  a:{x:(Literal Negative v) |
            isNegativeLiteral' x} -> Int @-}
    {-@lazy hashWithSalt @-}
    hashWithSalt i (NegativeLiteral a) = (hashWithSalt @(Atom v)) i a



instance Data Unique where
    toConstr = toConstr
    dataTypeOf _ = mkNoRepType "Data.Unique.Unique"
    gunfold = gunfold

instance Show Variable where
    show (AVariable (Just s) _) = s
    show _                      = "An anonymous coward"
{-# INLINE stringVarsToVariables #-}
stringVarsToVariables
    :: (Member (Fresh Variable) r)
    => [String]
    -> Sem r (HM.HashMap String Variable)
stringVarsToVariables strings = do
    assignments <- traverse stringToFresh strings
    return (HM.fromList assignments)
  where
    stringToFresh
        :: (Member (Fresh Variable) r) => String -> Sem r (String, Variable)
    stringToFresh str = do
        (AVariable _ var) <- fresh
        return (str, AVariable (Just str) var)
{-# INLINE runFreshVariable #-}
runFreshVariable :: Sem ((Fresh Variable) : r) a -> Sem ((Fresh Unique) : r) a
runFreshVariable = reinterpret $ \case
    Fresh -> do
        v <- fresh @Unique
        return (AVariable Nothing v)
newtype UnboundVariable = UnboundVariable Variable deriving (Eq, Show)
