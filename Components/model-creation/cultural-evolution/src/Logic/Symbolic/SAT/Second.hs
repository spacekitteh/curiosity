module Logic.Symbolic.SAT.Second where
import           Polysemy
import           Polysemy.State
import           Control.Effects.Commentary
import           Control.Effects.Progress
import           Control.Effects.WorkingMemory
import           Control.Effects.Configuration
import           Control.Effects.Log
import           Control.Effects.Substitution
import qualified Data.HashMap.Strict           as HM
import qualified Data.HashSet                  as HS
import           Polysemy.Fresh
import           Polysemy.Error
import           Data.Unique
import           Data.Data
import           GHC.Generics
import           Control.Lens
import           Data.Hashable
import           Data.Traversable
import Control.Effects.HypothesisTesting
import Data.Maybe
import           Polysemy.NonDet
import           Control.Lens.Plated
import           Polysemy.Async
import           Data.Monoid
import           Debug.Trace
import           GHC.Types                      ( SPEC(..) )
import           Data.Foldable                 as DF
import           Data.Either
import           Data.Witherable               as DW
import           Control.Applicative
import           Logic.Symbolic.SAT.Representations.Expression
import           Logic.Symbolic.SAT.Representations.CNF
import           Logic.Symbolic.SAT.Representations.Types
import           Logic.Symbolic.SAT.Types
import           Logic.Symbolic.SAT.Simplification
import           Logic.Symbolic.SAT.Representations.ConversionRules
import Control.Effects.CaseSplitting

type SATEffects r = (StandardEffects r, Member SAT r)



{-# INLINE evaluate #-}
evaluate
    :: (StandardEffects r, Member (Error UnboundVariable) r)
    => Prop
    -> Sem r Bool
evaluate prop = do
    comment "We proceed by simple induction."
    case prop of
        Top    -> pure True
        Bottom -> pure False
        Var v  -> do
            term <- lookupVar v
            evaluate term
        And as -> do
            a' <- traverse evaluate as
            return (DF.and a')
        Or as -> do
            a' <- traverse evaluate as
            return (DF.or a')
        Not a -> do
            a' <- evaluate a
            return (not a')
        a :<=>: b -> do
            a' <- evaluate a
            b' <- evaluate b
            return (a' == b')
        a :=>: b -> do
            assumption "Classical logic"
            a' <- evaluate a
            b' <- evaluate b
            return ((not a') || b')

{-# INLINE trivial #-}
trivial :: StandardEffects r => Prop -> Sem r (Maybe Bool)
trivial prop = do
    p <- trivialSimplifications prop
    case p of
        Top    -> return (Just True)
        Bottom -> return (Just False)
        _      -> return Nothing


{-ideas:
1. complexity metrics
2. NENF
3. XOR
4. DNF
5. rewriting monad
6. FOL
7. unsat core -}

{-# INLINABLE satToMaybe #-}
satToMaybe :: Maybe SATResult -> Maybe (HM.HashMap Variable Prop)
satToMaybe (Just (Satisfiable p)) = Just p
satToMaybe _                      = Nothing


data SAT m a where
    AddTerms ::Traversable t => t Prop -> SAT m ()
    Rollback ::SAT m ()
    Solve ::SAT m SATResult

makeSem ''SAT

{-# INLINE generateAssignments #-}
generateAssignments :: [Variable] -> [[(Variable, Prop)]]
generateAssignments [] = []
generateAssignments l  = go SPEC [[]] l  where
    go _     acc []       = acc
    go !sPEC acc (a : as) = do
        value <- [Top, Bottom]
        map ((a, value) :) (go sPEC acc as)


{-# INLINABLE trivialSAT #-}
trivialSAT
    :: (StandardEffects r, Members '[Async, CaseSplitting Variable Prop] r)
    => Prop
    -> Sem r SATResult
trivialSAT prop = do
    case prop of
        Top -> do
            subs <- substitutions
            --traceM $ "subs: " ++ (show subs)
            return (Satisfiable (HM.fromList subs))
        Bottom -> do
                --subs <- definedSubstitutions
                --return (Unsatisfiable (UnsatCore subs))
                hypos <- allHypotheses
                subs <- definedSubstitutions
                let allHypos = foldr HS.union subs (concat hypos)
                -- FIXME i need to either use a proper trailmix effect, and
                -- FIXME remove the derived definitions here, and roll back.
                return (Unsatisfiable (UnsatCore allHypos))
        _      -> do

            --traceM "before:"
            p' <- trivialSimplifications prop
            --traceM (show p')
            --traceM "after:"
            --let p = p'
            p <- rewriteMOn cnf trivialCNFSimplifications p'
            --traceM (show p)
            --traceM "-------"
            vars <- do
                explanation "Here is where we choose what variable(s) to attack next. This is where much research is focused in SAT/SMT; finding performant heuristics for selecting variables."
                chooseCases p
            let varsSet = HS.fromList vars
            addHypotheses [(varsSet)]
            let assignments = generateAssignments vars
            spawn <- for assignments $ \assignment -> async $ do
                addSubstitutions assignment
                subbed <- applySubstitution p
                trivialSAT subbed
            results <- traverse await spawn
            let possibleResult =
                    getFirst $ foldMap (First . satToMaybe) results
            case possibleResult of
                Just result -> return (Satisfiable result)
                _ | all (\case
                        Just (Unsatisfiable _) -> True
                        _ -> False) results ->
                    do
                        let cores = foldr HS.union varsSet $ fmap (fromJust . getUnsatCore . fromJust) results
                        hypos <- allHypotheses
                        subs <- definedSubstitutions

                        let allHypos = foldr HS.union subs (concat hypos)
                        return (Unsatisfiable (UnsatCore allHypos))
                _ -> return GaveUp



isTautology
    :: (StandardEffects r, Members '[Async, Error TooComplicatedForTautology, CaseSplitting Variable Prop] r)
    => Prop
    -> Sem r Bool
isTautology prop = do
    let negated = Not prop
    result <- trivialSAT negated
    case result of
        Satisfiable _ -> return False
        Unsatisfiable _ -> return True
        GaveUp        -> throw TooComplicatedForTautology



pickFirstVar
    :: (Eq var, Hashable var)
    => Sem ((CaseSplitting var (Expr var)):r) a
    -> Sem r a
pickFirstVar = interpret $ \case
    ChooseCases term -> do
        let vars = term ^.. variables
        case vars of
            [] -> return []
            a:_ -> return [a]
pickVarFromSmallestSubTerm :: (Eq var, Hashable var)
    => Sem ((CaseSplitting var (Expr var)):r) a
    -> Sem r a
pickVarFromSmallestSubTerm = interpret $ \case
    ChooseCases (Or os) -> return (smallest os)
    ChooseCases (And as) -> return (smallest as)
    ChooseCases (a :<=>: b) -> return (smallest [a,b])
    ChooseCases (a :=>: b) -> return (smallest [a,b])
    ChooseCases a -> do
        let vars = a ^.. variables
        case vars of
            [] -> return []
            a:_ -> return [a]
    where
        smallest list = toListOf variables $ minimumBy (\a b -> compare (a^..variables & length) (b^..variables & length)) list
{-# INLINE interpretSAT #-}
interpretSAT
    :: (Members '[State Prop, CaseSplitting Variable Prop] r, StandardEffects r)
    => Sem (SAT : r) a
    -> Sem r a
interpretSAT = interpret $ \case
    AddTerms newTerms -> do
        simplified <- mapM trivialSimplifications newTerms
        existing   <- get
        newTerm    <- trivialSimplifications
            (existing /\ And (DF.toList simplified))
        put newTerm
    Solve -> do
        terms <- get
        comment
            "For our first attempt, we will just see if it is naively satisfiable."
        trivialSAT terms
    Rollback -> error "Not yet implemented rollback"


{-# INLINE runSAT #-}
runSAT :: (StandardEffects r, Member (CaseSplitting Variable Prop) r) => Sem (SAT : State Prop : r) a -> Sem r a
runSAT s = (evalState Top) $ interpretSAT s

test' :: Prop -> IO (Either UnboundVariable SATResult)
test' a =
    runFinal
        $ embedToFinal
        $ errorToIOFinal
        $ freshToIO
        $ runFreshVariable
        $ ignoreCommentary
        $ asyncToIOFinal
        $ evalState HM.empty
        $ runSubstitutionsAsHashMapState
        $ evalState []
        $ runHypothesisTestingAsList
        $ pickVarFromSmallestSubTerm
        $ trivialSAT a

test :: Expr String -> IO (Either UnboundVariable SATResult)
test term =
    runFinal
        $ embedToFinal
        $ errorToIOFinal
        $ freshToIO
        $ runFreshVariable
        $ ignoreCommentary
        $ asyncToIOFinal
        $ evalState HM.empty
        $ runSubstitutionsAsHashMapState
        $ evalState []
        $ runHypothesisTestingAsList
        $ pickVarFromSmallestSubTerm
        $ do
              p    <- varifyExpr term
              trivialSAT p
{-Some tests-}
z' = Var "c" /\ Not (Var "a")
z = Var "c" /\ (Var "a" :=>: Bottom)
li 0 = z :<=>: z
li 1 = (Not (li 0)) :<=>: Top
li n = (li (n - 1)) :<=>: (Not (li (n - 2)))
l = li 15
compL = take 11 $ fmap (Var . show) [1 ..]
com = z /\ And [ a :<=>: (li 1 \/ b) | a <- compL, b <- compL ] /\ (Not (Var "1") :<=>: (Var "a"))

cuz = And [Or [Var "1",Var "a"],Or [Not (Var "a"),Not (Var "1")],Var "c",Not (Var "a"),Or [Not (Var "1"),Var "2"],Or [Not (Var "2"),Var "1"]]

fuckit = l /\ com

k = (And [Var "z", Or [Var "a", Var "b"], Or [Var "c", Var "d"]])
k1 = run $ ignoreCommentary $ distributiveLaws ToCNF k
k2 = run $ ignoreCommentary $ distributiveLaws ToCNF  (fromJust k1)
k3 = run $ ignoreCommentary $ distributiveLaws ToCNF (fromJust k2)
k4 = run $ ignoreCommentary $ distributiveLaws ToCNF (fromJust k3)
k5 = run $ ignoreCommentary $ distributiveLaws ToCNF (fromJust k4)
k6 = run $ ignoreCommentary $ distributiveLaws ToCNF (fromJust k5)

x = (Or [Var "z", And [Var "a", Var "b"], And [Var "c", Var "d"]])
x1 = run $ ignoreCommentary $ distributiveLaws ToCNF x
x2 = run $ ignoreCommentary $ distributiveLaws ToCNF (fromJust x1)
x3 = run $ ignoreCommentary $ distributiveLaws ToCNF (fromJust x2)
x4 = run $ ignoreCommentary $ distributiveLaws ToCNF (fromJust x3)
x5 = run $ ignoreCommentary $ distributiveLaws ToCNF (fromJust x4)
x6 = run $ ignoreCommentary $ distributiveLaws ToCNF (fromJust x5)

bjTest = And ((take 10 compL)  ++ [Or [Not (Var "1"), Not (Var "10"), Var "11"], Or [Not (Var "1"), Not (Var "10"), Not(Var "11") ]]) -- unsat should report 1, 10