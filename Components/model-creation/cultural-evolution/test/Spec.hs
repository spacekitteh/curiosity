module Main where

import           MaterialistDesign
import           Data.List
import           Data.Simulation.GeneticAlgorithms.Chromosomes
import           Data.Simulation.GeneticAlgorithms.Genome
import           Data.Simulation.GeneticAlgorithms.Individual
import           Data.Simulation.GeneticAlgorithms.Population
import           Data.Simulation.GeneticAlgorithms.Utils
import           Data.Simulation.GeneticAlgorithms.Types
import           Data.Foldable

import           Data.Vector
import           System.Random.MWC
import           Criterion.Main
import           Criterion.Types
import           Numeric.Natural
import           Control.Monad.Primitive
import           Data.Hashable
import           Control.DeepSeq
import           Data.Simulation.GeneticAlgorithms.Population
import           Control.Parallel
import           Control.Parallel.Strategies
import qualified Data.Vector                   as V

testfunc :: Double -> Double
testfunc !x = -8 * ((cos x) ** 2)

curve :: V.Vector Double
curve = fmap testfunc fitnessxs

myConfig = defaultConfig --{
              -- Resample 15 times for bootstrapping
--              resamples = 50
           --}

setupEnv cacheEvals = do
    (!popFiddy, gen) <- createPop @CompiledCGE 50 cacheEvals
    (!popFiveBenji, _) <- createPop @CompiledCGE 500 cacheEvals
    (popMilli@(APopulation _ _ _ !(b, _) _ _), _) <- createPop @CompiledCGE
        1000
        cacheEvals
    (!popTwoMilli, _) <- createPop @CompiledCGE 2000 cacheEvals
    (!popFiddyBenji, _) <- createPop @CompiledCGE 5000 cacheEvals
    critter <- (createIndividual @CompiledCGE defaultCritterParams gen)
    return
        ( gen
        , (withStrategy rdeepseq b) `pseq` popFiddy
        , popFiveBenji
        , popMilli
        , popTwoMilli
        , popFiddyBenji
        , critter
        )

createGroupWith :: Bool -> IO Benchmark
createGroupWith cacheEvals = do
    (gen, !popFiddy, !popFiveBenji, !popMilli, !popTwoMilli, !popFiddyBenji, !critter) <-
        setupEnv cacheEvals
    let cacheString = if cacheEvals then "cacheEnabled" else "cacheDisabled"
    return $ bgroup
        cacheString
        [ bgroup
            "mutate"
            [ bench "mutating one critter x1000"
                  $ whnfIO (mutateFor critter 1000 gen)
            ]
        , bgroup
            "repro"
            [ bgroup
                "shortRepro"
                [ bench "50critters reproducing x 10"
                    $ whnfAppIO (kRepros popFiddy gen) 10
                , bench "500critters reproducing x 10"
                    $ whnfAppIO (kRepros popFiveBenji gen) 10
                , bench "1000critters reproducing x 100"
                    $ whnfAppIO (kRepros popFiveBenji gen) 100
                , bench "5000critters reproducing x 20"
                    $ whnfAppIO (kRepros popFiddyBenji gen) 20
                ]
            , bgroup
                "longRepro"
                [ bgroup
                      "1,000,000"
                      [ bench "500critters reproducing x 2000"
                          $ whnfAppIO (kRepros popFiveBenji gen) 2000
                      , bench "1000critters reproducing x 1000"
                          $ whnfAppIO (kRepros popMilli gen) 1000
                      , bench "2000critters reproducing x 500"
                          $ whnfAppIO (kRepros popTwoMilli gen) 500
                      , bench "5000critters reproducing x 200"
                          $ whnfAppIO (kRepros popFiddyBenji gen) 200
                      ]
                ]
            ]
        ]

main = do
        nocache <- createGroupWith False
        withcache <- createGroupWith True
        defaultMainWith myConfig [nocache, withcache]

mutateFor :: (HasChromosomes a) => a -> Int -> Gen (PrimState IO) -> IO ()
mutateFor critter iters gen = do
--    print "Starting mutation iterations~"
    iterateM
        iters
        ( flip
                (mutateGenome
                    (ImperfectCopying 0.15)
                    (ASingleton (PointErrors (Run 0 1000), MutateGenes))
                )
        $ gen
        )
        critter
    return ()
--    print "Finishing mutation iterations~"

kRepros
    :: ( NFData a
       , IsIndividual a
       , Score a ~ Double
       , Hashable (PhenotypeType a)
       , Eq (PhenotypeType a)
       )
    => Population IO a
    -> Gen (PrimState IO)
    -> Int
    -> IO (Double, a)
kRepros pop gen k = do
--    print $ take 2 $ sortBy (flip compare) $ toList $ fmap fst $ evaluate
--        EvaluationParameters
--        pop
    pop' <- reproduceFor k (ReproductionParameters) gen pop
    return (_best pop')
--    print $ take 20 $ sortBy (flip compare) $ toList $ fmap fst $ evaluate
--        EvaluationParameters
--        newpop

createPop
    :: forall a
     . ( NFData a
       , IsIndividual a
       , Score a ~ Double
       , Hashable (PhenotypeType a)
       , Eq (PhenotypeType a)
       , Environment a ~ V.Vector Double
       )
    => Natural
    -> Bool
    -> IO (Population IO a, Gen (PrimState IO))
createPop n cacheEvals = do
    gen <- createSystemRandom
    pop <- spawnNewPopulation @(Population IO a)
        (PopulationParameters n cacheEvals)
        curve
        gen
    return (pop, gen)

{-nCrittersKRepros :: Natural -> Int -> IO (Double, CompiledCGE)
nCrittersKRepros n k = do
    (pop, gen) <- createPop @CompiledCGE n
    kRepros pop gen k
    (pop, gen) <- createPop @CompiledCGE n
    kRepros pop gen k
-}