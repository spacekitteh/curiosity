\section{The dialectics of compression algorithms}
Adaptive compression faces several inter-related complex trade-offs (in philosophical terms, this may be phrased as a \emph{\gls{contradiction}} \autocite{zedong_contradiction_1937}). Time and space complexity in the compression stage does not follow a simple relationship with time and space complexity during the decompression stage. Generally, however, increased processing can allow for more advanced compression, leading to smaller time and space requirements for handling compressed data, such as for transmission or storage. Another fundamental trade-off is the complexity of models versus the generality of the algorithm; more complex, specialised models generally restrict the scope to specific data domains, but often result in much better compression due to the usage of domain knowledge.

One common method of increasing the generality of a tool which uses domain knowledge is via mixture models \autocite{steinruecken_lossless_2014,mahoney_adaptive_2005}. These apply multiple different, distinct, models and synthesise a single prediction from the combined predictions of each. This approach, however, typically results in a much higher time and space complexity during compression and decompression, as not only does each model have to be processed, but so does the extra stage of combining predictions.

These fundamental contradictions result in an enormous variety of approaches and techniques used to generate models for entropy coding. Techniques range from simple histograms and \glspl{ngram}, to complex mixture models featuring hundreds of sub-models which are mixed with neural nets trained online. A large fraction of these models have something in common: They are created on an \emph{ad hoc} basis, without regard to a formal measure of model complexity. Instead, they typically only focus on compression ratio, which is, of course, usually the primary concern of compression.

But what if we are interested in more than just the data itself? In our context of an autonomous scientific agent, the data is just a means to an end: model discovery. We humans are only interested in the data for verifying, testing and replicating the model --- we do not care specifically about data points; rather, we are interested in the \emph{structure} of the data.

We are interested in both the model \emph{and} the data; as such, we wish to both transmit and receive the model and the data in a single message. Here is where the principal contradiction of adaptive compression is most succinctly stated, and most amenable to be attacked via information-theoretic methods. It also the most useful, as it gives us a specific criterion to optimise.

\subsection{Minimum Message Length: Overcoming the fundamental contradiction in compression}
Adaptive models generally pay for increased accuracy with increased complexity. All things being equal, given two equivalent models, we usually prefer the simpler model. Simpler models are easier to understand, usually have smaller time and space requirements, and are more likely to be correct (i.e. Occam's Razor). Complex models require more detailed descriptions; in other words, more data to transmit.

The trade-off between accurate models and better compression can be quantised in a simple equation when our message is composed of two parts: The model, or hypothesis, followed by the data, or observations. The definition of conditional dependence shows that the probability of the hypothesis, plus the observations given the hypothesis, is equivalent to the probability of the observations and hypothesis together. By Shannon's theorem, we get the entropy of the message, and therefore the length of the message after entropy coding.
\begin{figure}
    \centering
\begin{tikzpicture}
    \begin{axis}[
        ybar stacked,
        bar width=15pt,
        legend style={anchor=north,legend columns=-1},
        ylabel={Total message length (\si{\mega\byte})},
        symbolic x coords={Simple, Complex, MML},
        xtick=data,
        x tick label style={rotate=45,anchor=east}
        ]
    \addplot+[ybar] plot coordinates {(Simple,2) (Complex,11)
      (MML,6)};
    \addplot+[ybar] plot coordinates {(Simple,15) (Complex,3)
      (MML,5) };
    \legend{\strut Model length, \strut Data length}
    \end{axis}
    \end{tikzpicture}
    \caption{An illustrative example of the minimum message-length principle}
    \label{fig:mmlPrinciple}
\end{figure}
Therefore, our dialectic is as follows: The thesis of the hypothesis, and the anti-thesis of the observations (interpreted according to the hypothesis) results in the synthesis of the total message. This synthesis is quantifiable, and gives an objective measure of the naturalness of the the hypothesis. More accurate yet more complex models will result in a smaller compressed length of observations, but will require more information to encode the hypothesis; this naturally penalises a common problem in model estimation, overfitting.  Simpler models will require less information to encode the hypothesis, but more to encode the observations; this naturally penalises poorly fitting models. We can see an example in \autoref{fig:mmlPrinciple}. %TODO Explain why they are thesis/antithesis

We thus have a natural function to optimise during model creation: message length. Optimising the model to find the \gls{mml} can be approached through any number of optimisation schemes.
