\chapter{MMLThought\@: A new general-purpose lossless compressor}\label{chap:mml}
We introduce a new lossless general-purpose compression algorithm capable of adaptively compressing static and streaming data.
\subimport{.}{CompressionDialectics.tex}
\section{Compression contextualised}\label{sec:requirements}
Our context of a robotic cosmonaut leads to several constraints and assumptions.

\begin{itemize}
\item The purpose of our cosmonaut is to perform science. As such, its fundamental \emph{raison d'être} is model creation. Although this is typically done by human scientists, we wish to advance the usefulness of autonomous agents and artificial intelligence; not only to assist human scientists, but as a goal in-and-of itself.
\item Our agent performs experiments to collect \glslink{observation}{observational data}. Typically, these experiments are power-intensive, and require exclusive control of the agent's \gls{embodiment}. Therefore, we assume our agent can't actively perform multiple experiments at once.
\item The power requirements for performing experiments greatly outweigh the power requirements for pure computation. Computations can be performed while idling, using excess power from, for example, radioisotope thermal generators.
\item The agent is to send both its \gls{hypothesis} and \glspl{observation}.
\item Observational data is gathered in an online fashion. Processing of data can either occur as soon as it is generated, so as to minimise the storage requirements and worst-case execution time; or it can be done in batches, which may be more efficient by facilitating instruction \gls{pipelining}.
\item At any one time, we must have a model with which to compress our data against. This can be as simple as a uniform prior, resulting in no actual compression.
\item Models should be adapted by taking advantage of knowledge gained from previous \glspl{model}, rather than recreating a model \emph{ab initio}.
\item We should like to reason about our \glspl{model}, so as to gain insight about future experiments to perform in order to validate or refute them.
\item Likewise, \glspl{model} should be able to inform more mundane decisions where it makes sense; for example, studying friction should help our agent to decide if a sand dune is too slippery to risk traversing.
\item Communication with Earth is expensive in terms of power and time, and initialising communications is a task with an extraordinarily large latency, therefore we would prefer to send a single message containing both our \gls{hypothesis} and \glspl{observation}.
\item Likewise, we wish to minimise the message length of the combined \gls{hypothesis} and \glslink{observation}{observational data}, in order to minimise the effort involved in communication.
\item \fcolorbox{red}{white}{\begin{minipage}{\linewidth}
We would like to eliminate the redundant processing involved in model generation for science purposes, and model generation for compression purposes. \emph{For a large class of problems, the model we seek is identical in both circumstances.}\end{minipage}}
\end{itemize}
The last requirement in this list is the fundamental observation of this thesis.

\begin{sidewaysfigure}[p]
    \centering
    %\subimport{.}{RoboScienceSequence.puml}
    \includegraphics[width=0.9\linewidth]{RoboScienceSequence.eps}
    \caption{Sequence diagram for how our design may interact with humanity.}
    \label{fig:RoboScienceSequence}
\end{sidewaysfigure}
An example of how our robotic cosmonaut interacts with human scientists is shown in \Fref{fig:RoboScienceSequence}.


\section{Online evolution of probabilistic graphical models via genetic programming}\label{sec:gppgm}
The requirements outlined in \autoref{sec:requirements} immediately lead us to our approach of using \gls{gp} to evolve \glspl{pgm}.

\subsection{Genetic algorithms}
\begin{figure}
    \centering
        \subimport{.}{GAOverview.puml}

    \caption{Overview of genetic algorithms}
    \label{fig:GAOverview}
\end{figure}
\Glspl{ga} are a biologically-inspired class of \gls{optimisation} algorithms, depicted in \Fref{fig:GAOverview}. They are based around three fundamental operations: \gls{mutation}, \gls{reproduction}, and \gls{selection}. Potential problem solutions are represented as a \gls{population} of \glspl{individual}, each with unique \glspl{genotype} which specify solution parameters encoded in the form of \glspl{gene}. These are then \glslink{geneexpression}{\emph{expressed}} as a \gls{phenotype}.

After an initial population is spawned with (typically) randomised \glspl{genotype}, their \glspl{phenotype} are evaluated according to the \gls{fitnessfunc}, which assigns a score according to how well the \gls{phenotype} solves the problem. Following this, a subset of \glspl{individual} are selected for \gls{reproduction} according to a chosen \gls{reprostrat}, such as \gls{broodselection}, \gls{steadystaterepro} or \gls{elitism}. The selected \glspl{individual} are then used to create a new \gls{population}; one common method is to perform \gls{crossover} of the parental \glspl{chromosome}, followed by genetic \gls{mutation} of the \gls{progeny}. Typical \glspl{mutation} are \glspl{pointmutation}, which randomly change individual \glspl{nucleotide} with some small probability; and \gls{translocation}, or swapping, the movement of a stretch of \glspl{nucleotide} to a new location in the \gls{genotype}. These are illustrated in \Fref{fig:genops}. The process then repeats itself.

\begin{figure}
    \centering
    \subimport{.}{genops.tex}
    \caption{Basic genetic operations}
    \label{fig:genops}
\end{figure}

Although the initial \gls{generation} is usually a blind random search, succeeding \glspl{generation} use information gained from previous \glspl{generation}; this information is propagated via inheritance of \glspl{gene}. A crucial factor in overcoming local optima, however, is the maintenance of diversity in the \gls{genepool}; thus seeding the initial population randomly and subjecting each \gls{generation} to random \glspl{mutation} is crucial.


\subsection{Genetic programming}
\Gls{gp} is a technique which can be thought of as a combination of program \gls{induction} and \gls{optimisation}: Programs are synthesised which try to meet a specific criteria, which may only be indirectly expressible as input-output pairs. Our prototype implementation uses Cartesian genetic programming, a form of genetic programming which interprets its genes as nodes in a directed graph on a plane; each node has (typically integer) coordinates, and each instruction sub-gene refers to other nodes via their coordinates. This is demonstrated in \Fref{fig:cge}.
\begin{figure}
    \centering
    \begin{subfigure}{0.7\textwidth}\centering
        \begin{tikzpicture}[->]
            \tikzstyle{every path}=[semithick]
    \tikzstyle{cgenode}=[draw,minimum size=0.7cm]

    %% Draw TM tape
    \begin{scope}[start chain=1 going right,node distance=-0.15mm]
        \node [on chain=1,cgenode,draw=none] {$\ldots$};
        \node [on chain=1,cgenode] {};
        \node [on chain=1,cgenode] (a) {$+$};
        \node [on chain=1,cgenode] (b) {$\sin$};
        \node [on chain=1,cgenode] (c) {$\times$};
        \node [on chain=1,cgenode] (d) {$\pi$};
        \node [on chain=1,cgenode] (e){input 4};
        \node [on chain=1,cgenode] {1.334};
        \node [on chain=1,cgenode] {};
        \node [on chain=1,cgenode,draw=none] (f) {$\ldots$};
        \node [on chain=1] {\textbf{Chromosome}};

    \end{scope}
        \path (a.south) edge [bend right](d.south);
        \path (a.north) edge [bend left] (b.north);
        \path (b.south) edge [bend right] (e.south);
        \path (c.south) edge [bend right] (f.south);
        \path (c.north) edge [bend left] (f.north);
\end{tikzpicture}
\caption{An example portion of a possible genotype found in Cartesian genetic programming.}\label{fig:cgeex1}
    \end{subfigure}
    \begin{subfigure}{0.7\textwidth}\centering
        \begin{tikzpicture}[->, shorten >=1pt,auto,node distance=2.8cm,
            semithick]
\tikzstyle{every node}=[draw, circle,minimum size=12mm, text=black, fill=blue!20]
\begin{scope}
        \node  (a) {$+$};
        \node (b) [below right of=a]{$\sin$};
        \node (d) [below left of=a] {$\pi$};
        \node (e)[below of=b] {var 4};
\end{scope}
        \path (a) edge [bend right](d);
        \path (a) edge [bend left](b);
        \path (b) edge  (e);
\end{tikzpicture}
    \caption{The compiled portion of the gene sequence from \Fref{fig:cgeex1}.}
    \end{subfigure}
    \caption{An example genotype and its corresponding phenotype in Cartesian genetic programming.}\label{fig:cge}
\end{figure}
\subimport{.}{MMLGAImpl.tex}

\section{Outline of compressor}
\begin{figure}[p]
    \centering
        \subimport{.}{CompressorOverview.puml}
    \caption{UML activity diagram of the prototype compressor}
    \label{fig:CompressorOverview}
\end{figure}
\begin{sidewaysfigure}[p]
    \centering
        \subimport{.}{wbs.puml}
    \caption{Work breakdown structure of the compressor}
    \label{fig:CompressorWBS}
\end{sidewaysfigure}
The overall design is based on a computational effects framework\autocite{kiselyov_freer_nodate} in order to minimise the interaction between logically separate aspects of the implementation; for example, the random number generator doesn't need to worry about I/O. Computational effects are rather suited to a pipeline style of processing.
An overview of the data flow is shown in \Fref{fig:CompressorOverview}, and an implementation plan in \Fref{fig:CompressorWBS}.


Acquired data is used to evaluate generated models using message length as the \gls{fitnessfunc}. The \gls{model} precedes the \gls{data}, encoded in a manner similar to ZPAQ\autocite{mahoney_zpaq_2016}.
\subsection{Training}
Models are trained in an online fashion, with each model's fitness being evaluated by determining the message length of the current chunk of input data. It is yet to be determined whether generational reproduction or steady-state reproduction is the best approach here; likewise, it is unclear whether to use the same chunk of data for every individual in a generation, or divide it into exclusive batches in order to reduce the computational burden.
\section{Components}
\subsection{Encoder}
The encoder logic is primarily implemented in the range mapping functions of the \gls{model} \gls{typeclass}. The encoder iterates through the datastore, arithmetically coding each result using the range mapper.
\subsection{Datastore}
The datastore is a simple set of signs from the coding alphabet. The alphabet contains signs for \emph{sets}, \emph{sequences}, \emph{literals}, model description, and housekeeping details such as \emph{end of file}. Experiment results are stored as ordered tuples containing side information --- such as the control variables or a timestamp --- and the experimental result, typically a time series represented as a sequence.
\subsection{Models}
\subsubsection{Creation}
The model creation uses \gls{gp}, which has been explained in \Fref{sec:gppgm}.
\subsubsection{Range mapper}
Following \textcite{goos_arithmetic_2003} and \textcite{steinruecken_lossless_2014}, a model implements a function to map a symbol to a compact subinterval of \gls{unitinterval} and a new model.


\section{Current challenges and future research}
Due to \gls{haskell} being a strongly typed language, as well as being a simply typed one (i.e.\ one that does not allow types to depend on values), achieving the desired level of generality in a type-safe manner while maintaining computational performance requires some creativity. That being said, it has saved a lot of effort, with only a trivial amount of debugging required so far --- most potential bugs have been caught by the compiler.
\subsection{Model creation}
Exactly what operations and probability distributions to allow in computation nodes is still being determined. Inference of probabilities is yet to be implemented. Additionally, if \gls{beliefupdate} is to be allowed, then this may require using a dedicated \gls{pgm} library. Fortunately, however, the separation of \gls{genotype} and \gls{phenotype} naturally allow for separation of concerns in implementation.
\subsection{Hybrid symbolic regression}
One approach which may yield better results is to use genetic algorithms to determine the structure of the generating equations, and use an alternative method of symbolic regression to determine parameters (such as statistical moments). This approach would allow using various statistical ``goodness of fit'' measures computed by standard statistical software packages to be used in the fitness function.

\subsection{Simulated annealing}
Combining simulated annealing with genetic algorithms may yield interesting results. This may be as simple as making the mutation intensities and probabilities be a function of the generation; with the flexible approach taken with regards to ``special'' genes, it may also be species-dependent.
\subsection{Data representation}
One problem is how to integrate the model of the experimental results into the probabilistic model required for coding the datastore. It may be desirable to only partially arithmetically code the datastore --- namely, the experimental results --- and dictionary code the data structure description.

Another problem lies in finding a satisfactory representation of the coding alphabet, and its relation to the datastore and model.

\subsection{When should we build new models?}
Should building new models be part of the compressor that happens automatically, or should it be a deliberate action exposed to the reinforcement learning algorithm? In the prototype, it happens each time data is added, but this clearly is not efficient. Some possibilities are adding data in batches and only generating a model after each batch; asking each time data is added if a new model should be generated; or exposing a hook to the model generation process along with the existing model, and passing these to an external function which implements its own logic on when (and how) to generate new models. This last option is the most flexible.
