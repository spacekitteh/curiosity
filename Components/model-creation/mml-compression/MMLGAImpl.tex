\section{Implementation}
\subsection{Instruction set}
Our instruction set is a combination of typical \gls{expression} tree operators (such as addition, multiplication, and constants), as well as nodes representing probabilistic variables. Probabilistic nodes are conditioned on their inputs.

Each node consists of an operator, two input addresses, and two scalar parameters, shown in \Fref{fig:cgenode}.
\begin{figure}
    \centering\begin{bytefield}{32}
        \wordbox{1}{Operation}\\
        \bitbox{16}{Input 1}&\bitbox{16}{Input 2}\\
        \bitbox{16}{Scalar parameter 1}&\bitbox{16}{Scalar parameter 2}
    \end{bytefield}
    \caption{Cartesian genetic programming node layout}
    \label{fig:cgenode}
\end{figure}

Currently, only nodes denoting deterministic functions are implemented.
\subsection{Models}
A model is currently represented by a single linear chromosome containing a sequence of raw instructions, including their offset pointers. A future enhancement is the extraction of subsequences into their own chromosome, with the code in the initial chromosome being replaced by a function call to the new chromosome.

\subsection{Optimisation}
\subsubsection{Sequence representation}
Our initial prototype represented genomes via a linked list of genes. However, despite their simplicity, linked lists offer very poor time and space complexity for most operations of interest to us. For example, random access is on the order of \(O(n)\), and concatenation on the order of \(O(m+n)\). %TODO Use correct symbology

To facilitate experimenting with different representations of sequences, we abstracted the representation to a existentially quantified \gls{gadt}. All operations were thus constrained to be those exposed by the \mintinline{haskell}{Traversable} typeclass.

The first data structure we experimented with was the \mintinline{haskell}{Data.Sequence} type, which is a specialised implementation of a \emph{2,3-fingertree}. This offers a logarithmic improvement in algorithmic complexity for many operations, and constant time concatenation. However, it also makes some operations worse by a logarithmic factor.

This significantly reduced the time required to perform most genetic operations, with the time required to perform reproduction reducing by a factor of 20. However, it was clear that there were still some significant gains to be made in this aspect.

We therefore used the \mintinline{haskell}{Data.Vector} type, which is a highly optimised array implementation, allowing the compiler to make significant use of \emph{stream fusion} \autocite{mainland_exploiting_2017}. This further dramatically increased the efficiency of most genetic operations, to the point of no longer being a bottleneck.

\subsubsection{Reproduction probabilities}
Initially, the default strategy to select which organisms were to reproduce simply sorted by fitness, took the fittest half of the population, and randomly bred those. A depiction is shown in \Fref{fig:reproschemes}. Not only does this reduce the effectiveness of genetic algorithms by removing too much genetic diversity and increasing early convergence on local minima, it also requires sorting the population, which, for large populations (\(n > 1000\)), becomes prohibitively slow. Therefore, the default reproduction strategy was changed to be a randomised strategy, with individuals selected in proportion to their fitness. This changed selection from an \(O(n \log n)\) operation to an \(O(n)\) operation, helping tremendously for larger population sizes.
\begin{figure}
    \centering
    \begin{subfigure}{0.7\textwidth}
        \centering
    \digraph[width=1\linewidth]{reproschemes1}{
        forcelabels=true;
        graph [
        layout="dot"
        newrank="true"
        #rankdir = "LR"
        ]
        node [
        fontsize = "16"
        shape = "ellipse"
        ]
        edge [
        ]
        subgraph clusterElitism {
            "el1" [
            label = "1"
            ]
            "el2" [
            label = "2"
            ]
            "el3" [
            label = "3"
            ]
            "el4" [
            label = "4"
            fillcolor="red"
            style="filled"
            ]
            "el5" [
                rank=2
    label = "5"
    fillcolor="red"
    style="filled"
    ]
        "el6" [
                rank=2
    label = "8"
    fillcolor="red"
    style="filled"
    ]
            }
        }
        \caption{Elitist reproduction}
    \end{subfigure}
    \begin{subfigure}{0.7\textwidth}
        \centering
    \digraph[width=1\linewidth]{reproschemes2}{
        forcelabels=true;

        graph [
        layout="dot"
        newrank="true"
        #rankdir = "LR"
        ]
        node [
        fontsize = "16"
        shape = "ellipse"
        ]
        edge [
        ]
        subgraph clusterProportional {
            "pr1" [
            label = "1"
            rank=1
            ]
            "pr2" [
            rank=1
            label = "2"
                fillcolor="red"
            style="filled"
            ]
            "pr3" [
                    rank=1
            label = "3"
            ]
            "pr4" [
                    rank=1
            label = "4"

            ]
                "pr5" [
                        rank=2
            label = "5"
            fillcolor="red"
            style="filled"
            ]
                "pr6" [
                        rank=2
            label = "8"
            fillcolor="red"
            style="filled"
            ]


            }
        }
        \caption{Proportional reproduction}
    \end{subfigure}
    \caption{Reproduction schemes. Each node represents an individual, labelled with its fitness. The shaded nodes have been selected for reproduction.}
    \label{fig:reproschemes}
\end{figure}

\subsubsection{Parallelisation}
Genetic algorithms fall into the class of ``embarrassingly parallel'' algorithms, meaning they are relatively trivial to parallelise, and offering nearly linear speedup. As such, during reproduction, we parallelise the recombination and mutation operations for each individual. The problem is broken up into chunks, with the number of chunks equalling the number of CPU cores available.

This was mostly trivial to implement, with the exception of having to lift the random number generator seed to a pure value and split it into multiple seeds, in order to easily pass it to each CPU thread. Had we not done this, then each thread would have generated the same random numbers, or required numerous synchronisation delays.

Future work involves removing the parallelisation from the reproduction operations, and instead parallelising entire runs. This would simplify the implementation of genetic operations, but how much is yet to be determined.

\subsubsection{Caching of phenotype evaluations}
Due to the separation of genotype and phenotype, multiple genotypes may give rise to the same phenotype. Further, evaluations are typically the dominant performance cost in any optimisation algorithm. Therefore, we cache the fitness for each phenotype.

Further work in this direction may be to cache results over multiple runs, perhaps even persisting them to storage. For example, if the evaluation process is performing magnetohydrodynamic simulations, then the cost of disk access will be trivial compared to the cost of running a possibly duplicate evaluation.
\subsubsection{Specialisations and rewrite rules}
Haskell supports overloading of functions via \glspl{typeclass}, which are implemented by passing around \emph{dictionaries} at run-time. This is equivalent to the notion of an indirect pointer in imperative languages such as C. If a polymorphic, performance-critical function is called frequently with arguments of statically-known types, then it can be worth writing a new function specifically for the known type. Then, the calls to the original function can be replaced with calls to the new function, removing the indirection.

However, not only does manually rewriting function calls get tedious, it also makes the source code harder to read, and can introduce the possibility of bugs. Therefore, Haskell includes a mechanism for giving user-defined type-safe rewrite rules, which can be used not only to automatically rewrite statically-known calls to specialised functions, but to implement optimisation techniques such as stream fusion.

We therefore implemented several rewrite rules to call specialised versions of frequently called functions.
\subsubsection{Call-site specialisation}
Some performance-critical functions have been rewritten to use \emph{call-site specialisation} \autocite{jones_call-pattern_2007}. This is a technique where, in addition to manually performing a \emph{worker-wrapper transformation}, the worker function is annotated with a special value to instruct the compiler to very aggressively perform optimisations such as unboxing and constant folding. This was performed on several low-level functions which operate on numeric intervals.

\subsubsection{Random number generation}
Currently, the biggest bottleneck is the library used for random number generation. The \texttt{mwc-random} library used does not take explicit advantage of SIMD instructions, greatly reducing the performance obtainable from it. Originally, the \texttt{random} library was used, which was even slower and is currently being redesigned; an alternative library which uses SIMD instructions would likely greatly increase the performance of the overall software.

Some alternative, SIMD-optimised libraries offer a nearly-identical API to \texttt{mwc-random}, but are missing the API used to create probability distributions from tables; this would have to be implemented in order to make the switch seamless.

One simple optimisation, however, may be to only create a number of seeds equal to the number of CPU cores, instead of one for each individual. This may greatly help reduce cache thrashing.
\subsubsection{LLVM}
All development was performed on Windows systems, where the GHC distribution does not include the LLVM backend. One reason for this is the LLVM Windows binary distributions omitting the \texttt{opt} tool, requiring compilation from source. However, \texttt{stack} makes it difficult to use system software by design, and so optimising with LLVM was never performed.

\subsubsection{Unboxing}
The semantics of lazy evaluation require values to be passed by reference, in order to concretely represent values which may be \gls{bot}. In practice, this means passing pointers to values, rather than the values themselves. If it is guaranteed that a value will be used, however, it may be passed ``strictly''. A strict value is one which is evaluated to \gls{whnf}. A value in \gls{whnf} is one which has been evaluated sufficiently to determine what its outer-most constructor is, guaranteeing that it is not \gls{bot}\footnote{If a value guaranteed to be used \emph{is} \gls{bot}, then it wouldn't have mattered whether it was passed with lazy or strict semantics; it would have resulted in the same behaviour: a crash.}. Since a strict value is guaranteed to not be \gls{bot}, it may be passed as the value itself, rather than a pointer; this can potentially reduce memory traffic required in several ways.

GHC performs strictness analysis to attempt to identify places where it can pass values strictly; but as this is equivalent to the halting problem, it can only safely underestimate these locations. Therefore, the user can manually specify which values can be passed strictly, and which can be unboxed --- that is, which can be passed solely by passing its constituent values, eliminating further memory overhead.

One library which makes heavy use of this is the \mintinline{haskell}{Data.Vector.Unboxed} module, which allows customising the memory layout of a vector of unboxable types. Taking advantage of this is a potential future improvement.