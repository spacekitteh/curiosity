% !TEX TS-program = lualatex
% !TEX encoding = UTF-8 Unicode

\documentclass{book}
\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

\usepackage{import}
%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper}


\usepackage{graphicx} % support the \includegraphics command and options

\usepackage{fancyref}
\usepackage[binary-units=true]{siunitx}
% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
%\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...
\usepackage{rotating}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amsfonts}
%\usepackage{tikz}
\usepackage{pgfplots}

\usepackage{epstopdf}
\pgfplotsset{compat=1.16}
\usepgfplotslibrary{external}
\usetikzlibrary{chains,fit,shapes}
\tikzexternalize


%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

\usepackage[
    style=numeric-comp,
    sortlocale=en_AU,
    doi=true,
    eprint=false,
    maxcitenames=2
]{biblatex}
\addbibresource{Cognitive-robotics.bib}

\usepackage{xcolor}
\usepackage{minted}[newfloat=true,mathescape]
\usepackage{mdframed}
\usemintedstyle{fruity}
%\BeforeBeginEnvironment{minted}{\begin{mdframed}}
%\AfterEndEnvironment{minted}{\end{mdframed}}
\definecolor{solarizedbg}{rgb}{0.0,0.169,0.212}
\newenvironment{code}{\VerbatimEnvironment\begin{minted}[breaklines, autogobble,bgcolor=solarizedbg]{haskell}}{\end{minted}}
\setmintedinline{bgcolor=solarizedbg}
\newenvironment{spec}{\VerbatimEnvironment\begin{minted}{haskell}}{\end{minted}}

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\usepackage{plantuml}
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!
\usepackage[pdf]{graphviz}

\usepackage{hyperref}
\usepackage{academicons}
\newcommand{\orcid}[1]{\thanks{\url{https://orcid.org/#1}}\href{https://orcid.org/#1}{\textcolor[HTML]{A6CE39}{\aiOrcid}}}

\usepackage[xindy,toc,accsupp,symbols,abbreviations]{glossaries-extra}
\usepackage[xindy]{imakeidx}
\usepackage{bytefield}

\makeglossaries
\makeindex
\long\def\comment#1{}
\loadglsentries{Glossary.tex}


\title{Artificial curiosity in robotic cosmonauts \\ \large Information-theoretic thought}
\date{16/06/2019}
\usepackage[activate={true,nocompatibility},
    final,
    tracking=true,
    factor=1100,
    stretch=10,
    shrink=10]{microtype}

\author{Sophie Taylor\orcid{0000-0002-2418-5068}\\ n6362575 \small}
\newenvironment{abstract}%
    {\cleardoublepage\thispagestyle{empty}\null\vfill\begin{center}%
    \bfseries Abstract\end{center}}%
    {\vfill\null}

\begin{document}
\frontmatter
\maketitle
\begin{abstract}
    Space exploration is heavily reliant on automation; this reliance only increases with the distances involved in deep space exploration. Two problems arise with this. First, real-time communication is impossible, rendering remote control a slow, tedious affair; therefore leading to the reliance on automation. Second, the amount of science data which can be transmitted back to earth drops drastically, due to very tight power and weight constraints. Compression, while crucial to solve the problem of limited communication, has hitherto been relatively ignored in solving the problem of automation. Only recently has work in artificial intelligence and machine learning started to recognise the potential utility of compression --- as a stand-in for estimating Kolmogorov complexity --- being used in the creation of autonomous agents.

    Compression is the process of constructing a (possibly implicit) probabilistic model of the input data, and using it to assign probabilities during the source coding process. Similarly, we generally perform scientific experiments in order to construct models of (usually) physical processes. Both of these processes can be quite expensive in  terms of computational resources, power and energy usage, and financial.

    In this thesis, we note that there is significant redundant computational processing involved in model generation for science purposes, and model generation for compression purposes. \emph{For a large class of problems, the model we seek is identical in both circumstances.} We therefore propose an architecture for the combination of the two previously mentioned approaches, and describe the progress of a prototype implementation.

\end{abstract}
\tableofcontents
\mainmatter
\part{Overview}
\import{./}{Introduction}
\import{./}{Background}
\import{./}{Plan}

\part{Organic compression}
\import{Components/model-creation/mml-compression/}{UpholdMinimumMessageLengthThought}
\import{Components/model-creation/mml-compression/}{TurningHegelOnHisHead}
\part{Analysis}
\chapter{Discussion}
\section{Testing environments}
The choice of domain in which to perform testing of our architecture is one which must be carefully considered. The obvious first choice is classical mechanics, as Newton's laws provide a simple, yet important test case. Another choice --- albeit one less relevant to space exploration --- is in the field of bioinformatics. Finally, we can choose one which is not at all related to scientific space exploration, as a test of generality of the model discovery capabilities of our architecture. We therefore have chosen, as a third test case, the creation of a bot for the Real-Time Strategy game StarCraftII. An overview of some experiments in these domains can be found in \autoref{part:domains}.
\section{Optimisation results}
Due to the manner in which the prototype has been implemented, performance improvements were done at the same time as feature addition. Therefore, rigorous performance comparisons can not be made, as the optimised versions perform additional operations. We can therefore only give a qualitative figure for the run-time performance improvement compared to the original implementation. We estimate that there has been an improvement of three orders of magnitude in run-time, and an order of magnitude improvement in memory usage.

\section{Questions remaining about the test agent}
The following questions must be answered before implementation of an artificially curious agent can begin.
\subsection{How should decisions be made regarding what action to take?}
We have essentially described dual-purposing a reward signal to be used in traditional autonomous agent architectures. An orthogonal topic is how to actually choose actions in response to the reward signal. Possible options include neural networks, or a topological approach such as outlined by \textcite{lakkaraju_identifying_2016}. A weighted combination of the two after their evaluation, or perhaps passing the output of the topological approach as input to the neural network may be a good solution. %TODO Explain why
\subsection{What reinforcement learning algorithm should be chosen?}
Episodic learning is a seemingly appropriate technique to employ, enabling an agent to ``look back'' at past episodes in order to ``strengthen their reflexes''. The specific choice depends on the technique used for deciding actions.
\subsection{How can the agent move through the action space?}
The primary action available to the agent is to perform experiments. Thus, specific actions correspond to experiments with specific control variables. Moving through a continuous space of actions, such as by a gradient descent algorithm, is fairly straightforward. However, if the action space is a hybrid of discrete and continuous actions, then this becomes much more difficult.
\chapter{Conclusion}
We have identified an opportunity to greatly increase the performance and flexibility of autonomous agents, and outlined a prototype implementation of the core of the approach. While the implementation of the solution is still only partially completed, arguably the major result from the project is the statement of the problem itself: while all the pieces existed separately already, and many combinations of some of the pieces also existed, it appears to be a novel contribution to the field of autonomous scientific systems in general, and autonomous space exploration in particular.

As there is much work remaining to be done, and the potential for much deeper exploration, implementing a complete prototype presents an ideal topic for post-graduate research, which the author intends to pursue. Some specific future engineering work has been identified in previous chapters, but there are also deeper, more philosophical questions that remain to be explored.
\import{Reflection/}{FutureResearch}
\printglossaries
\printbibliography
\appendix
\part{Appendix --- Experiment domains}\label{part:domains}
\import{Domains/}{Bioinformatics}
\import{Domains/}{Mechanics}
\import{Domains/}{StarCraftII}
%TODO re-include code
%\part{Appendix --- Code listing}\label{part:code}
%\import{Components/model-creation/cultural-evolution/}{GPFramework}
\backmatter

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
