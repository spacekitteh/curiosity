#+BRAIN_PARENTS: selection numerical-optimisation

Pareto frontiers are a method for ranking solutions in optimisation problems with multiple objectives.
