\chapter{Introduction}
Space exploration is hard. One of the reasons for this is the distance. Despite the speed of light being incredibly fast to us, the universe is unfathomably large. The radio propagation time between Earth and a rover on Mars, for example, can be anywhere between 4 minutes and 24 minutes, depending on the time of (each planet's) year. The distance also means enormous \gls{freespacepathloss} --- for example, an S band transmission experiences between \SI{257}{\dB} and \SI{274}{\dB} of loss. This greatly reduces the \gls{shannoncapacity} of the communication channel, thereby reducing the amount of science data that can be returned\autocite{shannon_communication_1949}.

Autonomy is crucial in overcoming these barriers. Autonomous control reduces the need for detailed command constructions; we can create relatively high level, autonomous \gls{gnc} controllers which we issue high-level instructions such as ``go to that interesting rock'', and it can be expected to avoid getting stuck in a crater enroute without any human interaction\autocite{tompkins_global_2004}. To reduce the amount of uplink bandwidth required, we can also pre-process the collected data, such as creating and transmitting only a histogram of the data, rather than the individual data-points; we can also compress it. Combining these two functions is the goal this project is working towards.

It is desirable to make our robotic cosmonauts as autonomous as possible, so ideally we could just instruct them to ``go do some science and report any interesting discoveries''. But how can we mathematically model ``go do some science'', or, for that matter, ``interesting discovery''?

There are two main, complimentary approaches to the creation of autonomous systems: Control theory, and so-called artificial intelligence. Control theory is suited to systems where both the plant and desired behaviour are relatively easy to mathematically \gls{model}, and the desired behaviour is not much more abstract than the plant. Artificial intelligence, on the other hand, is suitable when we find great difficulty in modelling the desired behaviour, which may be very abstract --- typically, we can only indirectly describe the behaviour by providing a measurement of how much we approve of it, also known as a reward metric.

\section{Artificial curiosity}
Let's describe science as ``systematic curiosity''. But what is curiosity? We may describe it as acting in order to maximise the interestingness of our \glspl{observation}. We aren't interested in things that are completely predictable, nor about things which appear completely random. When we see probability --- particularly in its Bayesian, belief-based interpretation\autocite{cox_probability_1946} --- turn up in abstract concepts, such as ``interaction'', ``understanding'' or ``interesting'', it is helpful to turn to information theory. So, let us phrase it in such terms: interestingness is a nonmonotonic measure of similarity to previously understood (or modelled) information.
\begin{figure}
    \centering
\begin{tikzpicture}[baseline]
    \begin{axis}[
        title=Interest vs. complexity,
        xlabel={Complexity},
        ylabel={Interestingness},
        ticks=none,
        axis x line=bottom,axis y line=left]
    % density of Normal distribution:
    \newcommand\MU{0}
    \newcommand\SIGMA{1e-3}
    \addplot[red,domain=-3*\SIGMA:3*\SIGMA,samples=201,ticks=none]
       {exp(-(x-\MU)^2 / 2 / \SIGMA^2) / (\SIGMA * sqrt(2*pi))};
    \end{axis}
    \end{tikzpicture}
    \caption{Complexity increases interest to a point, after which it rapidly reduces.}
    \label{fig:coherenceprogress}
\end{figure}
Compression, the study of which is a field of information theory, works by finding and exploiting similarity in the given data by creating probabilistic \glspl{model}. This is exactly why we want the \glspl{observation} --- to study it to discover and \gls{model} the physical processes that create it. This means we can use \gls{compression} to formalise curiosity, in the form of, for example, coherence progress\autocite{graziano_artificial_2011}.
%TODO explainc oherence progress better
Roughly speaking, coherence progress is the derivative of how effective our \glspl{model} are as we add new \glspl{observation}: If two consecutive results are very efficiently compressed, then the difference in effectiveness is low, thus coherence progress is low; if two consecutive results are poorly understood and modelled, then they will not compress well, and the change in effectiveness is low as well. But if the adding of a new result allows the discovery of a new, more effective \gls{model}, then the difference is large. Thus, we can see compression progress captures the nonmonotonic nature of interestingness. This is illustrated in \autoref{fig:coherenceprogress}.

\section{Contributions}
While coherence progress provides an excellent approach for creating artificial curiosity, it is quite computationally intensive. In highly resource-constrained systems such as deep space science probes, efficiency is of paramount importance. We identify a key opportunity to greatly reduce the computational expense involved in the application of compression-based artificial curiosity, while at the same time greatly assisting human scientists in the creation of scientific theories. We present the core of a prototype implementation of our approach, and outline improvements in the process of being implemented.