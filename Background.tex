\chapter{Background}

%TODO Explain coherence progress
\section{Reinforcement learning}
Reinforcement learning algorithms are training algorithms, designed to maximise the expected reward from a (possibly continuous) sequence of actions of an agent in its environment. One notable class of reinforcement learning algorithms are episodic in nature, exemplified in \autocite{blundell_model-free_2016,pmlr-v70-pritzel17a}. Typically, rewards are extrinsic, such as some fitness measure. Intrinsic rewards, on the other hand, can be considered as \emph{motivations}. One such motivation is curiosity. Extrinsic and intrinsic rewards can generally be combined arbitrarily.


\section{Artificial curiosity}
The notion of ``interestingness'' has only recently been formalised in a satisfactory manner. Compression progress, as outlined by \textcite{pezzulo_driven_2009}, is an information-theoretic approach in which the reward is a function of the differential progress an arbitrarily chosen compression algorithm makes in adapting to its (generally streaming) input. \textcite{graziano_artificial_2011} expound on this in the context of space science. \textcite{frank_curiosity_2014} use artificial curiosity as a reward signal in order to learn motion planning for real-world humanoid robots.


\section{Compression}
\subsection{Lossless compression}
Compression comes in two flavours: Lossy compression, where we allow errors in the reconstructed data to achieve greater compression ratios, and lossless compression, where we can't allow any errors, and thus achieve lower compression ratios. Lossy compression is typically used for audio and visual data, primarily for entertainment purposes. We shall ignore lossy compression, and focus solely on lossless compression.

For decades, lossless compression research was mostly \emph{ad~hoc} and focused on heuristics and intuition which combined both modelling and coding. One notable example is the landmark development of the LZ77 algorithm by \textcite{ziv_universal_1977} which forms the basis of a wide variety of compression algorithms in use today, such as DEFLATE\autocite{deutsch_ghostaladdin.com_deflate_1996} as used in the popular ZIP archive file format. \textcite{steinruecken_lossless_2014} provided a rigorous approach, from information-theoretic first principles, to the fundamentals of lossless data compression for compressing basic mathematical objects such as sequences, sets, multisets and permutations. \textcite{steinruecken_compressing_2016} then expanded on compressing other combinatorial objects.

\subsection{Adaptive compression}
Adaptive compression, of which our compression scheme necessarily falls under, is when the probability model of the data to be compressed isn't known in advance, and must be learned during the compression process itself\autocite{steinruecken_lossless_2014}. A simple example is progressively learning a histogram of the data, and assigning the probability of a symbol having the frequency with which it has appeared so far, also called a 0-gram model\autocite{steinruecken_lossless_2014}.
\textcite{mahoney_adaptive_2005} introduced the use of adaptive weighting of multiple compression models via primitive neural networks.

\subsection{Context sensitive compression}
A model is said to be \emph{context-sensitive} when the probability distribution for a symbol depends on surrounding symbols. For example, in English text, after a ``q'' appears, the next symbol is much more likely to be a ``u'' than a ``z''. An \gls{ngram} model is when a symbol distribution depends on the $n$ symbols occurring immediately prior to it\autocite{steinruecken_lossless_2014}. As we shall be focusing on physical processes, which are typically \emph{topologically continuous}, our compression modelling must be context sensitive.

\section{Probabilistic modelling}
Given a perfectly accurate probabilistic model of some data, we can optimally compress it (to within 2 bits) via arithmetic coding to its information content \autocite{steinruecken_lossless_2014}. Thus, compression research mostly focuses on improving modelling techniques.
\subsection{Model building}
There are many approaches to building predictive models of data. Some of the techniques particularly relevant to our setting are Gaussian processes \autocite{duvenaud_automatic_2014} and \glspl{ga} such as used in Eureqa \autocite{schmidt_distilling_2009}.
\subsection{Probabilistic graphical models}
Probabilistic graphical modelling is the study of probabilistic modelling using the tools of graph theory. This can drastically simplify the resultant models, increasing both clarity and computational efficiency. \textcite{koller_probabilistic_2009} authored an introductory monograph on the subject, covering varying types of models. \textcite{xiang_probabilistic_2002} studies them in the context of intelligent agents.
\subsection{Induction}
Induction can be described as the problem of how we can justifiably extrapolate observations in order to make predictions. \textcite{rathmanner_philosophical_2011} provide an exposition on Solomonoff's theory of inductive inference\autocite{solomonoff_formal_1964, solomonoff_formal_1964-1}, a mathematical formalisation of Occam's Razor and Epicurus' Multiple Explanations theory, combined with Kolmogorov complexity\autocite{li_introduction_2008} in order to predict the next symbol of an observed sequence.

\section{Information theory}
\textcite{bookstein_compression_1990} provides an overview of how information theory relates to compression.
\subsection{Coding theory}
Coding theory is the study of codes, sequences or other combinations of symbols from an alphabet which are imbued with meaning. Source coding is the study of transforming one code to another in order to reduce the amount of information needed for the representation of its contents. In the context of compression, there are two major classes of codes: Dictionary codes, which are also known as substitution codes, and arithmetic codes \cite{steinruecken_lossless_2014}. A simple example of a dictionary code for English is augmenting the Latin alphabet with a new symbol which means ``the'', reducing the number of symbols required from three symbols to one. Arithmetic coding, on the other hand, encodes a single codeword representing the entire message by explicitly representing the decisions required to construct it. It is typically much easier to change the modelling process when using arithmetic coding, as dictionary codes tend to be intertwined with the model. Therefore, we shall be using arithmetic coding. One approach to implementing an arithmetic coder, particularly attractive in the context of using Haskell, is given by \textcite{goos_arithmetic_2003}. \textcite{steinruecken_lossless_2014} also proposed an interesting design. \textcite{huang_model-code_2015} provides a framework for a complete model-code separation in order to maximise modularity.


\subsection{Directed information and network information theory}
Directed information theory (and, more generally, network information theory) is the study of information flow in stateful systems with feedback. Originating from work by \textcite{massey_causality_1990}, it is particularly useful for causal systems, where future symbols may depend on past symbols. It has been used to characterise protocol dialog\autocite{kim_directed_2009}, filter networks\autocite{derpich_entropy_2015}, some aspects of compression and hypothesis testing\autocite{permuter_interpretations_2011}, and even LQG control\autocite{tanaka_lqg_2018}. \textcite{quinn_directed_2015} shows a connection with probabilistic graphical models.

\section{Genetic algorithms and genetic programming}
\Glspl{ga} are \glsdescplural{ga} Of particular interest to us is \gls{gp}, \glsdesc{gp} Cartesian genetic programming, as in \textcite{miller_cartesian_2000}, is a form of \gls{gp} which treats programs as \glspl{digraph} of instructions. This helps solve some representational problems with the traditional, abstract syntax tree style, as in the standard reference monograph by \textcite{koza_genetic_1992}.

A particularly strong application of \gls{gp} is in symbolic regression\autocite{keijzer_declarative_2002}\autocite{chen_improving_2018}\autocite{giustolisi_symbolic_2006}\autocite{babovic_genetic_2000}. It also finds use in the field of \gls{bioinformatics}, as in \textcite{konagurthu_minimum_2012} and \textcite{lee_comprehensive_2018}.

\import{Scope/}{Focus}