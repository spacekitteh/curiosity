FROM nixos/nix

RUN nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs && nix-channel --update

RUN nix-env -f "<nixpkgs>" -iA stack git openblas

RUN nix-env -iA cachix -f https://cachix.org/api/v1/install && cachix use all-hies

RUN nix-env -iA selection --arg selector 'p: { inherit (p) ghc865; }' -f https://github.com/infinisil/all-hies/tarball/master
