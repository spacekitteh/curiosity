\section{Focus of project}
The aspect of \gls{compression}-based curiosity we are interested in is the dual-purposing of \gls{compression} during \gls{online} learning, where curiosity is (possibly merely a part of) the agent's \gls{reward}. We examine the problem from the point of view of \glspl{scientificagent} -- in other words, agents who exist to learn for learning's sake.

\subsection{Efficiency}
Due to the enormous costs involved, a primary concern of space systems is efficiency.
\subsubsection{Computational}
As the primary computational challenge of \gls{compression} is in constructing a probabilistic \gls{model} of the \gls{data} to be compressed, it is highly desirable to not repeat this process. If we are compressing our \gls{data} for transmission, then it makes little sense to not take full advantage of the process. By integrating \gls{compression} into our agent's control scheme, we may greatly reduce the computational resources required for an autonomous agent.
\subsubsection{Power}
Transmitting a lot of \gls{data} requires a lot of power, whether due to large bandwidth requirements, the long duration to transmit it all, or just overcoming the \gls{freespacepathloss}. By minimising the amount to be transmitted, the power requirements are reduced. Further, with a reduced computational load, less power is used.
\subsubsection{Architectural}
Software reuse is a fundamental tool in software engineering. Not only does it reduce implementation time, it can also increase reliability by exposing more bugs. It can also result in smaller compiled code size, meaning more of the program can reside in the CPU cache, greatly reducing memory latency and power use.
\subsubsection{Weight}
By reducing the power requirements, the weight of the cosmonaut's power system can be reduced. Further, by reducing the bandwidth required to transmit \gls{data}, the telemetry system may require less gain, and therefore less weight in antennas and amplifiers.
%\subsection{Expressiveness of the reasoning}
\subsection{Introspection of the learned models}
\subsubsection{Experiment design}
An agent might use a computer algebra system, for example, to find critical points in a model to test. A notable example is discovery and characterisation of the Prandtl-Glauert singularity in compressible fluid dynamics.
\subsubsection{Simplification of the models}
By examining the model and the \gls{observation} history, the agent might be able to simplify the \gls{model} without losing accuracy, such as if it has exhaustively tested some subspace of actions. One example might be the elimination of handling special cases if it is shown that the special cases are equivalent to an already tested action.
\subsubsection{Diagnostics}
If there is a constant bias in the \glspl{observation}, this may indicate the agent effectors and sensors need recalibration. Additionally, if repeated identical actions typically produce identical observations, but not always, then this suggests that either the process is stochastic, or the \gls{embodiment} of the agent is transiently malfunctioning. Examples include a gyro installed in the wrong orientation, a loose solder joint, and radiation-induced \glspl{seu}. Another failure mode that may be detected is permanent damage, if old experiments are repeated periodically; for example, damage to RAM may be detected if old, verified \glspl{model} are suddenly stochastic.
\subsection{The philosophical relationship between desires and reasoning}
We are proceeding under the assumption that curiosity is a fundamental aspect of motivation in all sentient agents.
By examining motivation, modelling and reasoning simultaneously, we may gain some insight into what is common between them, and what is different. Is there some underlying process which generalises these? Or is the commonality between them merely a result of our chosen approach to modelling curiosity?

\subsection{Flexibility}
\emph{How} models are constructed is irrelevant to us. All we care about is that these models can be examined and understood; ideally by human scientists, but not necessarily.

This allows considerable flexibility in our design process. We may choose simple off-the-shelf \gls{compression} algorithms such as LZ77, and create adapters which expose the implicitly generated models. We may choose a neural network, which may be particularly useful in image-based problems due to the relative simplicity of analysing convolutional neural networks. We may choose to use a computer algebra system, if we are studying physical laws. We may even choose a mixture of these, in the shotgun-style of the PAQ\autocite{mahoney_adaptive_2005} series of compressors.

By allowing the model generation to be a design parameter, we can choose it in consideration with its desired autonomy. If we desire to create an agent to study the geochemistry of Pluto's moons, then the raw distance constrains us to highly autonomous systems; it also means that due to the enormous \gls{freespacepathloss}, the bandwidth will be miniscule, and thus the \gls{data} must be as compressed as possible. So not only will the bandwidth requirements be reduced, but the productivity may be increased as a result as well. This also allows flexibility in how the models are reflected upon and updated.

However, as we will see in \Fref{chap:mml}, we have gone with an evolutionary approach.